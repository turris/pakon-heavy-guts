/*
 * Copyright 2017, CZ.NIC z.s.p.o. (http://www.nic.cz/)
 *
 * This file is part of the pakon system.
 *
 * Pakon is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 * Pakon is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pakon.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PAKOND_RINGLOG_H
#define PAKOND_RINGLOG_H

#include <string>

/*
 * The ring buffer logger
 *
 * The idea is to have a logging destination that is fast but doesn't output
 * anything. It simply keeps number of the last messages and has the ability to
 * dump them. Therefore, we can send detailed logging there and if anything bad
 * happens (unknown exception, known bug situation, crash, …), the detailed
 * logs prior to the problem can be dumped without slowing down the whole run
 * time.
 *
 * However, reliability in some corner cases has been sacrificed for simpler
 * and likely faster implementation. Therefore, it is not guaranteed all the
 * messages logged in the interval from the oldest to the newest message in the
 * dump are present, especially at the beginning and after the call to the
 * dump* function (eg. the messages logged by other threads while dumping).
 *
 * Also, each message has a limited maximal size, long ones may be truncated.
 */

namespace Pakon {

// Send a message to the ring log
void ringlog(const std::string &msg);
/*
 * Returns the content of the ring log, one message per line.
 *
 * The messages are sorted according to the age. The log is cleared by this
 * call.
 */
std::string dumpRingMem();
/*
 * Dump the log into a file somewhere in /tmp.
 *
 * The format and side effects are the same as of dumpMem(). The filename is
 * then logged on the error level.
 */
void dumpRingFile();

}

#endif
