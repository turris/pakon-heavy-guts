#!/bin/sh
# Copyright (C) 2017 CZ.NIC z.s.p.o. (http://www.nic.cz/)

# wrapper script to make sure that iptables NFQUEUE rules dies with pakond
if [ "$#" -ne 2 ];then
	echo >&2 "arguments: config_name pid_file"
	exit 1
fi
#workaround to get pid: run in background, get pid and wait for child
PAKOND_STDERR_LEVEL=INFO PAKOND_SYSLOG_LEVEL=INFO nice -n -1 /usr/bin/pakond $1 &
iptables -I forwarding_rule -j pakon_filter
ip6tables -I forwarding_rule -j pakon_filter
iptables -I input_rule -j pakon_filter
ip6tables -I input_rule -j pakon_filter
iptables -I output_rule -j pakon_filter
ip6tables -I output_rule -j pakon_filter
pid=$!
echo $pid > $2
#wait does not deliver signals to child, I had to do it manually
trap "iptables -F pakon_filter; ip6tables -F pakon_filter; kill $pid" SIGTERM SIGINT
trap "kill -HUP $pid" SIGHUP
wait $pid
iptables -D forwarding_rule -j pakon_filter
ip6tables -D forwarding_rule -j pakon_filter
iptables -D input_rule -j pakon_filter
ip6tables -D input_rule -j pakon_filter
iptables -D output_rule -j pakon_filter
ip6tables -D output_rule -j pakon_filter
iptables -X pakon_filter
ip6tables -X pakon_filter
iptables -F pakon_filter_ignore 2>/dev/null
ip6tables -F pakon_filter_ignore 2>/dev/null
iptables -X pakon_filter_ignore 2>/dev/null
ip6tables -X pakon_filter_ignore 2>/dev/null
