/*
 * Copyright 2016, CZ.NIC z.s.p.o. (http://www.nic.cz/)
 *
 * This file is part of the pakon system.
 *
 * Updater is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 * Updater is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Updater.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "interpreter.hpp"
#include "configurator.hpp"
#include "lua_convert.hpp"
#include "lua_common.hpp"
#include "types.hpp"

#include <strings.h>

using std::string;
using std::vector;

namespace Pakon {
namespace Lua {

template<> class Extractable<Direction> {
public:
	static void extract(lua_State *state, int index, Direction &result) {
		string unparsed;
		Pakon::Lua::extract(state, index, unparsed);
		if (strcasecmp("in", unparsed.c_str()) == 0)
			result = Direction::IN;
		else if (strcasecmp("out", unparsed.c_str()) == 0)
			result = Direction::OUT;
		else
			throw Mismatch("Direction must be either 'IN' or 'OUT' string, not " + unparsed);
	}
};

template<> class Extractable<Family> {
public:
	static void extract(lua_State *state, int index, Family &result) {
		string unparsed;
		Pakon::Lua::extract(state, index, unparsed);
		if (strcasecmp("ipv4", unparsed.c_str()) == 0 || strcasecmp("ip", unparsed.c_str()) == 0 || strcasecmp("ip4", unparsed.c_str()) == 0 || unparsed == "4") // The last will take numeric 4 as well
			result = Family::IPv4;
		else if (strcasecmp("ipv6", unparsed.c_str()) == 0 || strcasecmp("ip6", unparsed.c_str()) == 0 || unparsed == "6")
			result = Family::IPv6;
		else
			throw Mismatch("Family must be either IPv4 or IPv6, not " + unparsed);
	}
};

}

Interpreter::Interpreter() {
	TRC;
	state = luaL_newstate();
	luaL_openlibs(state);
	// Insert pointer to ourself into the lua interpreter, so we can map back
	lua_pushlightuserdata(state, this);
	lua_setfield(state, LUA_REGISTRYINDEX, "interpreter");
#define CBACK(name) insertCallback<&Interpreter::name##Callback>(#name);
	CBACK(queue);
	CBACK(listen);
#undef CBACK
}

Interpreter::~ Interpreter() {
	TRC;
	if (state)
		// It might be null if there was a problem allocating in the constructor
		lua_close(state);
}

/*
 * Because lua calls us with its state, not with our own object,
 * we need to have a static method instead, and look up ourselves in
 * there. But doing it all the time is tiresome, so this way we generate
 * a new wrapper static method for each callback function we have.
 */
template<int (Interpreter::*Callback)(lua_State *)> int Interpreter::callback(lua_State *state) {
	TRC;
	// Look up ourselves first.
	lua_getfield(state, LUA_REGISTRYINDEX, "interpreter");
	Interpreter *self = static_cast<Interpreter *>(lua_touserdata(state, -1));
	lua_pop(state, 1);
	// Perform the call
	return (self->*Callback)(state);
}

template<int (Interpreter::*Callback)(lua_State *)> void Interpreter::insertCallback(const char *name) {
	LOG(TRACE, "Inserting global function ", name);
	lua_pushcfunction(state, &Interpreter::callback<Callback>);
	lua_setglobal(state, name);
}

void Interpreter::doFile(const string &path) {
	using namespace Lua;
	TRC;
	mutex_guard guard(mutex);
	LOG(DBG, "Loading file ", path);
	StackGuard stackGuard(state);

	// Load the function.
	const int load_result = luaL_loadfile(state, path.c_str());
	switch (load_result) {
		case 0: // OK
			break;
#define ERR(CONST, EXCP) \
		case LUA_##CONST: \
			throw EXCP(lua_tostring(state, -1))
		ERR(ERRSYNTAX, SyntaxError);
		ERR(ERRMEM, OutOfMemory);
		ERR(ERRFILE, FileError);
		default:
			throw Error(lua_tostring(state, -1));
	}
	/*
	 * Create a new environment for the file to run in, so the files don't clobber each other's globals.
	 * As this is not a security measure, just convenience, we don't explicitly prevent them clobbering
	 * the global modules.
	 */
	lua_newtable(state);
	// Iterate through LUA_GLOBALSINDEX
	lua_pushnil(state);
	while (lua_next(state, LUA_GLOBALSINDEX)) {
		// As lua_settable pops both the index and the value, we create a copy of them
		lua_pushvalue(state, -2);
		lua_pushvalue(state, -2);
		// Now make a copy in the new table (which is currently at -5, 2*2 values from iteration is at the top of them)
		lua_settable(state, -5);
		// Pop the original value pushed by lua_next, but keep the index there (it is popped by the next lua_next)
		lua_pop(state, 1);
	}
	// The table is on top, the function just below. Let's put them together.
	lua_setfenv(state, -2);
	// Now we have the file with the environment set, run it.
	const int call_result = lua_pcall(state, 0, 0, 0);
	switch (call_result) {
		case 0: // OK
			break;
		ERR(ERRRUN, RuntimeError);
		ERR(ERRMEM, OutOfMemory);
		// We don't handle ERRERR, as we don't set the error handler
		default:
			throw Error(lua_tostring(state, -1));
	}
}

int Interpreter::queueCallback(lua_State *state) {
	return Lua::cFromLua(state, Configurator::instance(), &Configurator::addQueue);
}

int Interpreter::listenCallback(lua_State *state) {
	return Lua::cFromLua(state, Configurator::instance(), &Configurator::addSocket);
}

}
