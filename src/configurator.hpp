/*
 * Copyright 2016, CZ.NIC z.s.p.o. (http://www.nic.cz/)
 *
 * This file is part of the pakon system.
 *
 * Pakon is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 * Pakon is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pakon.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PAKOND_CONFIGURATOR_H
#define PAKOND_CONFIGURATOR_H

#include "util.hpp"
#include "worker.hpp"
#include "nfq.hpp"
#include "dissect.hpp"
#include "types.hpp"

#include <mutex>
#include <list>
#include <unordered_map>
#include <unordered_set>

namespace Pakon {

/*
 * This is used to configure the runtime state of the pakond.
 * The main idea is that you start a configuration and hold
 * a transaction object (it currently can't do a rollback
 * on error, though). When it starts the transaction, it locks
 * everything needed and stops activity. It closes previous
 * resources. Once all that is done, commands to provide new
 * configuration may be issued. After the transaction object
 * is destroyed, the operation is resumed with the new config.
 *
 * Only one transaction object can be created during each time
 * (others wait for a mutex).
 */
class Configurator : public Singleton<Configurator> {
private:
	/*
	 * We protect from multiple transactions running. Note
	 * we don't have to protect any actual configuration that
	 * may be stored here. Once we start the transaction, we
	 * first stop all the other threads before we are able
	 * to do any reconfiguration. Therefore once we start
	 * changing it, there's nothing that could access it.
	 */
	std::mutex transaction_mutex;
	// The dissectors we currently have
	std::unordered_map<std::string, Dissector> dissectors;
	// Set of dissectors used during this transaction. Used at the end of the transaction to remove the others, unused ones.
	std::unordered_set<std::string> dissectorsUsed;
	std::list<Worker> workers;
	std::list<NFQ> nfqs;
	// The sockets being listened on
	std::unordered_set<std::string> sockets;
	// The ones left behind after reconfiguration ‒ nonempty only during transaction
	std::unordered_set<std::string> socketsUnused;
	Configurator();
	// For easier sanity checking.
	std::atomic<bool> in_transaction = {false};
	// Metadata so we can manage the workers and restart them
	uint64_t last_thread_id = 0;
	// A mapping from ids to metadata ‒ iterator and index of the NFQ for the thread
	std::unordered_map<uint64_t, std::tuple<std::list<Worker>::iterator, std::list<NFQ>::iterator>> worker_ids;
	void begin();
	void end();
	// Start a worker thread with given the nfq
	void startThread(const std::list<NFQ>::iterator &nfq);
	// Find or create a disector of the given name. Mark it as used.
	Dissector &dissector(const std::string &name);
	friend class Transaction;
	friend class Singleton<Configurator>;
public:
	/*
	 * A transaction. It is mostly just a guard object, ensuring
	 * a start and an end of a transaction.
	 */
	class Transaction : private NonCopyable {
	public:
		Transaction();
		~ Transaction();
	};
	/*
	 * Add new set of related NF queues.
	 * tags are arbitrary values to be put onto packets from this queue. Each one is type → name mapping (the querying and aggregation might decide to look at only some tags, and only one tag of a type is allowed).
	 * The dissector is name of the instance of the flow-detector. The ones with the same name share it. They are created implicitly.
	 * The qsize is how many packets the kernel can hold before it starts dropping.
	 * queue is the identifier of the queue in kernel/iptables.
	 * Family and direction are self-explanatory.
	 */
	void addQueue(const std::unordered_map<std::string, std::string> &tags, const std::string &dissector, size_t qsize, uint16_t queue, Family family, Direction direction);
	/*
	 * Add a unix-domain listening socket. We would report the flow events to connecting clients.
	 *
	 * It is ignored if such socket is already opened.
	 */
	void addSocket(const std::string &path);
	// Return all currently active dissectors (pointers valid only until reconfiguration)
	std::vector<Dissector *> allDissectors();
	// Restart a possibly crashed thread. If it is no longer known, do nothing.
	void restartThread(uint64_t id);
};

}

#endif
