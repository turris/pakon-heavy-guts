/*
 * Copyright 2016, CZ.NIC z.s.p.o. (http://www.nic.cz/)
 *
 * This file is part of the pakon system.
 *
 * Updater is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 * Updater is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Updater.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PAKOND_BLOB_H
#define PAKOND_BLOB_H

#include "util.hpp"

#include <string>
#include <cstring>
#include <stdexcept>
#include <tuple>
#include <cassert>
#include <vector>
#include <functional>

namespace Pakon {

/*
 * Extract continuous field of bits.
 */
template<class T> T bitsExtract(T input, size_t offset, size_t count) {
	size_t total = 8 * sizeof input;
	assert(offset + count <= total);
	// Get rid of the left bits
	input = input << offset;
	// Push the count bits to the right side (cutting of everything on the right side)
	return input >> (total - count);
}

/*
 * Something that holds binary data. It does not *own* the
 * data, and expects the data lives through the lifetime of
 * this.
 *
 * Unlike std containers, it doesn't copy the data, it's just
 * thin wrapper.
 *
 * Also, it helps parsing binary data (like IP packets).
 */
class Blob {
private:
	const uint8_t *data = nullptr;
	size_t size_ = 0;
	size_t position = 0;
	// Swap endian of the value, no matter how large
	template<class T> void endianSwap(T &value) {
		const size_t size_ = sizeof value;
		uint8_t *a = reinterpret_cast<uint8_t *>(&value);
		for (size_t i = 0; i < size_ / 2; i ++)
			std::swap(a[i], a[size_ - 1 - i]);
	}
public:
	EXCEPTION(std::out_of_range, OutOfRange);
	Blob() = default;
	Blob(const void *data, size_t size) :
		data(static_cast<const uint8_t *>(data)),
		size_(size)
	{}
	const uint8_t *begin() const { return data; }
	const uint8_t *pos() const { return data + position; }
	const uint8_t *end() const { return data + size_; }
	size_t size() const { return size_; }
	std::string toString() const;
	// Start parsing from the start
	void reset() { position = 0; }
	// Skip so many bytes when parsing
	void skip(size_t bytes) {
		if (position + bytes > size_)
			throw OutOfRange("Skipping past the end");
		position += bytes;
	}
	// Get so many bytes at the current position as another Blob
	Blob slice(size_t bytes) {
		if (position + bytes > size_)
			throw OutOfRange("Slicing past the end");
		Blob result(data + position, bytes);
		position += bytes;
		return result;
	}
	// Get all the data from here to the end
	Blob rest() {
		return slice(size_ - position);
	}
	/*
	 * Read the type. If nbo (network byte order) is true and the
	 * local architecture is not, swap as needed.
	 */
	template<class T> T get(bool nbo = false) {
		T result;
		const size_t size = sizeof result;
		if (position + size > size_)
			throw OutOfRange("Not enough space to fit the data type");
		memcpy(&result, data + position, size);
		position += size;
#if __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__
		if (nbo)
			endianSwap(result);
#elif __BYTE_ORDER__ == __ORDER_BIG_ENDIAN__
		(void) nbo; // Just prevent warning
#else
#error "Unknown byte order"
#endif
		return result;
	}
};

/*
 * An address. Depending on the length, it may be MAC,
 * IPv4 or IPv6 address. In case of TCP/UDP, it may
 * also mean the ports.
 */
class Address {
private:
	/*
	 * The use of string is a bit of an abuse, but it allows embedded 0 and
	 * we use it for various different tricks (like being able to hash it).
	 */
	std::string data;
public:
	Address() = default;
	explicit Address(const Blob &blob);
	std::string toString() const;
	const std::string &binString() const { return data; }
	size_t size() const { return data.size(); }
	bool operator ==(const Address &other) const { return data == other.data; }
	bool operator !=(const Address &other) const { return data != other.data; }
};

enum class Direction {
	IN,
	OUT
};

enum class Family {
	IPv4,
	IPv6
};

enum class IpProto {
	TCP,
	UDP,
	Other
};

}

namespace std {

// Delegate hashing of the address to the string inside
template<> struct hash<Pakon::Address> {
	size_t operator()(const Pakon::Address &addr) {
		static std::hash<string> hasher;
		return hasher(addr.binString());
	}
};

}

#endif
