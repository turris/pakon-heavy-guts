/*
 * Copyright 2016, CZ.NIC z.s.p.o. (http://www.nic.cz/)
 *
 * This file is part of the pakon system.
 *
 * Pakon is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 * Pakon is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pakon.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PAKOND_LUA_EXCP_H
#define PAKOND_LUA_EXCP_H

#include "util.hpp"

#include <lua.hpp>

namespace Pakon {
namespace Lua {

// Base class of lua-based errors (also used when the real exception hasn't been identified)
EXCEPTION(std::runtime_error, Error);
// A syntax error in lua code
EXCEPTION(Error, SyntaxError);
// When the lua interpreter feels to be out of memory
EXCEPTION(Error, OutOfMemory);
// Runtime error inside lua code
EXCEPTION(Error, RuntimeError);
// If types don't match (between lua and C++)
EXCEPTION(Error, Mismatch);

/*
 * Something that restores the stack position to the one at the time
 * of its creation upon destruction. Therefore, if you want to manipulate
 * the stack and you're not sure if you return it correctly (a lot of
 * possible exit places, exceptions, etc), place object of this type at
 * the beginning of your function.
 */
class StackGuard : private NonCopyable {
private:
	lua_State *const state;
	const int stackDepth;
	bool active = true;
public:
	explicit StackGuard(lua_State *state) :
		state(state),
		stackDepth(lua_gettop(state))
	{ }
	~ StackGuard() {
		if (active)
			lua_settop(state, stackDepth);
	}
	// Do not restore the stack on exit
	void release() {
		active = false;
	}
};

}
}

#endif
