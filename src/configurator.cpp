/*
 * Copyright 2016, CZ.NIC z.s.p.o. (http://www.nic.cz/)
 *
 * This file is part of the pakon system.
 *
 * Updater is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 * Updater is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Updater.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "configurator.hpp"
#include "report.hpp"

#include <sys/socket.h>
#include <string>

using std::bind;
using std::string;
using std::make_tuple;
using std::get;

namespace Pakon {

Configurator::Transaction::Transaction() {
	Configurator::instance().begin();
}

Configurator::Transaction::~ Transaction() {
	Configurator::instance().end();
}

Configurator::Configurator() {
	TRC;
	// Make sure the report object is created in the main thread.
	Report::instance();
}

void Configurator::begin() {
	transaction_mutex.lock();
	TRC;
	// Ask all worker threads to terminate
	for (auto &worker: workers)
		worker.stop(false);
	// And wait for them to actually terminate
	for (auto &worker: workers)
		worker.stop(true);
	LOG(DBG, "Terminated workers");
	// Get rid of all the stopped workers
	workers.clear();
	worker_ids.clear();
	// Get rid of all current nfqs, so we can open new ones
	nfqs.clear();
	// Move all the current socket paths to unused, we shall pick from there as we re-add them
	socketsUnused.clear(); // It should be already empty, but in case of exception in end(), it might not
	sockets.swap(socketsUnused);
	in_transaction = true;
}

void Configurator::end() {
	TRC;
	// Create and start workers for the NFQs.
	for (auto nfq = nfqs.begin(); nfq != nfqs.end(); nfq ++)
		startThread(nfq);
	// Remove unused dissectors. Iterate manually, since dissectors.erase(X) invalidates X (but not other iterators)
	auto dispos(dissectors.cbegin());
	while (dispos != dissectors.end()) {
		auto cp(dispos);
		dispos ++;
		if (dissectorsUsed.find(cp->first) == dissectorsUsed.cend()) {
			LOG(DBG, "Removing unused dissector ", cp->first);
			dissectors.erase(cp);
		}
	}
	dissectorsUsed.clear();
	// Get rid of the sockets we no longer use
	for (const string &path: socketsUnused)
		Report::instance().removeSocket(path);
	in_transaction = false;
	transaction_mutex.unlock();
}

Dissector &Configurator::dissector(const string &name) {
	/*
	 * Make sure the dissector is marked as used (no matter if we create it in a while or just find it).
	 * If we throw during creation, it doesn't really matter, since it matters only if
	 * the name is present for those that exist in the living dissectors.
	 */
	dissectorsUsed.insert(name);
	auto found(dissectors.find(name));
	if (found == dissectors.cend()) {
		// Not here, create a new one inside
		// The emplace creates the dissector in there, without copying. A bit of hexing to make it work inside the tuple.
		auto result(dissectors.emplace(std::piecewise_construct, std::forward_as_tuple(name), std::forward_as_tuple()));
		return result.first->second;
	} else {
		return found->second;
	}
}

void Configurator::addQueue(const std::unordered_map<std::string, std::string> &tags, const std::string &dissector, size_t qsize, uint16_t queue, Family family, Direction direction) {
	TRC;
	CHECK(in_transaction);
	Dissector &d(this->dissector(dissector));
	nfqs.emplace_back(d, family, direction, qsize, queue, tags);
}

void Configurator::addSocket(const std::string &path) {
	TRC;
	if (sockets.find(path) != sockets.end())
		// Already here, don't try again
		return;
	auto found = socketsUnused.find(path);
	if (found != socketsUnused.end()) {
		LOG(DBG, "Reusing previous socket on ", path);
		// It is opened from the previous config. Move it to the used, but don't try to reopen
		socketsUnused.erase(found);
	} else {
		LOG(DBG, "Asking for a new socket on ", path);
		// We don't have that one, so get a new one
		Report::instance().addSocket(path);
	}
	sockets.insert(path);
}

std::vector<Dissector *> Configurator::allDissectors() {
	std::vector<Dissector *> result;
	for (auto it = dissectors.begin(); it != dissectors.end(); ++ it)
		result.emplace_back(&it->second);
	return result;
}

void Configurator::restartThread(uint64_t id) {
	LOG(WARN, "Restarting a thread ", id);
	std::lock_guard<std::mutex> guard(transaction_mutex);
	const auto it = worker_ids.find(id);
	if (it != worker_ids.end()) {
		// Remove the metadata from the storage
		const auto metadata = it->second;
		worker_ids.erase(it);
		// Stop the thread if it didn't stop already
		const auto &position = get<0>(metadata);
		const auto &nfq = get<1>(metadata);
		position->stop(true);
		startThread(nfq);
	} else
		LOG(WARN, "Thread ", id, " not found, not restarting");
}

void Configurator::startThread(const std::list<NFQ>::iterator &nfq) {
	workers.emplace_front(++ last_thread_id);
	LOG(INFO, "New thread ", last_thread_id);
	workers.front().addTask(nfq->fd(), bind(&NFQ::process, &*nfq));
	worker_ids[last_thread_id] = make_tuple(workers.begin(), nfq);
}

}
