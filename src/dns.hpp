/*
 * Copyright 2017, CZ.NIC z.s.p.o. (http://www.nic.cz/)
 *
 * This file is part of the pakon system.
 *
 * Pakon is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 * Pakon is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pakon.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Minimal DNS parser.
 *
 * When looking through the available DNS libraries, all are either deeply
 * integrated with something, hairy and undocumented or bloated monsters. We
 * don't need something generic providing all the functionality, we just need
 * to have a peek into a small subset of the information there.
 */

#ifndef PAKOND_DNS_H
#define PAKOND_DNS_H

#include "util.hpp"
#include "types.hpp"
#include "serialize.hpp"

#include <stdexcept>
#include <memory>
#include <unordered_map>

namespace Pakon {
namespace DNS {

enum class QR {
	Query = 0,
	Response = 1
};

enum class Opcode {
	Query = 0,
	Other
};

enum class RCode {
	NoError = 0,
	FormatError = 1,
	ServerFailure = 2,
	NameError = 3,
	NotImelemnted = 4,
	Refused = 5,
	Other
};

class ParsedName;

/*
 * A DNS name. It holds all the data inside, unlike ParsedName.
 *
 * Note that there's no constructor, it is created by a conversion from ParsedName.
 *
 * TODO: We may want (at some future time) have faster implementations. Like,
 * the hashing and comparing with strings could be done without all these
 * conversions and copying around. But get the functionality first, make it
 * faster later.
 */
class Name {
private:
	std::vector<std::string> labels_;
	friend class ParsedName;
public:
	const std::vector<std::string> &labels() const { return labels_; }
	std::string toString() const;
	/*
	 * Don't use toString here, as toString loses some parts of the
	 * information. When comparing with a string, it is already lost, so it
	 * doesn't really matter.
	 */
	bool operator ==(const Name &other) const;
	bool operator !=(const Name &other) const { return !((*this) == other); }
	bool operator ==(const std::string &other) const { return toString() == other; }
	bool operator !=(const std::string &other) const { return toString() != other; }
	bool operator ==(const ParsedName &other) const;
	bool operator !=(const ParsedName &other) const { return !((*this) == other); }
};

/*
 * A version that shares data with other ParsedNames and an underlying Blob.
 * Used when parsing.
 *
 * TODO: The same note as with Name…
 */
typedef std::shared_ptr<const ParsedName> PParsedName;
class ParsedName {
private:
	Blob label_;
	PParsedName next_;
	friend class Message;
public:
	const Blob &label() const { return label_; };
	const PParsedName &next() const { return next_; }
	// Get a copy of the name in an object that owns its data
	Name toOwned() const;
	/*
	 * Format as a string representation of DNS name.
	 *
	 * Note that this has some drawbacks and is generally incomplete ‒ for
	 * example, dots inside labels aren't handled properly. This is
	 * postponed for now and servers testing and informative purposes.
	 *
	 * It converts the name to lowercase for uniformity and eaiser
	 * handling. The data kept inside still preserve the case, so if we
	 * ever need it, we have it (but we would have to provide a getter
	 * method).
	 */
	std::string toString() const { return toOwned().toString(); }
	/*
	 * Unlike with Name, we may just convert all the comparisons to other
	 * types. We don't care about the performance too much just yet and the
	 * conversion is lossless here.
	 */
	bool operator ==(const ParsedName &other) const { return toOwned() == other.toOwned(); }
	bool operator !=(const ParsedName &other) const { return toOwned() != other.toOwned(); }
	bool operator ==(const Name &other) const { return toOwned() == other; }
	bool operator !=(const Name &other) const { return toOwned() != other; }
	bool operator ==(const std::string &other) const { return toString() == other; }
	bool operator !=(const std::string &other) const { return toString() != other; }
};

enum class Section {
	Question,
	Answer,
	Authority,
	Additional
};

enum class Class {
	IN = 1,
	CS = 2,
	CH = 3,
	HS = 4,
	Other
};

enum class Type {
	A = 1,
	CNAME = 5,
	SOA = 6,
	PTR = 12,
	MX = 15,
	AAAA = 28,
	SRV = 33,
	Other
};

// cppcheck-suppress noConstructor Yeh, that's on purpose. You don't create them, they just appear magically inside Message
class Record {
private:
	Section section_;
	Class class_;
	Type type_;
	PParsedName name_;
	uint32_t ttl_;
	Blob data_;
	friend class Message;
public:
	Section section() const { return section_; }
	Class rclass() const { return class_; }
	Type rtype() const { return type_; }
	const PParsedName &name() const { return name_; }
	uint32_t ttl() const { return ttl_; }
	const Blob &data() const { return data_; }
};

class Message {
private:
	Blob data;
	QR qr_;
	Opcode opcode_;
	RCode rcode_;
	bool authoritative_;
	bool truncated_;
	// Contains null for entries we started to work on, but haven't finished yet. Helps with cycle checks.
	typedef std::unordered_map<uint16_t, PParsedName> NameCache;
	NameCache nameCache_;
	std::vector<Record> records_;
	void sectionParse(Section section, Blob &packet, size_t cnt);
	PParsedName nameParse(Blob &packet);
public:
	// When the passed buffer isn't a valid DNS message
	EXCEPTION(std::invalid_argument, ParseError);
	/*
	 * May throw ParseError. The message is not self-contained and needs
	 * the provided Blob and its data to live for its whole lifetime.
	 *
	 * TODO: Do we want some kind of permissive mode? Like, parsing only
	 * the contained part of message, if it's cut short or ignoring extra
	 * garbage at the end?
	 */
	explicit Message(Blob packet);
	// These fields are described in the RFC 1035...
	QR qr() const { return qr_; }
	Opcode opcode() const { return opcode_; }
	bool authoritative() const { return authoritative_; }
	bool truncated() const { return truncated_; }
	RCode rcode() const { return rcode_; }
	// Iteration through the records inside
	typedef std::vector<Record>::const_iterator iterator;
	iterator begin() const { return records_.cbegin(); }
	iterator end() const { return records_.cend(); }
	size_t size() const { return records_.size(); }
	const Record &operator [](size_t idx) const { return records_[idx]; }
	// Parse a name at that position. The position must be inside the message.
	PParsedName nameParseAt(const uint8_t *position);
};

}

template<> class JSORiazible<DNS::Name> {
public:
	template<class Stream> static void jsorialize(Stream &stream, const DNS::Name &name) {
		::Pakon::jsorialize(stream, name.toString());
	}
};

template<> class JSORiazible<DNS::ParsedName> {
public:
	template<class Stream> static void jsorialize(Stream &stream, const DNS::ParsedName &name) {
		::Pakon::jsorialize(stream, name.toString());
	}
};

}

namespace std {

// Hash implementations that just convert it to string.
template<> struct hash<Pakon::DNS::Name> {
	size_t operator ()(const Pakon::DNS::Name &name) const {
		return hash<std::string>{}(name.toString());
	}
};
template<> struct hash<Pakon::DNS::ParsedName> {
	size_t operator ()(const Pakon::DNS::ParsedName &name) const {
		return hash<std::string>{}(name.toString());
	}
};

}

#endif
