/*
 * Copyright 2016, CZ.NIC z.s.p.o. (http://www.nic.cz/)
 *
 * This file is part of the pakon system.
 *
 * Updater is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 * Updater is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Updater.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "types.hpp"

#include <sstream>
#include <ios>
#include <iomanip>

using std::string;
using std::ostringstream;

namespace Pakon {

string Blob::toString() const {
	ostringstream result;
	result << std::hex << std::setfill('0');
	for (unsigned b: *this)
		// Type-cast to unsigned, otherwise it wants to print it as a char :-(
		result << std::setw(2) << b << ' ';
	return result.str();
}

Address::Address(const Blob &blob) :
	data(blob.begin(), blob.end())
{}

string Address::toString() const {
	ostringstream result;
	char sep = ':';
	size_t width = 2;
	bool alternate = false;
	bool printsep = true;
	if (data.size() == 4) {
		sep = '.';
		width = 1;
	} else
		result << std::hex << std::setfill('0');
	if (data.size() == 16)
		alternate = true;
	bool first = true;
	for (uint8_t b: data) {
		if (first)
			first = false;
		else if (printsep)
			result << sep;
		if (alternate)
			printsep = !printsep;
		result << std::setw(width) << (unsigned) b;
	}
	return result.str();
}

}
