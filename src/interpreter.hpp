/*
 * Copyright 2016, CZ.NIC z.s.p.o. (http://www.nic.cz/)
 *
 * This file is part of the pakon system.
 *
 * Pakon is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 * Pakon is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pakon.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PAKOND_INTERPRETER_H
#define PAKOND_INTERPRETER_H

#include "util.hpp"

#include <lua.hpp>

#include <mutex>
#include <string>
#include <stdexcept>

namespace Pakon {

/*
 * A lua interpreter (specialized for our needs).
 *
 * It is protected by a recursive mutex (it is possible one
 * interpreter function to get called from within another).
 *
 * It may throw exceptions from lua_common.hpp.
 */
class Interpreter : private NonCopyable {
private:
	lua_State *state = nullptr;
	std::recursive_mutex mutex;
	typedef std::lock_guard<std::recursive_mutex> mutex_guard;

	// Manipulation of callbacks, for initialization
	template<int (Interpreter::*Callback)(lua_State *)> static int callback(lua_State *state);
	template<int (Interpreter::*Callback)(lua_State *)> void insertCallback(const char *name);
	// The actual callbacks
	int queueCallback(lua_State *state);
	int listenCallback(lua_State *state);
public:
	Interpreter();
	~ Interpreter();
	/*
	 * Load a lua file and run it, with a copy of global environment.
	 */
	void doFile(const std::string &path);
	// The internal lua state for direct manipulation. Be careful with it.
	lua_State *getState() const {
		return state;
	}
};

}

#endif
