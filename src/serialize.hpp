/*
 * Copyright 2016, CZ.NIC z.s.p.o. (http://www.nic.cz/)
 *
 * This file is part of the pakon system.
 *
 * Pakon is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 * Pakon is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pakon.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PAKOND_SERIALIZE_H
#define PAKOND_SERIALIZE_H

#include "util.hpp"

#include <string>
#include <sstream>

/*
 * This does serializing of various data types to JSON. We do it here
 * ourselves, as it is a simple format to write, we don't parse it
 * and integrating a 3rd-party library is likely more work.
 *
 * Note that we never produce a newline as part of the output.
 *
 * You can either use jsorialize(stream, data), or toJSON(data).
 */

namespace Pakon {

namespace Introspectable {
template<class ...Fields> class Struct;
}

// Escape any relevant characters for a JSON string (this includes \n and \r, so this never contains a newline)
std::string jsonEscape(const std::string &str);

template<class Stream> void jsorialize(Stream &stream, const std::string &str) {
	stream << "\"" << jsonEscape(str) << "\"";
}
template<class Stream> void jsorialize(Stream &stream, const char *str) {
	jsorialize(stream, std::string(str));
}
template<class Stream> void jsorialize(Stream &stream, bool b) {
	stream << (b ? "true" : "false");
}
template<class Stream, class T> typename std::enable_if<std::is_arithmetic<T>::value>::type jsorialize(Stream &stream, const T &v) {
	stream << v;
}
template<class Stream, class Sequence, class Format> void jsonSequence(Stream &stream, const Sequence &sequence, const Format &f) {
	bool first = true;
	for (const auto &v: sequence) {
		if (first)
			first = false;
		else
			stream << ',';
		f(v);
	}
}
/*
 * As the types may call each other in arbitrary ways,
 * we provide forward declarations for the rest, so
 * each compound type can find any other.
 */
template<class Stream, class Sequence> typename std::enable_if<IsSequence<Sequence>::value || IsSet<Sequence>::value>::type jsorialize(Stream &stream, const Sequence &s);
template<class Stream, class Map> typename std::enable_if<IsMap<Map>::value>::type jsorialize(Stream &stream, const Map &m);
template<class Stream, class Ptr> typename std::enable_if<IsPtr<Ptr>::value>::type jsorialize(Stream &stream, const Ptr &ptr);
template<class Stream, class ...Fields> void jsorialize(Stream &stream, const Introspectable::Struct<Fields...> &value);
/*
 * This allows creating own serializable types, by specializing this class
 * The class must contain static void jsorialize(Stream &stream, const T &t);
 *
 * Unfortunately, C++ doesn't look up another overloaded function if it is
 * declared after the use. It works with partial specialization of a class.
 */
template<class T> class JSORiazible;
GEN_CHECK(IsJSORiazible, JSORiazible<U>::jsorialize(std::cout, std::declval<U>()), );
template<class Stream, class S> typename std::enable_if<IsJSORiazible<S>::value>::type jsorialize(Stream &stream, const S &s) {
	JSORiazible<S>::jsorialize(stream, s);
}
// We emulate sets as arrays.
template<class Stream, class Sequence> typename std::enable_if<IsSequence<Sequence>::value || IsSet<Sequence>::value>::type jsorialize(Stream &stream, const Sequence &s) {
	stream << '[';
	jsonSequence(stream, s, [&stream] (const typename Sequence::value_type &v) { jsorialize(stream, v); });
	stream << ']';
}
// Technically, this produces valid json only for string→* maps, but we won't use it for anything else, so we don't bother to check
template<class Stream, class Map> typename std::enable_if<IsMap<Map>::value>::type jsorialize(Stream &stream, const Map &m) {
	stream << '{';
	jsonSequence(stream, m, [&stream] (const typename Map::value_type &v) {
		jsorialize(stream, v.first);
		stream << ':';
		jsorialize(stream, v.second);
	});
	stream << '}';
}
template<class Stream, class Ptr> typename std::enable_if<IsPtr<Ptr>::value>::type jsorialize(Stream &stream, const Ptr &ptr) {
	if (ptr)
		jsorialize(stream, *ptr);
	else
		stream << "null";
}
// Just a helper class to serialize an introspectable struct
template<class Stream> class SerializationStep {
private:
	Stream &stream;
	bool first = true;
public:
	explicit SerializationStep(Stream &stream) : stream(stream) {}
	template<class Type, class Name, class Field> void step(const std::string &name, const Type &v) {
		if (first)
			first = false;
		else
			stream << ',';
		jsorialize(stream, name);
		stream << ':';
		jsorialize(stream, v);
	}
	template<class Type, class Name, class Field> void step(const Type &v) {
		step<Type, Name, Field>(Name::name(), v);
	}
};
template<class Stream, class ...Fields> void jsorialize(Stream &stream, const Introspectable::Struct<Fields...> &value) {
	stream << '{';
	SerializationStep<Stream> step(stream);
	value.iterate(step);
	stream << '}';
}
// Function to convert data to JSON-encoded string.
template<class T> std::string toJSON(const T &t) {
	std::ostringstream os;
	jsorialize(os, t);
	return os.str();
}

}

#endif
