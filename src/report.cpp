/*
 * Copyright 2016, CZ.NIC z.s.p.o. (http://www.nic.cz/)
 *
 * This file is part of the pakon system.
 *
 * Updater is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 * Updater is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Updater.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "report.hpp"
#include "mainthread.hpp"
#include "flow.hpp"

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <sys/epoll.h>
#include <time.h>
#include <unistd.h>
#include <fcntl.h>

using std::string;

namespace Pakon {

namespace {
// For temporary errors, like EAGAIN
EXCEPTION(std::runtime_error, TmpError);
}

class Report::UnixSocket : private NonCopyable {
private:
	int fdInternal = -1;
protected:
	UnixSocket() {
		TRC;
		CHECK((fdInternal = socket(AF_UNIX, SOCK_STREAM, 0)) != -1);
		int flags = fcntl(fdInternal, F_GETFL, 0);
		CHECK(flags >= 0);
		CHECK(fcntl(fdInternal, F_SETFL, flags | O_NONBLOCK) != -1);
	}
	explicit UnixSocket(int fd) :
		fdInternal(fd)
	{
		TRC;
	}
public:
	~ UnixSocket() {
		if (inactive)
			return;
		TRC;
		if (task)
			MainThread::global().dropTask(task);
		else if (fdInternal != -1)
			// cppcheck-suppress exceptThrowInDestructor Yeh, I know, an exception in destructor… but this one is meant to kill the program anyway
			CHECK(close(fdInternal) != -1);
	}
	int fd() const { return fdInternal; }
	// A tag, so it can be removed later on from the SPE
	const SPE::TaskHolder *task = nullptr;
	// Don't try to remove it from SPE multiple times (mutable so the thing can live in a set)
	mutable bool inactive = false;
};

class Report::ListenSocket : public UnixSocket {
public:
	ListenSocket(const string &path, Report *owner) {
		// Create the address structure
		struct sockaddr_un addr;
		addr.sun_family = AF_UNIX;
		memset(addr.sun_path, 0, sizeof addr.sun_path);
		if (path.size() + 1 > sizeof addr.sun_path)
			throw Report::PathTooLong("The path '" + path + "' is too long");
		memcpy(addr.sun_path, path.c_str(), path.size());
		if (*path.c_str()) // If it is a real-path socket, not an abstract one
			CHECK(unlink(path.c_str()) != -1 || errno == ENOENT); // If we can't remove it and it existed to start with...
		CHECK(bind(fd(), reinterpret_cast<struct sockaddr *>(&addr), sizeof addr) != -1);
		CHECK(listen(fd(), 10) != -1);
		task = MainThread::global().insertTask(fd(), std::bind(&Report::accept, owner, this));
		LOG(DBG, "Have a new listen socket ", fd());
	}
};

class Report::ReportSocket : public UnixSocket {
public:
	ReportSocket(const ListenSocket &parent, Report *owner) :
		UnixSocket(::accept(parent.fd(), nullptr, nullptr))
	{
		if (fd() == -1) {
			if (errno == EAGAIN || errno == EWOULDBLOCK || errno == ECONNABORTED || errno == EINTR)
				throw TmpError("Temporary error");
			else
				CHECK(fd() != -1);
		}
		task = MainThread::global().insertTask(fd(), std::bind(&Report::read, owner, this));
		LOG(DBG, "Have a new report socket ", fd());
		// Send a hello to the other side. We assume there's enough space in the buffer as this is a new socket. Therefore we simply block until it is sent.
		const char hello[] = "{\"jsonrpc\":\"2.0\",\"method\":\"pakon.proto-version\",\"params\":{\"version\":1}}\n";
		const char *position = hello;
		// Make sure we don't send the \0 at the end
		size_t rest = sizeof hello - 1;
		while (rest) {
			ssize_t sent = send(fd(), position, rest, MSG_NOSIGNAL);
			switch (sent) {
				case -1:
					if (errno == EPIPE) {
						// Make sure it is not included
						close(fd());
						inactive = true;
						throw TmpError("Temporary error");
					}
					if (errno == EINTR)
						continue;
					else
						CHECK(sent != -1);
					break;
				default:
					rest -= sent;
					position += sent;
					break;
			}
		}
	}
};

class Report::SocketHash {
public:
	size_t operator()(const UnixSocket &s) const { return s.fd(); }
};

class Report::SocketEq {
public:
	bool operator()(const UnixSocket &s1, const UnixSocket &s2) const { return s1.fd() == s2.fd(); }
};

class Report::Private {
public:
	std::unordered_map<string, ListenSocket> listenSockets;
	std::unordered_set<ReportSocket, SocketHash, SocketEq> reportSockets;
	std::mutex flowsMutex;
	Flows flows;
};

// Empty but private
Report::Report() :
	priv(new Private)
{}

// Generate the destructor where the destructors of sockets are known
Report::~ Report() {
	/*
	 * The report class is a singleton. There's a chance the MainThread gets destroyed
	 * first, in which case it already dropped all the tasks. Or maybe it will get
	 * destroyed soon enough. Anyway, don't destroy the tasks here and now ‒ so
	 * just drop the pointers to them.
	 */
	for (auto it = priv->listenSockets.begin(); it != priv->listenSockets.end(); ++ it)
		it->second.inactive = true;
	for (auto it = priv->reportSockets.begin(); it != priv->reportSockets.end(); ++ it)
		it->inactive = true;
}

void Report::reportFlows(const Flows &flows) {
	if (!flows.empty()) {
		TRC;
		{
			std::lock_guard<std::mutex> lock(priv->flowsMutex);
			// Copy the flows here, so we can process them
			priv->flows.insert(flows.begin(), flows.end());
		}
		MainThread::global().delay(std::bind(&Report::processFlows, this));
	}
}

void Report::processFlows() {
	// Extract them from the mutex-protected storage
	Flows flows;
	{
		std::lock_guard<std::mutex> lock(priv->flowsMutex);
		flows.swap(priv->flows);
	}
	if (priv->reportSockets.empty())
		return; // Well, nobody listening, no reason to generate the report
	if (flows.empty())
		return; // Nothing to send (why are we woken up?)
	LOG(TRACE, "Sending flows to users");
	string report;
	for (const auto &f: flows)
		report += string("{\"jsonrpc\":\"2.0\",\"method\":\"pakon.flow-update\",\"params\":") + f->toJSON() + "}\n";
	// We now use epoll to try to write to all the sockets at once, with a timeout.
	struct FD {
		struct epoll_event event;
		int fd;
		ReportSockets::iterator it;
		size_t position = 0;
		bool active = true;
		explicit FD(ReportSockets::iterator it) :
			fd(it->fd()),
			it(it)
		{
			memset(&event, 0, sizeof event);
			event.events = EPOLLOUT;
			event.data.ptr = this;
		}
	};
	int epollfd = epoll_create(42); // 42 - unused arbitrary parameter for compatbility
	CHECK(epollfd != -1);
	// Make sure the epoll is closed no matter how we exit from here
	ExitGuard fdClose([epollfd] { CHECK(close(epollfd) != -1); });
	std::deque<FD> fds;
	for (auto it = priv->reportSockets.begin(); it != priv->reportSockets.end(); it ++) {
		fds.emplace_back(it);
		CHECK(epoll_ctl(epollfd, EPOLL_CTL_ADD, it->fd(), &fds.back().event) != -1);
	}
	size_t active_count(fds.size());
	uint64_t now = timeMsec(CLOCK_MONOTONIC);
	uint64_t end = now + 3000; // We provide 3 seconds to send everything
	while (active_count > 0 && end >= now) {
		const size_t max_events = 10;
		struct epoll_event events[max_events];
		int result = epoll_wait(epollfd, events, max_events, end - now);
		now = timeMsec(CLOCK_MONOTONIC);
		if (result == -1) {
			CHECK(errno == EINTR);
			result = 0;
		}
		for (int i = 0; i < result; i ++) {
			// Try to send the rest of the message there
			FD *fd = static_cast<FD *>(events[i].data.ptr);
			if (!fd->active)
				// Likely some kind of racish thing. It is possible this never happens in practice.
				continue;
			ssize_t sent = send(fd->fd, report.c_str() + fd->position, report.size() - fd->position, MSG_DONTWAIT | MSG_NOSIGNAL);
			if (sent == -1) {
				if (errno == ECONNRESET || errno == EPIPE) {
					LOG(DBG, "File descriptor ", fd->fd, " closed while writing to it");
					/*
					 * It has been closed by the other end.
					 * We don't close it here, we leave it to the reading part.
					 * But we want to take it out of our statistics and out of
					 * epoll.
					 */
					fd->active = false;
					-- active_count;
					CHECK(epoll_ctl(epollfd, EPOLL_CTL_DEL, fd->fd, nullptr) != -1);
					continue;
				}
				CHECK(errno == EAGAIN || errno == EWOULDBLOCK);
				continue;
			}
			fd->position += sent;
			if (fd->position == report.size()) {
				// We sent everything, remove from epoll
				fd->active = false;
				-- active_count;
				CHECK(epoll_ctl(epollfd, EPOLL_CTL_DEL, fd->fd, nullptr) != -1);
			}
		}
	}
	// Go through the still active sockets. These didn't eat up everything in the given, very generous, timeout. So kill them.
	for (const auto &fd: fds)
		if (fd.active) {
			LOG(WARN, "Killing report socket ", fd.fd, " due to timeout on write (", now, " ", end, " ", active_count, ")");
			// It gets closed and removed from the main epoll in its descructor
			priv->reportSockets.erase(fd.it);
		}
}

void Report::addSocket(const string &path) {
	priv->listenSockets.emplace(std::piecewise_construct, std::forward_as_tuple(path), std::forward_as_tuple(path, this));
}

void Report::removeSocket(const string &path) {
	// The destruction also closes the socket and removes it from the main thread
	priv->listenSockets.erase(path);
}

bool Report::accept(ListenSocket *socket) {
	try {
		priv->reportSockets.emplace(*socket, this);
	} catch (const TmpError &) {
		LOG(WARN, "Spurious accept on ", socket->fd());
	}
	// We can accept forever
	return true;
}

/*
 * We want to empty the input buffer if anything sends stuff (but ignore it). But most importantly, we want
 * to know when the socket is closed.
 */
bool Report::read(ReportSocket *socket) {
	const size_t bufsize = 1024;
	uint8_t buffer[bufsize];
	ssize_t result = recv(socket->fd(), &buffer, bufsize, MSG_DONTWAIT);
	switch (result) {
		case -1: {
			if (errno == EAGAIN || errno == EWOULDBLOCK || errno == EINTR)
				// Some temporary problem
				return true;
			LOG(WARN, "Error on socket ", socket->fd(), ": ", strerror(errno));
			// Fall through to closing of the socket
		}
		case 0:
			LOG(DBG, "Closing report socket ", socket->fd());
			priv->reportSockets.erase(*socket);
			// Since we already deleted the task by the destructor, don't try doing so again by returning false
			return true;
		// Other values are fine, just throw away the data
	}
	return true;
}

}
