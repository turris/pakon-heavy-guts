/*
 * Copyright 2016, CZ.NIC z.s.p.o. (http://www.nic.cz/)
 *
 * This file is part of the pakon system.
 *
 * Pakon is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 * Pakon is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pakon.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PAKOND_NFQ_H
#define PAKOND_NFQ_H

#include "util.hpp"
#include "packet.hpp"
#include "types.hpp"

#include <string>
#include <unordered_map>
#include <sys/socket.h>

struct nfq_handle;
struct nfq_q_handle;
struct nfgenmsg;
struct nfq_data;

namespace Pakon {

class Dissector;

class NFQ : private NonCopyable {
private:
	/*
	 * How long packets do we take from the kernel?
	 * The rest of the message gets truncated.
	 *
	 * While we can cope with truncated packet (by filling it in with zeroes), it
	 * is not optimal and we want to avoid it in the usual case. The common maximal
	 * size of packet seen in the wild is 4096 (for DNS packets). However, field
	 * observation suggests the netfilter header is between 44 and 80 bytes long,
	 * so we add a bit more to make sure these DNS packets fit.
	 */
	static const size_t pktSize = 4196;
	// How many packets to receive at once at most
	static const size_t batchSize = 50;
	// NFQ buffer size
	static const size_t bufSize = 16 * 1024 * 1024;
	static int packetCback(struct nfq_q_handle *h, struct nfgenmsg *nfmsg, struct nfq_data *data, void *self);
	Dissector &dissector;
	const std::unordered_map<std::string, std::string> flags;
	const Direction direction;
	struct nfq_handle *handle = nullptr;
	struct nfq_q_handle *queue = nullptr;
	std::vector<Packet> packets; // Current batch of packets
	// Current cache timestamp, when processing the packets
	uint64_t timestamp, timestamp_monotonic;
	int fdInternal;
	// The following is prepared to receive data by recvmmsg and reused.
	// Buffer to receive packets into
	uint8_t buffer[pktSize * batchSize];
#ifdef HAVE_RECVMMSG
	// The headers for recvmmsg
	std::vector<struct mmsghdr> recvHeaders;
#endif
	// The vectors for receiving scatter-gather by recvmmsg
	std::vector<struct iovec> recvIovs;
	int packet(struct nfq_data *data);
public:
	/*
	 * Create the NFQ handler.
	 * family is either AF_INET or AF_INET6.
	 * qsize is the number of packets that can be held in the queue.
	 * queueNum is the number as used in iptables.
	 * name is an identifier, that shall be embedded in the flows introduced here.
	 * direction is the direction the packets would go (in or out).
	 */
	NFQ(Dissector &dissector, Family family, Direction direction, size_t qsize, uint16_t queueNum, const std::unordered_map<std::string, std::string> &flags);
	~ NFQ();
	// The file descriptor of the interface to kernel
	int fd() const;
	/*
	 * Process some packets in the queue (non-blocking).
	 * Return false if there is no way to continue on this queue
	 * (Can this actually happen? The kernel closing it on us?)
	 */
	bool process();
};

}

#endif
