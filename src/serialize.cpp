/*
 * Copyright 2016, CZ.NIC z.s.p.o. (http://www.nic.cz/)
 *
 * This file is part of the pakon system.
 *
 * Pakon is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 * Pakon is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pakon.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "serialize.hpp"

#include <iomanip>
#include <map>

using std::string;

namespace Pakon {

string jsonEscape(const string &str) {
	string result;
	for (char c: str) {
		if (c == '\"' || c == '\\' || c <= 0x1F) {
			result += "\\u";
			std::ostringstream s;
			s << std::hex << std::setfill('0') << std::setw(4) << static_cast<int>(c);
			result += s.str();
		} else
			result += c;
	}
	return result;
}

}
