/*
 * Copyright 2016, CZ.NIC z.s.p.o. (http://www.nic.cz/)
 *
 * This file is part of the pakon system.
 *
 * Updater is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 * Updater is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Updater.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "nfq.hpp"
#include "dissect.hpp"
#include "report.hpp"

#include <libnetfilter_queue/libnetfilter_queue.h>
#include <libnfnetlink/libnfnetlink.h>
#include <netinet/in.h>
#include <linux/netfilter.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>

namespace Pakon {

NFQ::NFQ(Dissector &dissector, Family family, Direction direction, size_t qsize, uint16_t qnum, const std::unordered_map<std::string, std::string> &flags) :
	dissector(dissector),
	flags(flags),
	direction(direction),
#ifdef HAVE_RECVMMSG
	recvHeaders(batchSize),
#endif
	recvIovs(batchSize)
{
	TRC;
	CHECK(handle = nfq_open());
	int familyRaw = (family == Family::IPv4 ? AF_INET : AF_INET6);
	CHECK(nfq_unbind_pf(handle, familyRaw) >= 0);
	CHECK(nfq_bind_pf(handle, familyRaw) >= 0);
	CHECK(queue = nfq_create_queue(handle, qnum, packetCback, this));
	CHECK(nfq_set_mode(queue, NFQNL_COPY_PACKET, pktSize) >= 0);
	CHECK(nfq_set_queue_maxlen(queue, qsize) >= 0);
	fdInternal = nfq_fd(handle);
	// Set some socket options to improve performance
	int opts = 1;
	CHECK(setsockopt(fdInternal, SOL_NETLINK, NETLINK_NO_ENOBUFS, &opts, sizeof opts) == 0);
	CHECK(nfnl_rcvbufsiz(nfq_nfnlh(handle), bufSize) > 0);
	// Prepare data to call recvmmsg (this data is reused across the calls)
	for (size_t i = 0; i < batchSize; i ++) {
		recvIovs[i].iov_len = pktSize;
		recvIovs[i].iov_base = buffer + i * pktSize;
#ifdef HAVE_RECVMMSG
		recvHeaders[i].msg_hdr.msg_name = nullptr;
		recvHeaders[i].msg_hdr.msg_namelen = 0;
		recvHeaders[i].msg_hdr.msg_iov = &recvIovs[i];
		recvHeaders[i].msg_hdr.msg_iovlen = 1;
		recvHeaders[i].msg_hdr.msg_control = nullptr;
		recvHeaders[i].msg_hdr.msg_controllen = 0;
		// We have to reset this after each call, unfortunately :-(, as it is set
		recvHeaders[i].msg_hdr.msg_flags = 0;
#endif
	}
}

NFQ::~NFQ() {
	TRC;
	if (queue)
		nfq_destroy_queue(queue);
	if (handle)
		nfq_close(handle);
}

int NFQ::fd() const { return fdInternal; }

namespace {

/*
 * Check that the code to compare if one ID is after another in the
 * ID sequence actually works around overflows. Because that typecasting
 * to int is actually implementation-defined (and we hope the c++ compiler
 * does the same as the c one that compiled the kernel, since both is gcc).
 */
constexpr bool after(uint32_t id, uint32_t max) {
	return (int32_t)(id - max) > 0;
}
static_assert(after(0, 0xffffffff), "Possibly broken ID comparism in kernel on this architecture :-(");

}

bool NFQ::process() {
	TRC;
	/*
	 * Make sure the packets don't survive out of this function.
	 * They reference buffers reused on the next call.
	 */
	ExitGuard guard([this]{ this->packets.clear(); });
	std::vector<std::unique_ptr<char[]>> bigPktBuff; // Buffer to copy big packets into and fill with zeroes
	timestamp = timeMsec();
	timestamp_monotonic = timeMsec(CLOCK_MONOTONIC);
#ifdef HAVE_RECVMMSG
	int result = recvmmsg(fdInternal, &recvHeaders[0], recvHeaders.size(), MSG_DONTWAIT | MSG_TRUNC, NULL);
	switch (result) {
		case -1:
			// These errors are allowed, try again next time.
			CHECK(errno == EAGAIN || errno == EWOULDBLOCK || errno == EINTR || errno == ENOBUFS);
			LOG(DBG, "Retry error");
			return true;
		case 0:
			LOG(DBG, "No packets read");
			// Closed by kernel? Can it actually happen?
			return true;
		default:
			// Handle the packets below
			break;
	}
	LOG(TRACE, "Received ", result, " packets");
	// Process the received messages and convert them to packets
	for (int i = 0; i < result; i ++) {
		char *data = static_cast<char *>(recvIovs[i].iov_base);
		if (recvHeaders[i].msg_hdr.msg_flags & MSG_TRUNC) {
			LOG(INFO, "Truncated packet, original ", recvHeaders[i].msg_len, " bytes");
			// Copy the packet somewhere and fill the rest with zeroes
			size_t size = recvHeaders[i].msg_len;
			bigPktBuff.emplace_back(new char[size]);
			data = bigPktBuff.back().get();
			memcpy(data, recvIovs[i].iov_base, pktSize);
			memset(data + pktSize, 0, size - pktSize);
		}
		recvHeaders[i].msg_hdr.msg_flags = 0;
		/*
		 * This function sometimes fails for an unknown reason. We just
		 * log it ‒ the packet will get handled together with some
		 * other one in a batch. That is wrong, but hopefully one
		 * packet won't do anything bad.
		 *
		 * In the meantime we hope this logging will shed some light
		 * into why the function might want to fail from time to time.
		 */
		int result = nfq_handle_packet(handle, data, recvHeaders[i].msg_len);
		if (result != 0)
			LOG_DUMP(ERROR, "Failed to handle a packet: ", result, "/", Blob(data, recvHeaders[i].msg_len).toString());
	}
#else
	for (size_t i = 0; i < batchSize; i ++) {
		struct msghdr hdr;
		hdr.msg_name = nullptr;
		hdr.msg_namelen = 0;
		hdr.msg_iov = &recvIovs[i];
		hdr.msg_iovlen = 1;
		hdr.msg_control = nullptr;
		hdr.msg_controllen = 0;
		hdr.msg_flags = 0;
		int result = recvmsg(fdInternal, &hdr, MSG_DONTWAIT | MSG_TRUNC);
		if (result == -1) {
			CHECK(errno == EAGAIN || errno == EWOULDBLOCK || errno == EINTR || errno == ENOBUFS);
			// OK, no more packets for now, try again later
			break;
		}
		char *data = static_cast<char *>(recvIovs[i].iov_base);
		size_t size = result;
		if (hdr.msg_flags & MSG_TRUNC) {
			LOG(INFO, "Truncated packet, original ", result, " bytes");
			// Copy the packet somewhere and fill the rest with zeroes
			bigPktBuff.emplace_back(new char[size]);
			data = bigPktBuff.back().get();
			memcpy(data, recvIovs[i].iov_base, pktSize);
			memset(data + pktSize, 0, size - pktSize);
		}
		result = nfq_handle_packet(handle, data, size);
		if (result != 0)
			LOG_DUMP(ERROR, "Failed to handle a packet: ", result, "/", Blob(data, size).toString());
	}
#endif
	// Process the packets and assign flows
	const Flows &flows(dissector.process(packets));
	// TODO: Process the flows further ‒ through lua, DNS, send them to the socket…
	/*
	 * Push the packet verdicts into kernel. Construct runs of consequtive
	 * packets with the same verdicts and push them all at once.
	 *
	 * Note that the documentation about the batch verdict is incorrect
	 * and kernel does overflow-resistant comparism of IDs ‒ so we actually
	 * want to provide the *last* ID, not the *highest*. Yes, this really
	 * returns true for nfq_id_after(0, 0xffffffff) ‒ this comes from the kernel
	 * source code:
	 *
	 * static int nfq_id_after(unsigned int id, unsigned int max)
	 * {
	 *   return (int)(id - max) > 0;
	 * }
	 */
	bool doBatch = false; // Anything to put in there
	uint32_t lastId;
	std::pair<uint32_t, uint32_t> lastVerdict;
	for (const Packet &p: packets) {
		// Extract the flow, its verdict and combine the mask of the flow with what the packet already has
		const PFlow &f(p.field<PKT_flow>());
		const Verdict &v(f->verdict());
		uint32_t mask(v.mark_mask);
		const std::pair<uint32_t, uint32_t> currentVerdict(v.action, (p.field<PKT_mark>() & ~mask) | (v.mark_value & mask));
		if (!doBatch || currentVerdict != lastVerdict) {
			// If there's a change
			if (doBatch) {
				LOG(TRACE, "Setting batch verdict up to packet ", lastId);
				nfq_set_verdict_batch2(queue, lastId, std::get<0>(lastVerdict), std::get<1>(lastVerdict));
			}
			doBatch = true;
			lastVerdict = currentVerdict;
		}
		lastId = p.field<PKT_id>();
	}
	if (doBatch) {
		LOG(TRACE, "Setting batch verdict up to packet ", lastId);
		nfq_set_verdict_batch2(queue, lastId, std::get<0>(lastVerdict), std::get<1>(lastVerdict));
	}
	// Send the interesting flows to the interested other programs
	if (!flows.empty())
		Report::instance().reportFlows(flows);
	return true;
}

int NFQ::packet(struct nfq_data *data) {
	try {
		packets.emplace_back(data, flags, direction, timestamp, timestamp_monotonic);
	} catch (const Packet::Unparsable &) {
		LOG(WARN, "Unparsable packet");
	}
	return 0;
}

int NFQ::packetCback(struct nfq_q_handle *, struct nfgenmsg *, struct nfq_data *data, void *self) {
	TRC;
	return static_cast<NFQ *>(self)->packet(data);
}

}
