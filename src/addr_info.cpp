/*
 * Copyright 2017, CZ.NIC z.s.p.o. (http://www.nic.cz/)
 *
 * This file is part of the pakon system.
 *
 * Pakon is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 * Pakon is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pakon.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "addr_info.hpp"

using std::vector;

namespace Pakon {

// TODO: Make this configurable?
constexpr size_t maxAddrInfoCount = 65536;

/*
 * TODO: These names methods aren't tested. Besides having to implement testing
 * constructors for types that are currently only parsed from packets, we would
 * need a way to mock time flow to be able to write some even remotely
 * reasonable tests, which is hard in C++ :-(.
 */

void AddrInfo::pruneNames() {
	/*
	 * Don't lock ‒ we are a private method and expect to be already locked
	 * by now.
	 */
	const uint64_t now = timeGrab(CLOCK_MONOTONIC);
	/*
	 * The spec does state that erasure invalidates only the one deleted
	 * iterator. However, it states the relative order of others is
	 * unaffected only since C++14 and we use C++11. This suggests the
	 * order could be changed. Therefore, we prepare the ones to delete upfront.
	 *
	 * The first guarantee allows us to not restart during the erasure when
	 * we have all the iterators ready.
	 */
	vector<NameSet::iterator> to_delete;
	for (auto it = names_.begin(); it != names_.end(); it ++) {
		if (it->second < now)
			to_delete.emplace_back(it);
	}
	/*
	 * If we intend to delete the last entry, bail out. For our use, having
	 * stale data is likely better than having none.
	 */
	if (to_delete.size() == names_.size())
		return;
	for (const auto &it: to_delete)
		names_.erase(it);
}

void AddrInfo::touchName(const DNS::Name &name, uint32_t ttl) {
	Guard lock(mutex);
	/*
	 * It expires ttl seconds from now, but not sooner than after 15 ‒ see
	 * the comment at the method description why.
	 */
	const uint64_t expires = timeGrab(CLOCK_MONOTONIC) + std::max(ttl, static_cast<uint32_t>(15));
	const auto inserted = names_.insert({name, expires});
	if (!inserted.second) {
		// Already present, only update the TTL there
		inserted.first->second = std::max(inserted.first->second, expires);
	}
	// Drop any other expired names
	pruneNames();
}

vector<DNS::Name> AddrInfo::names() {
	Guard lock(mutex);
	// Drop the old ones first
	pruneNames();
	// And copy the names (we can't reference them anyway for thread safety)
	vector<DNS::Name> result;
	result.reserve(names_.size());
	for (const auto &n: names_)
		result.emplace_back(n.first);
	return result;
}

AddrInfoCache addrInfoCache(maxAddrInfoCount);

}
