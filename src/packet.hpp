/*
 * Copyright 2016, CZ.NIC z.s.p.o. (http://www.nic.cz/)
 *
 * This file is part of the pakon system.
 *
 * Pakon is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 * Pakon is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pakon.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PAKOND_PACKET_H
#define PAKOND_PACKET_H

#include "util.hpp"
#include "flow.hpp"

struct nfq_data;

namespace Pakon {

// Named Field
#define NF(NAME) struct PKT_##NAME { static constexpr const char *name() { return #NAME; } }

// The NFQ id
NF(id);
// The original mark of the packet
NF(mark);
// The flow it is assigned to
NF(flow);
// The queue flags
NF(qflags);
// The protocol level
NF(level);
// The raw value of IP protocol (TCP/UDP/…)
NF(ip_proto);
// ports (either TCP or UDP)
NF(ports);
// The TCP flags field
NF(tcp_flags);
// Anything inside all the protocols we decoded
NF(payload);
// Direction of the packet
NF(direction);
// The timestamp of the packet
NF(timestamp);
NF(timestamp_monotonic); // One that doesn't jump when clock is modified

#undef NF

using Introspectable::Field;
using Introspectable::Simple;
using Introspectable::Multiple;
using Introspectable::BatchEmpty;
using Introspectable::Optional;
using Introspectable::Explicit;

/*
 * One level of protocol. Usually, packet has a MAC level, then IP (v4/6) level.
 * However, there may be a tunnel, therefore there may be multiple IP levels,
 * for example. There it can have a TCP/UDP level on top of that (there's other
 * info about TCP/UDP in the packet itself, but this is used during the flow
 * identification).
 */
struct ProtoLevel {
	enum Type {
		Ethernet,
		IPv4,
		IPv6,
		TCP,
		UDP,
		// Some other sub-protocol of IP. Both addresses contain the protocol number
		Unknown
	} type;
	Address src, dst;
};

// Constants for TCP flags
enum TcpFlags {
	FIN = 1 << 0,
	SYN = 1 << 1,
	RST = 1 << 2,
	PSH = 1 << 3,
	ACK = 1 << 4,
	URG = 1 << 5,
	ECE = 1 << 6,
	CWR = 1 << 7,
	NS = 1 << 8,
};

/*
 * Simple container for parsed info about a single packet.
 * Note that it references the data passed to it, so
 * the buffer the packet was read into must stay alive
 * for the whole lifetime of this object.
 */
class Packet : public Introspectable::Struct<
	Field<uint32_t, Simple, PKT_id>,
	Field<uint32_t, Simple, PKT_mark>,
	// Assigned by a dissector
	Field<PFlow, Simple, PKT_flow>,
	// Parsed from the data
	Field<Direction, Simple, PKT_direction>,
	Field<ProtoLevel, Multiple<BatchEmpty>, PKT_level>,
	Field<unsigned, Simple, PKT_ip_proto>,
	// Src & dst port (only on UDP & TCP packets)
	Field<std::pair<uint16_t, uint16_t>, Optional<Explicit>, PKT_ports>,
	// TCP flags number (you need to get the correct flags yourself)
	Field<uint16_t, Optional<Explicit>, PKT_tcp_flags>,
	// The timestamp of the packet, in milliseconds (since the epoch)
	Field<uint64_t, Simple, PKT_timestamp>,
	Field<uint64_t, Simple, PKT_timestamp_monotonic>, // The same, but doesn't jump and the epoch likely isn't 1.1.1970
	// The content of the package
	Field<Blob, Simple, PKT_payload>,
	/*
	 * The flags from the queue the packet has been received on.
	 * As the packet is owned by the queue object and never survives
	 * its destruction, it is OK to reference the flags in there
	 * instead of copying them.
	 */
	Field<const std::unordered_map<std::string, std::string> *, Simple, PKT_qflags>
> {
private:
	size_t data_len;
	const uint8_t *data;
	// Precomputed value of flowMatch.
	std::string flowMatchValue;
	void flowMatchCompute();
	void parse();
public:
	EXCEPTION(std::runtime_error, Unparsable);
	Packet(struct nfq_data *data, const std::unordered_map<std::string, std::string> &flags, Direction direction, uint64_t timestamp, uint64_t timestamp_monotonic);
	/*
	 * Packet parser for *testing purposes*. Does only the parsing and a
	 * lot of the fields are left blank.
	 */
	Packet(const uint8_t *data, size_t size);
	/*
	 * An identifier of a flow. This is some binary data
	 * (all the relevant addresses and protocol types
	 * concatenated together), so it is an abuse of the std::string.
	 * However, that one can be hashed, so it is handy.
	 *
	 * TODO: We may want to consider creating our own (thinner)
	 * object with custom hashing (precomputed, probably) and custom
	 * comparing mechanisms. We would avoid hashing it multiple times
	 * (which we currently do) and construction of that string, which
	 * is just a copy of data we already have.
	 *
	 * On the other hand, the object probably may need to be copiable
	 * somehow (a trick to copy the data only during a copy-construction
	 * and reuse it under normal construction?).
	 */
	const std::string &flowMatch() const;
	size_t size() const { return data_len; }
};

}

#endif
