/*
 * Copyright 2016, CZ.NIC z.s.p.o. (http://www.nic.cz/)
 *
 * This file is part of the pakon system.
 *
 * Pakon is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 * Pakon is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pakon.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PAKOND_CACHE_H
#define PAKOND_CACHE_H

#include "util.hpp"

#include <mutex>
#include <atomic>
#include <unordered_set>
#include <list>
#include <map>
#include <vector>
#include <cassert>

namespace Pakon {

/*
 * This is a class a bit similar to std::unordered_map in a sense it maps
 * keys to values. However, there are several differences:
 * • Many of the operations are thread-safe.
 * • The values can have TTLs and if cleanupTTL is called, the values
 *   with smaller TTL than passed are dropped and returned through
 *   deleted().
 * • There may be a limit on the number of items stored. If it is reached,
 *   some of the items are removed using LRU. The LRU can be used either
 *   implicitly (by each access) or explicitly (by a touch method).
 *
 * The purpose is to cache something, like reverse DNS, and access it from multiple
 * threads.
 *
 * There are no const versions of the operations, simply because we don't need
 * them for now.
 *
 * Note that other thread may cause the item to disappear from the cache at any time.
 * However, as long as there's an iterator active, the value is not destroyed and
 * can be manipulated.
 *
 *
 * Implementation notes:
 * The hash table is split into segmentCount segments and each one is locked separately.
 * Each one keeps its own LRU and TTL tree. However, for the LRU to work, we also
 * keep global counter and assign "timestamps".
 *
 * This does *NOT* provide any exception guarantees with regards to allocation-related
 * exceptions. If you get bad_alloc during the use of this, you're on your own.
 */
template<class Key, class Value, size_t segmentCount = 5, bool keepDeleted = false, bool sizeLimitEnabled = true, bool implicitLRU = true, class TTL = uint64_t> class Cache : private NonCopyable {
private:
	typedef std::lock_guard<std::mutex> Guard;
	struct Node;
	typedef std::shared_ptr<Node> PNode;
	typedef std::list<PNode> LRU;
	typedef std::multimap<TTL, PNode> TTLMap;
	/*
	 * Each node holds the precomputed hash, the value and various
	 * accounting information.
	 *
	 * It holds an index in its bucket, it holds LRU iterator (if
	 * we have LRU active) and the TTL position (if TTL has been set
	 * for the item). These are updated on any corresponding changes.
	 */
	struct Node : private NonCopyable {
		const size_t hash;
		Value value;
		bool alive = true;
		bool hasTTL = false;
		typename TTLMap::iterator ttlPos;
		size_t buckPos = 0; // Position inside the bucket (the bucket can be computed from the hash)
		typename LRU::iterator lruPos; // Position in the LRU
		uint32_t lruTime = 0; // A "time" so we can merge the several LRUs together
		template<class ...Params> Node(size_t hash, Params &&...params) :
			hash(hash),
			value(std::forward<Params>(params)...)
		{ }
	};
	// We store the key in the bucket, easier to access than going through the pointer.
	typedef std::vector<std::pair<Key, PNode>> Bucket;
	// One segment with multiple buckets. Each segment may have different number of buckets.
	struct Segment {
		std::mutex mutex;
		std::vector<Bucket> buckets;
		/*
		 * We have a separate LRU for each segment. That makes it faster to „touch“ the
		 * elements, but a bit harder during the cleanup. We also assign a „timestamp“
		 * to each element when touched, making it possible to compare the segment's lrus
		 * and pick the one segment to delete the oldest item from.
		 */
		LRU lru;
		// Map, sorted by the TTL, so we can just iterate from the front.
		TTLMap ttlMap;
		size_t itemCount = 0;
		Segment() :
			buckets(3) // Start with 3 empty buckets
		{ }
	};
	Segment segments[segmentCount];
	const size_t sizeLimit, cleanupSize;
	std::atomic<size_t> itemCount {0};
	static std::hash<Key> hasher;
	static std::hash<PNode> nodeHasher;
	// Generator of the timestamps for LRUs. It can wrap around and it is handled during the comparison.
	std::atomic<uint32_t> lruTime = {0};
	// A mutex for some global structures. It is not used during most of the operations.
	std::mutex globalMutex;
public:
	// A public interface to the node pointer
	class iterator {
	private:
		PNode node;
		friend class Cache;
	public:
		iterator() = default;
		explicit iterator(const PNode &node) :
			node(node)
		{}
		bool operator ==(const iterator &other) const {
			return node == other.node;
		}
		Value &operator*() { return node->value; }
		Value *operator->() { return &node->value; }
		const Value &operator*() const { return node->value; }
		const Value *operator->() const { return &node->value; }
		bool valid() const {
			return node.operator bool();
		}
		operator bool() const { return valid(); }
	};
	// So the iterators may be hashed
	struct ItHash {
		size_t operator()(const iterator &it) const { return nodeHasher(it.node); }
	};
	// The maximum size (if sizeLimitEnabled) and how many items to clean up once clean up starts
	Cache(size_t sizeLimit = 0, size_t cleanupSize = 100) :
		sizeLimit(sizeLimit),
		cleanupSize(cleanupSize)
	{
		// There's enough room for the circular arithmetic to work even if we're full
		assert(sizeLimit < 0xffffffff / 4);
		assert(!sizeLimitEnabled || sizeLimit > cleanupSize);
	}
	/*
	 * Look up a value by the key. Returns end() if not present.
	 */
	iterator find(const Key &key) {
		// First pick the segment and lock it
		size_t hash = hasher(key);
		Segment &segment(segments[hash % segmentCount]);
		Guard lock(segment.mutex);
		// Then look into the right bucket and try finding the value there
		Bucket &bucket(segment.buckets[hash % segment.buckets.size()]);
		for (const auto ptr: bucket)
			if (key == ptr.first)
				return iterator(ptr.second);
		return iterator();
	}
private:
	// This compares them with overflow arithmetis ‒ when the IDs overflow, the new 0 is larger then the last maxint
	static constexpr bool lruLT(uint32_t _1, uint32_t _2) {
		return (int32_t)(_1 - _2) < 0;
	}
	// As it relies on concrete signed implementation, check it actually works on this architecture.
	static_assert(lruLT(0xffffffff, 0), "Circular arithmetic compare broken");
	/*
	 * Removes an item. Expects the corresponding segment to be already locked. If storeDeleted && keepDeleted, the global mutex needs to be locked too.
	 * It removes it from the corresponding bucket and all the auxiliary accounting structures.
	 * It may still be kept in memory by an iterator, but it'll never again be looked up
	 * and all the touch and similar methods do nothing.
	 */
	void eraseInternal(const PNode &pnode, bool storeDeleted) {
		// If we store the deleted items, do so
		if (keepDeleted && storeDeleted)
			deleted_.emplace(pnode);
		Node &node(*pnode);
		node.alive = false;
		Segment &segment(segments[node.hash % segmentCount]);
		Bucket &bucket(segment.buckets[node.hash % segment.buckets.size()]);
		// Modify the statistics & LRU & ttl Map
		-- itemCount;
		-- segment.itemCount;
		if (sizeLimitEnabled)
			segment.lru.erase(node.lruPos);
		if (node.hasTTL)
			segment.ttlMap.erase(node.ttlPos);
		// Move the last item in its position and drop the last one
		bucket[node.buckPos].swap(bucket[bucket.size() - 1]);
		bucket[node.buckPos].second->buckPos = node.buckPos; // The last one moved ‒ update the index
		bucket.pop_back(); // This may actually delete it if it's the last reference (invalidating the node variable)
	}
	// Go through the LRU and erase enough items so we have only sizeLimit - cleanupSize
	void cleanupLRU() {
		TRC;
		assert(sizeLimitEnabled);
		// Lock everything (in the order global → sequence of segments, so we avoid possible deadlocks)
		std::list<Guard> guards;
		guards.emplace_back(globalMutex);
		for (Segment &s: segments)
			guards.emplace_back(s.mutex);
		const size_t low = sizeLimit - cleanupSize;
		while (itemCount > low) {
			uint32_t best = 0;
			PNode found;
			for (Segment &s: segments)
				if (!s.lru.empty() && (!found || lruLT(s.lru.front()->lruTime, best))) {
					found = s.lru.front();
					best = found->lruTime;
				}
			assert(found);
			eraseInternal(found, true);
		}
	}
	// Move the given node to the back of the LRU. Expects the segment to be already locked.
	void touchInternal(Segment &segment, const PNode &node) {
		assert(sizeLimitEnabled);
		// Assign a new LRU time and move it to the back of the LRU
		node->lruTime = lruTime ++;
		LRU &l(segment.lru);
		l.splice(l.end(), l, node->lruPos); // Abuse splice to steal the item from itself, without re-creating it.
	}
public:
	/*
	 * Look up or create an item.
	 * Returns the iterator for the value and bool specifying if a new
	 * value was created.
	 *
	 * It is inserted at the back of the LRU and has no TTL set (so it won't time out).
	 *
	 * The result is:
	 * • Iterator for the value
	 * • Was it inserted anew?
	 * • Were some other values deleted?
	 */
	template<class ...Params> std::tuple<iterator, bool, bool> access(const Key &key, Params &&...params) {
		// First, look up the correct segment and lock it.
		size_t hash = hasher(key);
		Segment &segment(segments[hash % segmentCount]);
		iterator i;
		{
			Guard lock(segment.mutex);
			// Now look up the bucket and find the correct value, if present
			Bucket &bucket(segment.buckets[hash % segment.buckets.size()]);
			for (const auto &ptr: bucket)
				if (key == ptr.first) {
					if (sizeLimitEnabled && implicitLRU)
						touchInternal(segment, ptr.second);
					return std::make_tuple(iterator(ptr.second), false, false);
				}
			LOG(TRACE, "Adding new item to the cache");
			bucket.emplace_back(std::make_pair(key, PNode(new Node(hash, std::forward<Params>(params)...))));
			i = iterator(bucket.back().second);
			i.node->buckPos = bucket.size() - 1;
			// It is rather full, do a re-hash of this segment
			if (++ segment.itemCount >= segment.buckets.size()) {
				LOG(TRACE, "Rehash");
				// TODO: Some better selection of the size ‒ a prime number?
				size_t s = segment.buckets.size() * 2 + 1;
				std::vector<Bucket> newBuckets(s);
				for (const auto &b: segment.buckets)
					for (const auto &n: b) {
						Node &node(*n.second);
						size_t buckNum(node.hash % s);
						newBuckets[buckNum].emplace_back(n);
						node.buckPos = newBuckets[buckNum].size() - 1;
					}
				segment.buckets.swap(newBuckets);
			}
			++ itemCount;
			// Insert into LRU
			if (sizeLimitEnabled) {
				i.node->lruTime = lruTime ++;
				i.node->lruPos = segment.lru.emplace(segment.lru.end(), i.node);
			}
		}
		bool cleanedUp = false;
		if (sizeLimitEnabled && itemCount > sizeLimit) {// This can cause the cleanup to be called from multiple threads, but that's not a big problem
			cleanupLRU(); // The cleanup is done outside of the mutex, as it locks everything itself. We have to start locking from the beginning, otherwise we could get a deadlock.
			cleanedUp = true;
		}
		return std::make_tuple(i, true, cleanedUp);
	}
	// Move the item to the back of LRU (so it won't get deleted as soon)
	void touch(const iterator &it) {
		if (!it)
			return;
		Node &node(*it.node);
		Segment &segment(segments[node.hash % segmentCount]);
		Guard lock(segment.mutex);
		if (!node.alive)
			return;
		touchInternal(segment, it.node);
	}
	void touch(const Key &key) {
		touch(find(key));
	}
	// Set a TTL value of an item (absolute value)
	void ttl(const iterator &it, const TTL &ttl) {
		if (!it)
			return;
		Node &node(*it.node);
		Segment &segment(segments[node.hash % segmentCount]);
		Guard lock(segment.mutex);
		if (!node.alive)
			return;
		// Remove the old TTL if any
		if (node.hasTTL)
			segment.ttlMap.erase(node.ttlPos);
		// Install a new one
		node.ttlPos = segment.ttlMap.emplace(ttl, it.node);
		node.hasTTL = true;
	}
	void ttl(const Key &key, const TTL &ttl) {
		this->ttl(find(key), ttl);
	}
	// Remove items with TTL less than before.
	void cleanupTTL(const TTL &before) {
		std::unique_ptr<Guard> lock;
		if (keepDeleted) // We need to lock the global mutex only when we want to store the deleted things
			lock.reset(new Guard(globalMutex));
		for (Segment &s: segments) {
			// Lock the segments sequentially, only one at a time ‒ we don't need the others
			Guard segLock(s.mutex);
			auto ttlPos = s.ttlMap.begin();
			while (ttlPos != s.ttlMap.end() && ttlPos->first < before) {
				LOG(TRACE, "Deleting because of TTL");
				// Move to the next iterator, but keep a copy we're going to delete (eraseInternal invalidates cp)
				auto cp(ttlPos ++);
				eraseInternal(cp->second, true);
			}
		}
	}
	typedef std::unordered_set<iterator, ItHash> Iterators;
private:
	Iterators deleted_;
public:
	/*
	 * Return all the items implicitly deleted since the last time this
	 * was called (only if keepDeleted is true).
	 */
	Iterators deleted() {
		Guard lock(globalMutex);
		Iterators result;
		result.swap(deleted_);
		return result;
	}
	size_t size() const {
		return itemCount;
	}
};

// Just a storage of the static members
template<class Key, class Value, size_t segmentCount, bool keepDeleted, bool sizeLimitEnabled, bool implicitLRU, class TTL> std::hash<Key> Cache<Key, Value, segmentCount, keepDeleted, sizeLimitEnabled, implicitLRU, TTL>::hasher;
template<class Key, class Value, size_t segmentCount, bool keepDeleted, bool sizeLimitEnabled, bool implicitLRU, class TTL> std::hash<typename Cache<Key, Value, segmentCount, keepDeleted, sizeLimitEnabled, implicitLRU, TTL>::PNode> Cache<Key, Value, segmentCount, keepDeleted, sizeLimitEnabled, implicitLRU, TTL>::nodeHasher;

}

#endif
