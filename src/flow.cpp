/*
 * Copyright 2016, CZ.NIC z.s.p.o. (http://www.nic.cz/)
 *
 * This file is part of the pakon system.
 *
 * Updater is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 * Updater is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Updater.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "flow.hpp"
#include "serialize.hpp"
#include "type_serialization.hpp"
#include "packet.hpp"
#include "addr_info.hpp"

#include <netinet/in.h>
#include <libnetfilter_queue/libnetfilter_queue.h>
#include <linux/netfilter.h>

using std::get;
using std::tie;
using std::ignore;
using std::make_pair;

namespace Pakon {

// Add definitions how to serialize few more data types we use.
template<> class JSORiazible<Verdict> {
public:
	template<class Stream> static void jsorialize(Stream &stream, const Verdict &verdict) {
		stream << "{\"action\":";
		switch (verdict.action) {
			case NF_ACCEPT:
				stream << "\"ACCEPT\"";
				break;
			case NF_DROP:
				stream << "\"DROP\"";
				break;
			default:
				// Not known ‒ use a number
				stream << verdict.action;
				break;
		}
		if (verdict.mark_mask)
			stream << ",\"mask\":" << verdict.mark_mask << ",\"mark\":" << verdict.mark_value;
		stream << "}";
	}
};
template<> class JSORiazible<FID> {
public:
	template<class Stream> static void jsorialize(Stream &stream, const FID &fid) {
		stream << '"' << fid.ctime << '-' << fid.rtime << '-' << fid.tid << '-' << fid.seq << '"';
	}
};

template<> class JSORiazible<FlowStatus> {
public:
	template<class Stream> static void jsorialize(Stream &stream, FlowStatus status) {
		switch (status) {
			case FlowStatus::New:
				stream << "\"new\"";
				break;
			case FlowStatus::Ongoing:
				stream << "\"ongoing\"";
				break;
			case FlowStatus::Closed:
				stream << "\"closed\"";
				break;
			case FlowStatus::Dropped:
				stream << "\"dropped\"";
				break;
		}
	}
};

template<> class JSORiazible<NameDetails> {
public:
	template<class Stream> static void jsorialize(Stream &stream, NameDetails) {
		stream << "{}";
	}
};

namespace {

thread_local uint64_t fid_ord = 0;

typedef std::lock_guard<std::mutex> MuxGuard;

}

FID::FID(uint64_t curtime) :
	ctime(curtime),
	rtime(timeGrab(CLOCK_MONOTONIC)),
	tid(std::this_thread::get_id()),
	seq(fid_ord ++)
{ }

FlowHalf::FlowHalf() {
	store<FL_packets>(0);
	store<FL_payload>(0);
	store<FL_total>(0);
	store<FL_closed>(false);
}

void FlowHalf::update(const Packet &packet) {
	field<FL_packets>() ++;
	field<FL_payload>() += packet.field<PKT_payload>().size();
	field<FL_total>() += packet.size();
	if (packet.present<PKT_tcp_flags>() && (packet.field<PKT_tcp_flags>() & TcpFlags::FIN))
		store<FL_closed>(true);
}

Flow::Flow(const Packet &packet) :
	match(packet.flowMatch())
{
	// The default is to accept the flow
	fields.store<FL_verdict>(NF_ACCEPT, 0, 0);
	// The flow is brand new now
	fields.store<FL_status>(FlowStatus::New);
	// Inherit a bunch of fields from the first packet
	uint64_t curtime(packet.field<PKT_timestamp>());
	fields.store<FL_id>(curtime);
	fields.store<FL_starttime>(curtime);
	fields.store<FL_starttime_monotonic>(packet.field<PKT_timestamp_monotonic>());
	fields.store<FL_lasttime>(curtime);
	fields.store<FL_lasttime_monotonic>(packet.field<PKT_timestamp_monotonic>());
	Direction dir = packet.field<PKT_direction>();
	fields.store<FL_direction>(dir);
	fields.store<FL_flags>(*packet.field<PKT_qflags>());
	fields.store<FL_ip_proto_raw>(packet.field<PKT_ip_proto>());
	// Find the inner-most IP header and get the IP addresses from there
	const ProtoLevel *lastLevel = nullptr;
	for (const auto &l: packet.field<PKT_level>())
		if (l.type == ProtoLevel::IPv4 || l.type == ProtoLevel::IPv6) {
			lastLevel = &l;
			fields.store<FL_family>(l.type == ProtoLevel::IPv4 ? Family::IPv4 : Family::IPv6);
		}
	assert(lastLevel);
	// Load info about the addresses from a cache.
	PAddrInfo srcInfo, dstInfo;
	tie(srcInfo, ignore, ignore) = addrInfoCache.access(lastLevel->src);
	tie(dstInfo, ignore, ignore) = addrInfoCache.access(lastLevel->dst);
	const Address srcMac(srcInfo->mac());
	const Address dstMac(dstInfo->mac());
	Addresses *src = nullptr, *dst = nullptr;
	if (dir == Direction::IN) {
		src = &fields.field<FL_remote>();
		dst = &fields.field<FL_local>();
	} else {
		src = &fields.field<FL_local>();
		dst = &fields.field<FL_remote>();
	}
	src->store<FL_ip>(lastLevel->src);
	if (srcMac.size()) // No address = empty, zero size
		src->store<FL_mac>(srcMac);
	dst->store<FL_ip>(lastLevel->dst);
	if (dstMac.size())
		dst->store<FL_mac>(dstMac);
	/*
	 * Look up domain names in our cache (the client should have asked for
	 * the addresses recently so we should have it).
	 */
	for (const auto &name: srcInfo->names())
		src->field<FL_name>().insert(make_pair(name, NameDetails()));
	for (const auto &name: dstInfo->names())
		dst->field<FL_name>().insert(make_pair(name, NameDetails()));
	const ProtoLevel &ipProto(packet.field<PKT_level>().back());
	IpProto ipProtoVal = IpProto::Other;
	switch (ipProto.type) {
		case ProtoLevel::TCP:
			ipProtoVal = IpProto::TCP;
			break;
		case ProtoLevel::UDP:
			ipProtoVal = IpProto::UDP;
			break;
		case ProtoLevel::Unknown:
			ipProtoVal = IpProto::Other;
			break;
		case ProtoLevel::Ethernet:
		case ProtoLevel::IPv4:
		case ProtoLevel::IPv6:
			assert(0);
	}
	fields.store<FL_ip_proto>(ipProtoVal);
	if (packet.present<PKT_ports>()) {
		const auto &ports(packet.field<PKT_ports>());
		src->store<FL_port>(ports.first);
		dst->store<FL_port>(ports.second);
	}
}

std::string Flow::toJSON() const {
	MuxGuard lock(mutex);
	return Pakon::toJSON(fields);
}

bool Flow::update(const Packet &packet, bool initial) {
	MuxGuard lock(mutex);
	if (!initial)
		fields.store<FL_status>(FlowStatus::Ongoing);
	Direction dir = packet.field<PKT_direction>();
	bool interesting = false;
	if (dir == Direction::IN)
		fields.field<FL_in>().update(packet);
	else
		fields.field<FL_out>().update(packet);
	// Did the second half of the flow just close?
	if (fields.field<FL_in>().field<FL_closed>()
		&& fields.field<FL_out>().field<FL_closed>()
		&& fields.field<FL_status>() != FlowStatus::Closed) {
		TRC;
		fields.store<FL_status>(FlowStatus::Closed);
		interesting = true;
	}
	// Take max ‒ the packets may be processed out of order
	fields.field<FL_lasttime>() = std::max(fields.field<FL_lasttime>(), packet.field<PKT_timestamp>());
	fields.field<FL_lasttime_monotonic>() = std::max(fields.field<FL_lasttime_monotonic>(), packet.field<PKT_timestamp_monotonic>());
	// As not all packets have the MAC address available, check every time if we want to fill it in
	const auto &macLevel(packet.field<PKT_level>().front());
	assert(macLevel.type == ProtoLevel::Ethernet);
	// Note that only the SRC MAC address is available (if any)
	Addresses &src(dir == Direction::IN ? fields.field<FL_remote>() : fields.field<FL_local>());
	/*
	 * If the packet has a MAC address specified and the flow doesn't have one or
	 * has a different one, we want to update the flow and possibly the cache (as
	 * the wrong value might have come from the cache).
	 */
	if (macLevel.src.size() && (!src.present<FL_mac>() || macLevel.src != src.field<FL_mac>())) {
		TRC;
		src.store<FL_mac>(macLevel.src);
		// Store the current MAC address, creating the entry if necessary
		get<0>(addrInfoCache.access(src.field<FL_ip>()))->setMac(macLevel.src);
		// We enriched the flow info about an important information
		interesting = true;
	}
	return interesting;
}

void Flow::setStatus(FlowStatus status) {
	MuxGuard lock(mutex);
	fields.store<FL_status>(status);
}

uint64_t Flow::ttl(uint64_t now) const {
	MuxGuard lock(mutex);
	switch (fields.field<FL_status>()) {
		case FlowStatus::Closed:
			return now + closedTime;
		default:
			return now + inactivityTime;
	}
}

bool Flow::maybeDns() const {
	/*
	 * No locking here, on purpose. Since we only read fields that are set
	 * on creation and never updated, it is legal.
	 */
	/*
	 * TODO: Note that there might be DNS flows on TCP as well. But we
	 * aren't able to handle these for now. Therefore we just ignore them,
	 * as they are rather rare on the client side anyway. We need to do
	 * some stream reconstruction or heuristics to handle those.
	 */
	return fields.field<FL_ip_proto>() == IpProto::UDP &&
		(fields.field<FL_local>().field<FL_port>() == 53 || fields.field<FL_remote>().field<FL_port>() == 53);
}

}
