/*
 * Copyright 2016, CZ.NIC z.s.p.o. (http://www.nic.cz/)
 *
 * This file is part of the pakon system.
 *
 * Pakon is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 * Pakon is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pakon.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Help with converting data between lua and C++. Also with
 * calling functions.
 */

#ifndef PAKOND_LUA_CONVERT_H
#define PAKOND_LUA_CONVERT_H

#include "lua_common.hpp"

#include <string>
#include <vector>

namespace Pakon {
namespace Lua {

class Value;
class AnyFunction;
template<class Result, class ...Params> class Function;

/*
 * Group of functions to push values to lua stack.
 */
template<class T> typename std::enable_if<std::is_integral<T>::value>::type push(lua_State *state, T value) {
	lua_pushinteger(state, value);
}
template<class T> typename std::enable_if<std::is_floating_point<T>::value>::type push(lua_State *state, T value) {
	lua_pushnumber(state, value);
}
void push(lua_State *state, bool value);
void push(lua_State *state, const std::string &value);
void push(lua_State *state, const char *value);
void push(lua_State *state, const Value &value);
void push(lua_State *state, const AnyFunction &value);
template<class Result, class ...Params> void push(lua_State *state, const Function<Result, Params...> &value) {
	push(state, static_cast<const Value &>(value));
}
// Forward declarations of template functions, so they can call each other when nested
template<class Container> typename std::enable_if<IsSequence<Container>::value>::type push(lua_State *state, const Container &container);
template<class Container> typename std::enable_if<IsMap<Container>::value>::type push(lua_State *state, const Container &container);
template<class Container> typename std::enable_if<IsSet<Container>::value>::type push(lua_State *state, const Container &container);
/*
 * Allow other types to be pushed to lua.
 * Provide a template partial specialization for each such
 * type and put a static void push method in it.
 */
template<class T> class Pushable;
GEN_CHECK(IsLuaPushable, Lua::Pushable<U>::push(nullptr, std::declval<U>()), );
template<class T> typename std::enable_if<IsLuaPushable<T>::value>::type push(lua_State *state, const T &t) {
	Pushable<T>::push(state, t);
}
template<class Container> typename std::enable_if<IsSequence<Container>::value>::type push(lua_State *state, const Container &container) {
	lua_newtable(state);
	size_t i = 1;
	// This could recurse through the types and the pushes. Make sure we have space
	// to push to.
	lua_checkstack(state, 4);
	for (const auto &v: container) {
		lua_pushinteger(state, i ++);
		push(state, v);
		lua_rawset(state, -3);
	}
}
template<class Container> typename std::enable_if<IsMap<Container>::value>::type push(lua_State *state, const Container &container) {
	lua_newtable(state);
	// This could recurse through the types and the pushes. Make sure we have space
	// to push to.
	lua_checkstack(state, 4);
	for (const auto &p: container) {
		push(state, p.first);
		push(state, p.second);
		lua_rawset(state, -3);
	}
}
template<class Container> typename std::enable_if<IsSet<Container>::value>::type push(lua_State *state, const Container &container) {
	lua_newtable(state);
	// This could recurse through the types and the pushes. Make sure we have space
	// to push to.
	lua_checkstack(state, 4);
	for (const auto &k: container) {
		push(state, k);
		lua_pushboolean(state, true);
		lua_rawset(state, -3);
	}
}
// Loop through all the parameters, as a helper for variadic templates
template<class Head, class ...Tail> void push(lua_State *state, const Head &head, const Tail &...tail) {
	/*
	 * +1 for head, +2 as a reserve when manipulating with data.
	 */
	lua_checkstack(state, sizeof...(Tail) + 1 + 2);
	push(state, head);
	push(state, tail...);
}

/*
 * Group of functions to extract values from lua stack.
 * Start at the given index and continue. Throw
 * Mismatch if the value is not available.
 *
 * The types are specified by providing the variables
 * to be filled in.
 */
template<class T> typename std::enable_if<std::is_integral<T>::value>::type extract(lua_State *state, int index, T &value) {
	if (lua_isnumber(state, index))
		value = lua_tointeger(state, index);
	else
		throw Mismatch("Integer expected on index " + to_string(index));
}
template<class T> typename std::enable_if<std::is_floating_point<T>::value>::type extract(lua_State *state, int index, T &value) {
	if (lua_isnumber(state, index))
		value = lua_tonumber(state, index);
	else
		throw Mismatch("Number expected on index " + to_string(index));
}
void extract(lua_State *state, int index, bool &value);
void extract(lua_State *state, int index, std::string &value);
void extract(lua_State *state, int index, Value &value);
void extract(lua_State *state, int index, AnyFunction &value);
template<class Result, class ...Params> void extract(lua_State *state, int index, Function<Result, Params...> &value) {
	extract(state, index, static_cast<Value &>(value));
}
// Some forward declarations, so these things can call each other if nested
template<class Container> typename std::enable_if<IsSequence<Container>::value>::type extract(lua_State *state, int index, Container &container);
template<class Container> typename std::enable_if<IsMap<Container>::value>::type extract(lua_State *state, int index, Container &container);
template<class Container> typename std::enable_if<IsSet<Container>::value>::type extract(lua_State *state, int index, Container &container);
/*
 * Allow for our own types to be extracted from lua.
 * Provide a template partial specialization for each
 * such type and put a static void extract method in it.
 */
template<class T> class Extractable;
GEN_CHECK(IsLuaExtractable, Lua::Extractable<U>::extract(nullptr, 0, std::declval<U &>()), );
template<class T> typename std::enable_if<IsLuaExtractable<T>::value>::type extract(lua_State *state, int index, T &value) {
	Extractable<T>::extract(state, index, value);
}
// A sequence container, like std::vector. Note that all the sequences have the default allocator parameter, that's why there's one extra.
template<class Container> typename std::enable_if<IsSequence<Container>::value>::type extract(lua_State *state, int index, Container &container) {
	if (lua_istable(state, index)) {
		// Clear the container and iterate through the table
		container.clear();
		// We copy the table at known index -1 (we shall change the indices a bit)
		if (!lua_checkstack(state, 2))
			throw OutOfMemory("Couldn't grow stack during sequence extraction");
		lua_pushvalue(state, index);
		// Increase indices until we find a nil/nonexistant index
		for (size_t i = 1; ; i ++) {
			lua_pushinteger(state, i);
			lua_gettable(state, -2);
			if (lua_isnil(state, -1))
				// End of the table
				break;
			// Extract the one value we just got ouf of the table and store it
			container.emplace_back();
			extract(state, -1, container.back());
			// Get rid of the value from the stack
			lua_pop(state, 1);
		}
		// Get rid of that nil on top of stack and the table below
		lua_pop(state, 2);
	} else
		throw Mismatch("Table expected on index " + to_string(index) + " to fill in seqeunce container");
}
// A helper function to unify code between extract for maps and sets
template<class Store> void extractTable(lua_State *state, int index, const Store &store) {
// A std::map-like container
	if (lua_istable(state, index)) {
		// Copy the table to a known index, we're going to change the indices a bit
		if (!lua_checkstack(state, 4))
			throw OutOfMemory("Couldn't grow the stack during map extraction");
		lua_pushvalue(state, index);
		// Iterate through the whole thing
		lua_pushnil(state); // Starting point for next()
		while (lua_next(state, -2)) {
			// Make a copy of the key. Reading it might change its type, which would confuse lua_next
			lua_pushvalue(state, -2);
			// Let the lambda handle the values
			store(state);
			// Clean up ‒ get rid of the value and the key copy
			lua_pop(state, 2);
		}
		// Get rid of the copied table
		lua_pop(state, 1);
	} else
		throw Mismatch("Table expected on index " + to_string(index) + " to fill mapping container");
}
template<class Container> typename std::enable_if<IsMap<Container>::value>::type extract(lua_State *state, int index, Container &container) {
	// Instead of clearing the old container first, we create a new one and swap if it went well. That way we don't have to do it in the middle of the helper function
	Container newC;
	extractTable(state, index, [&newC] (lua_State *state) {
		// Each time, we have value, key on top of the stack. Extract them and store.
		typename Container::mapped_type value;
		extract(state, -2, value);
		typename Container::key_type key;
		extract(state, -1, key);
		newC.insert(typename Container::value_type(key, value));
	});
	container.swap(newC);
}
// A set-like container (similar to above, but take only the keys
template<class Container> typename std::enable_if<IsSet<Container>::value>::type extract(lua_State *state, int index, Container &container) {
	Container newC;
	extractTable(state, index, [&newC] (lua_State *state) {
		// Ignore the value at -2
		typename Container::key_type key;
		extract(state, -1, key);
		newC.insert(key);
	});
	container.swap(newC);
}
// Loop through the parameters to fill in
template<class Head, class ...Tail> void extract(lua_State *state, int index, Head &head, Tail& ...tail) {
	extract(state, index, head);
	extract(state, index + 1, tail...);
}
// For the sake of completeness, so we can call functions without parameters
inline static void extract(lua_State *, int) {}

/*
 * Helper functions for cFromLua. Most of it is just transforming
 * of parameters/extracting the param and result types. There's also
 * a trick to create the param variable as function parameters (since
 * we can't declare parameter pack in the middle of function).
 */
template<class R> class CFromLuaResult {
public:
	static int push(lua_State *state, const R &value) {
		::Pakon::Lua::push(state, value);
		return 1;
	}
};
template<class ...T> class CFromLuaResult<std::tuple<T...>> {
private:
	// Bumper at the end of the recursion
	template<size_t i> static typename std::enable_if<i == sizeof...(T)>::type pushOne(lua_State *, const std::tuple<T...> &) {}
	// One step of the push recursion through the tuple
	template<size_t i> static typename std::enable_if<i < sizeof...(T)>::type pushOne(lua_State *state, const std::tuple<T...> &values) {
		::Pakon::Lua::push(state, std::get<i>(values));
		pushOne<i + 1>(state, values);
	}
public:
	static int push(lua_State *state, const std::tuple<T...> &values) {
		/*
		 * Make sure the stack is large enough for the whole tuple
		 * Add a bit more, just to make sure we don't use something more
		 * dunig the pushing.
		 */
		lua_checkstack(state, sizeof...(T) + 2);
		pushOne<0>(state, values);
		return sizeof...(T);
	}
};
template<class ...P> int cFromLuaParams(lua_State *state, const std::function<void(P...)> &f, typename std::decay<P>::type ...vals) {
	extract(state, 1, vals...);
	f(vals...);
	return 0;
}
template<class R, class ...P> typename std::enable_if<!std::is_void<R>::value, int>::type cFromLuaParams(lua_State *state, const std::function<R(P...)> &f, typename std::decay<P>::type ...vals) {
	extract(state, 1, vals...);
	return CFromLuaResult<typename std::decay<R>::type>::push(state, f(vals...));
}

template<class R, class ...P> int cFromLuaI(lua_State *state, const std::function<R(P...)> &f) {
	return cFromLuaParams(state, f, typename std::decay<P>::type()...);
}
template<class B, class R, class ...P> int cFromLuaI(lua_State *state, B *b, R (B::*function)(P...)) {
	return cFromLuaI<R, P...>(state, std::function<R(P...)>([b, function](P ...p) -> R { return (b->*function)(p...);}));
}
template<class B, class R, class ...P> int cFromLuaI(lua_State *state, B &&b, R (B::*function)(P...)) {
	return cFromLuaI(state, &b, function);
}
template<class B, class R, class ...P> int cFromLuaI(lua_State *state, B &b, R (B::*function)(P...)) {
	return cFromLuaI(state, &b, function);
}
template<class B, class R, class ...P> int cFromLuaI(lua_State *state, const B *b, R (B::*function)(P...) const) {
	return cFromLuaI<R, P...>(state, std::function<R(P...)>([b, function](P ...p) -> R { return (b->*function)(p...);}));
}
template<class B, class R, class ...P> int cFromLuaI(lua_State *state, const B &&b, R (B::*function)(P...) const) {
	return cFromLuaI(state, &b, function);
}
template<class B, class R, class ...P> int cFromLuaI(lua_State *state, const B &b, R (B::*function)(P...) const) {
	return cFromLuaI(state, &b, function);
}
// TODO Plain old function
template<class F> int cFromLuaI(lua_State *state, F &&f) {
	// Abuse that functors have operator(). Therefore we can transform this to pointer to member function case (one of the above).
	return cFromLuaI(state, std::forward<F>(f), &F::operator());
}

/*
 * Provided there are correct arguments on the lua stack, call the given
 * callable. You can also pass an object and pointer to member function.
 * The arguments are deduced from the function type, automatically extracted
 * from lua stack. The results are put back onto the stack and number of results
 * is returned (void → 0, X → 1, tupple<…> → number of things inside).
 *
 * Exceptions derived from Lua::Error are turned into lua errors (catchable inside
 * the lua code), others just jump through the interpreter to whatever called it.
 */
template<class ...P> int cFromLua(lua_State *state, P &&...p) {
	TRC;
	try {
		/*
		 * We wrap in the try-catch on the highest possible level.
		 * This way we won't need to call any fancy destructors
		 * here when we longjum from lua_error. This however changes
		 * nothing about our caller :-(.
		 */
		return cFromLuaI(state, std::forward<P>(p)...);
	} catch (const Error &e) {
		lua_pushstring(state, e.what());
		return lua_error(state);
	}
}

/*
 * A generic storage of a lua value. It copies the value from the given place
 * to the REGISTRY global table of the interpreter and keeps it alive
 * for as long as the Value itself is alive.
 *
 * It does _not_ take ownership of the lua state.
 */
class Value {
protected:
	lua_State *state = nullptr;
	void extractToStack() const;
private:
	/*
	 * An unique ID used inside the REGISTRY. The this pointer is used
	 * (which is safe, if we destroy this, we release the thing in the
	 * table as well.
	 *
	 * Note that this is a binary string and may as well contain
	 * embedded zeroes.
	 */
	std::string id() const;
	// Push the ID to the top of the lua stack
	void pushId() const;
	friend void push(lua_State *state, const Value &value);
public:
	/*
	 * Create an empty (invalid) value. It must never be read from,
	 * but can be assigned into (from a value of any interpreter).
	 */
	Value() = default;
	// Create nil
	explicit Value(lua_State *state);
	// Copy value from given stack index
	Value(lua_State *state, int index);
	// Copy from other value. Note that both must be on the same lua state.
	Value(const Value &other);
	Value &operator =(const Value &other);
	// Destructor
	~ Value();
	// Get the value (throws Mismatch if it can't be converted)
	template <class T> T extract() const {
		T result;
		StackGuard guard(state);
		extractToStack();
		::Pakon::Lua::extract(state, -1, result);
		return result;
	}
	/*
	 * Store the value on the given index under our own ID.
	 * May provide a state in case the value is invalid.
	 */
	void store(int index, lua_State *state = nullptr);
};

// Some helper classes to extract a result
template<class Result> class ResultExtractor {
public:
	static Result extract(lua_State *state) {
		Result result;
		::Pakon::Lua::extract(state, 1, result);
		return result;
	}
};
template<> class ResultExtractor<void> {
public:
	static void extract(lua_State *) {}
};
template<class ...Results> class ResultExtractor<std::tuple<Results...>> {
private:
	typedef std::tuple<Results...> Result;
	static Result extractInternal(lua_State *state, typename std::decay<Results>::type ...results) {
		::Pakon::Lua::extract(state, 1, results...);
		return std::make_tuple(results...);
	}
public:
	static Result extract(lua_State *state) {
		return extractInternal(state, typename std::decay<Results>::type()...);
	}
};

/*
 * A function wrapper. It is a value wrapper internally and no
 * check that the value is actually a function is done at construction
 * time.
 *
 * Method to actually call the function is added.
 *
 * This version is templated on the call method, so one function
 * may be called with different parameters.
 */
class AnyFunction : public Value {
public:
	AnyFunction() = default;
	using Value::Value; // Inherit constructors, please
	AnyFunction(const Value &other) : Value(other) {} // This one isn't inherited even when asked for :-(
	using Value::operator =; // Inherit assignment operator (which accepts Value, not just AnyFunction)
	template<class Result, class ...Params> Result call(const Params &...params) const {
		StackGuard stackGuard(state);
		// Put the function and parameters there
		push(state, *this);
		lua_checkstack(state, sizeof...(params) + 5);
		push(state, params...);
		// Call the function and decide if there was an error
		const int result = lua_pcall(state, sizeof...(Params), LUA_MULTRET, 0);
		switch (result) {
			case 0: // OK
				break;
			case LUA_ERRRUN:
				throw RuntimeError(lua_tostring(state, -1));
			case LUA_ERRMEM:
				throw OutOfMemory(lua_tostring(state, -1));
			default:
				throw Error(lua_tostring(state, -1));
		}
		// Get the result
		return ResultExtractor<Result>::extract(state);
	}
};

/*
 * A function wrapper, templated on the class, so it can then
 * be used as a functor conveniently.
 */
template<class Result, class ...Params> class Function : public AnyFunction {
public:
	Function() = default;
	using AnyFunction::AnyFunction;
	Function(const Value &other) : AnyFunction(other) {}
	using AnyFunction::operator =;
	Result operator ()(Params &&...params) const { return AnyFunction::call<Result, Params...>(std::forward<Params>(params)...); }
};

}
}

#endif
