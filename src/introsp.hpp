/*
 * Copyright 2016, CZ.NIC z.s.p.o. (http://www.nic.cz/)
 *
 * This file is part of the pakon system.
 *
 * Pakon is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 * Pakon is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pakon.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PAKOND_INTROSP_H
#define PAKOND_INTROSP_H

#include "util.hpp"
#include <vector>
#include <unordered_map>

namespace Pakon {

/*
 * The purpose of this file is to provide something like a struct ‒
 * something that contains bunch of fields, possibly of different types.
 * However, it provides some more features (for the price of less convenient
 * use and more internal complexity).
 *
 * It allow the fields to be either simple, optional (eg. it may be there
 * or may not, with either explicit creation or implicit creation on
 * first access), or multiple (eg. an array).
 *
 * However, the main feature is it is possible to iterate through all
 * the fields declared there. This helps with eg. serializing.
 *
 * You can do something like:
 * class SomeType;
 * class OtherType;
 * typedef Struct<
 *	Field<int, Simple, SomeType>,
 *	Field<float, Simple, OtherType>
 *	> SomeStruct;
 *
 * To iterate over the fields, prepare a class with a template method
 * named "step". That template method takes three tempalte paramerets:
 * • The type of value passed.
 * • The name of the field.
 * • The full Field specification.
 * The only runtime parameter is the value.
 *
 * And yes, expect a bit of arcana here.
 */
namespace Introspectable {

/*
 * These classes work only as identifiers/atoms to specify something. They
 * don't actually exist.
 */

class Simple; // Just a single value, present all the time
class Implicit; // Created on first non-const access
class Explicit; // Has to call store() first
template<class CreationMode> class Optional; // A value that may or may not be there
class BatchNonempty; // Called with the whole std::vector, but only if it is not empty
class BatchEmpty; // Called with the whole std::vector (even empty one)
class Dribble; // Called once for each value
template<class IterationMode> class Multiple; // Any number of values (in std::vector internally)
// A string→something map that pretends to be flattened into the struct
class InlineMap;

// Thrown when the optional field is accessed and not present.
EXCEPTION(std::runtime_error, MissingField);

/*
 * (Also nonexistant) specification of one field.
 * The name is any type. There may be one field of each type only.
 * It works like the name of the field. The type is also passed
 * to function when iterating, so you can have other info
 * (eg. a string for the identification during serializing)
 * in it. No instance is ever created by this module.
 */
template<class Type, class Mode, class Name> class Field;
// This would hold single field. We have no non-specialized instances.
template<class Field> class FieldHolder;
template<class Type, class Name> class FieldHolder<Field<Type, Simple, Name>> {
private:
	Type storage;
public:
	Type &field() { return storage; }
	const Type &field() const { return storage; }
	bool present() const { return true; }
	template<class ...Params> void store(Params &&...params) { storage = Type(std::forward<Params>(params)...); }
	template<class Call> void iterationStep(Call &c) const { c.template step<Type, Name, Field<Type, Simple, Name>>(field()); }
};
template<class Type, class Name, class IterationMode> class FieldHolder<Field<Type, Multiple<IterationMode>, Name>> {
public:
	static_assert(std::is_same<IterationMode, BatchEmpty>::value || std::is_same<IterationMode, BatchNonempty>::value || std::is_same<IterationMode, Dribble>::value, "Unknown iteration mode");
	typedef std::vector<Type> RealType;
private:
	RealType storage;
public:
	RealType &field() { return storage; }
	const RealType &field() const { return storage; }
	bool present() const { return !storage.empty(); }
	template<class ...Params> void store(Params &&...params) { storage.emplace_back(std::forward<Params>(params)...); }
	void store_all(const std::vector<Type> &data) { storage = data; }
	void store_all(std::vector<Type> &&data) { storage = data; }
	template<class Call> void iterationStep(Call &c) const {
		if (std::is_same<IterationMode, BatchEmpty>::value || (std::is_same<IterationMode, BatchNonempty>::value && present()))
			c.template step<RealType, Name, Field<Type, Multiple<IterationMode>, Name>>(field());
		else if(std::is_same<IterationMode, Dribble>::value)
			for (const Type &v: storage)
				c.template step<Type, Name, Field<Type, Multiple<IterationMode>, Name>>(v);
		// Third option BatchNonempty and no data → call nothing
	}
	void clear() { storage.clear(); }
};
template<class Type, class Name, class CreationMode> class FieldHolder<Field<Type, Optional<CreationMode>, Name>> {
private:
	static_assert(std::is_same<CreationMode, Implicit>::value || std::is_same<CreationMode, Explicit>::value, "Unknown creation mode");
	std::unique_ptr<Type> storage;
public:
	template<class ...Params> void store(Params &&...params) { storage.reset(new Type(std::forward<Params>(params)...)); }
	void clear() { storage.reset(); }
	const Type &field() const {
		if (storage)
			return *storage;
		else
			throw MissingField("Accessing missing field");
	}
	Type &field() {
		if (storage)
			return *storage;
		else if (std::is_same<CreationMode, Implicit>::value) {
			// Create a new one and try again
			store();
			return field();
		} else if (std::is_same<CreationMode, Explicit>::value) {
			throw MissingField("Implicit creation of field attempted on explicit field");
		}
	}
	bool present() const { return bool(storage); }
	template<class Call> void iterationStep(Call &c) const {
		if (present())
			c.template step<Type, Name, Field<Type, Optional<CreationMode>, Name>>(field());
	}
};
template<class Type, class Name> class FieldHolder<Field<Type, InlineMap, Name>> {
public:
	typedef std::unordered_map<std::string, Type> Storage;
private:
	Storage storage;
public:
	template<class ...Params> void store(Params &&...params) {
		storage = Storage(std::forward<Params>(params)...);
	}
	template<class ...Params> void add(const std::string &name, Params &&...params) {
		// emplace doesn't want to replace an existing field, but we do.
		const auto &found = storage.find(name);
		if (found == storage.end())
			// Not there yet, create
			storage.emplace(std::piecewise_construct, std::forward_as_tuple(name), std::forward_as_tuple<Params...>(params...));
		else
			found->second = Type(std::forward<Params>(params)...);
	}
	void clear() { storage.clear(); }
	const Type &field(const std::string &name) const {
		const auto &found = storage.find(name);
		if (found == storage.end())
			throw MissingField("Sub-field with name " + name + " is not present");
		else
			return found->second;
	}
	Type &field(const std::string &name) {
		const auto &found = storage.find(name);
		if (found == storage.end())
			throw MissingField("Sub-field with name " + name + " is not present");
		else
			return found->second;
	}
	const Storage &field() const { return storage; }
	bool present() const { return !storage.empty(); }
	bool present(const std::string &name) const { return storage.find(name) != storage.end(); }
	template<class Call> void iterationStep(Call &c) const {
		for (const auto &pair: storage)
			c.template step<Type, Name, Field<Type, InlineMap, Name>>(pair.first, pair.second);
	}
};

// Something that just binds the fields together. We could use FieldHolder<Fields>... as a base class, but cppcheck breaks on that, so we work around that this way (recursively collecting them together).
template<class ...Fields> class StructBase;
template<> class StructBase<> {
public:
	template<class FName> struct FoundField;
	template<class Call> void iterationStep(Call &) const {}
};
template<class Type_, class Mode_, class Name, class ...Tail> class StructBase<Field<Type_, Mode_, Name>, Tail...> : protected FieldHolder<Field<Type_, Mode_, Name>>, protected StructBase<Tail...> {
private:
	using Head = Field<Type_, Mode_, Name>;
protected:
	/*
	 * Look for the right field by its name. It is either
	 * the local one, in which case we just pick it, or
	 * we delegate it further for our parent to choose.
	 */
	template<class FName> using FoundField = typename std::conditional<std::is_same<Name, FName>::value, Head, typename StructBase<Tail...>::template FoundField<FName>>::type;
	// Recursively delegate the call through the ancestors
	template<class Call> void iterationStep(Call &call) const {
		FieldHolder<Head>::iterationStep(call);
		StructBase<Tail...>::iterationStep(call);
	}
};

/*
 * This is the real class the user shall use. It bundles the fields together and
 * delegates the calls to the proper fields.
 */
template<class ...Fields> class Struct : private StructBase<Fields...> {
private:
	template<class Name> using FoundHolder = FieldHolder<typename StructBase<Fields...>::template FoundField<Name>>;
public:
// Proxy a method. Unfortunately, the method name can't be a template parameter and having so many very similar proxy methods is ugly, so a macro to save the day.
#define PRX(METHOD, CONST) \
	template<class Name, class ...Params> auto METHOD(Params &&...params) CONST -> decltype(std::declval<CONST FoundHolder<Name>>().METHOD(std::forward<Params>(params)...)) { return FoundHolder<Name>::METHOD(std::forward<Params>(params)...); }
	PRX(field,)
	PRX(field, const)
	PRX(present, const)
	PRX(store,)
	PRX(store_all,)
	PRX(add,)
	PRX(clear,)
#undef PRX
	template<class Call> void iterate(Call &&call) const {
		StructBase<Fields...>::iterationStep(std::forward<Call>(call));
	}
};

}

}

#endif
