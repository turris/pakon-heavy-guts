/*
 * Copyright 2016, CZ.NIC z.s.p.o. (http://www.nic.cz/)
 *
 * This file is part of the pakon system.
 *
 * Pakon is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 * Pakon is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pakon.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PAKOND_UTIL_H
#define PAKOND_UTIL_H

#include "ringlog.hpp"

#include <sstream>
#include <string>
#include <cstring>
#include <cerrno>
#include <iostream>
#include <iomanip>
#include <functional>
#include <thread>
#include <mutex>
#include <utility>
#include <numeric>
#include <type_traits>
#include <time.h>

namespace Pakon {

/*
 * A class that can't be copied or copy-constructed. It is expected
 * to be used as a base class.
 */
class NonCopyable {
private:
	NonCopyable(const NonCopyable &);
	NonCopyable(NonCopyable &&);
	NonCopyable &operator =(const NonCopyable &);
	NonCopyable &operator =(NonCopyable &&);
protected:
	NonCopyable() = default;
};

/*
 * A singleton mixin. It is also a non-copyable.
 *
 * Use as:
 * class X : public Singleton<X> {
 * frient class Singleton<X>;
 * };
 *
 * Note that this is *not* thread safe.
 */
template <class Ancestor> class Singleton : protected NonCopyable {
public:
	static Ancestor &instance() {
		static Ancestor instance;
		return instance;
	}
};

// An easy way to generate new exceptions
#define EXCEPTION(Parent, Name) \
class Name : public Parent { \
public: \
	explicit Name(const std::string &what) : Parent(what) {} \
}

// Thrown with IO-related errors (eg. missing file, couldn't write, …)
EXCEPTION(std::runtime_error, FileError);
// Thrown when CHECK (below) fails.
EXCEPTION(std::runtime_error, CheckError);

typedef std::function<void()> Procedure;

// A guard-style class that runs its parameter on the exit of the current scope
class ExitGuard : private NonCopyable {
private:
	Procedure snippet;
public:
	explicit ExitGuard(const Procedure &snippet) : snippet(snippet) {}
	~ ExitGuard() {
		if (snippet)
			snippet();
	}
	// Abort running the snippet at the end of the scope
	void abort() {
		snippet = nullptr;
	}
};

/*
 * Implement our own to_string.
 *
 * One of the systems we compile for doesn't seem to provide to_string, despite
 * that being part of standard library. This is a very simple function, so the
 * easiest thing is to simply implement our own copy.
 */
template<class T> std::string to_string(const T& value) {
	std::stringstream output;
	output << value;
	return output.str();
}


#define CHECK(condition) do { if (!(condition)) throw CheckError(std::string(#condition " failed at " __FILE__ ":") + to_string(__LINE__) + ":" + __PRETTY_FUNCTION__ + "(error: " + to_string(errno) + "/" + strerror(errno) + ")"); } while (0)

/*
 * Thanks to Hanička Dusíková for providing the spark that
 * makes this magic work.
 */
#define GEN_CHECK(NAME, SNIPPET, EXTRA_CHECK) \
template<class T> class NAME { \
private: \
	template<class U> static decltype((SNIPPET), std::true_type{}) test(void *); \
	template<class> static std::false_type test(...); \
public: \
	static constexpr const bool value = std::is_same<decltype(test<T>(nullptr)), std::true_type>::value EXTRA_CHECK; \
}
// Can this be called?
GEN_CHECK(IsCallable, std::declval<U>()(), );
// Is it sequence like std::vector or list? These have front() method.
GEN_CHECK(IsSequence, std::declval<U>().front(), );
// Is it something like std::map? These have mapped_type typedef.
GEN_CHECK(IsMap, std::declval<typename U::mapped_type>(), );
// Is it something like std::set? These have key_type and are not maps.
GEN_CHECK(IsSet, std::declval<typename U::key_type>(), && !IsMap<T>::value);
// A pointer is something that can be dereferenced
GEN_CHECK(IsPtr, *std::declval<U>(), );

// Logging…

enum LogLevel {
	TRACE,
	DBG,
	INFO,
	WARN,
	ERROR,
	FATAL
};

// Initialize logging based on environment variables.
void loggingInitialize();

#ifndef MAX_LOG_LEVEL
#define MAX_LOG_LEVEL DBG
#endif
/*
 * Is this log severity level enabled at all?
 * This is a compile time configuration (though that define).
 */
constexpr bool logEnabled(LogLevel level) { return level >= MAX_LOG_LEVEL; }

extern LogLevel maxLogLevelStderr;
extern LogLevel maxLogLevelSyslog;
extern bool ringlogEnabled;

// Is this log severity level enabled on stderr?
static inline bool logEnabledStderr(LogLevel level) {
	return level >= maxLogLevelStderr;
}
// Is this log severity level enabled on syslog?
static inline bool logEnabledSyslog(LogLevel level) {
	return level >= maxLogLevelSyslog;
}
// Send this message to the syslog
void doSyslog(LogLevel level, const std::string &msg);

static const char *logNames[] = { "TRACE", "DBG", "INFO", "WARN", "ERROR", "FATAL" };

static inline const char *logName(LogLevel level) {
	return logNames[level];
}
/*
 * A bit of template magic…
 *
 * We keep unwinding the parameters and output them one by one. If the parameter is callable,
 * we call it and print the result (or not call it in case we don't log).
 */
template<class Stream, class Msg> typename std::enable_if<!IsCallable<Msg>::value>::type logPrint(Stream &stream, const Msg &msg) {
	stream << msg;
}
template<class Stream, class F> typename std::enable_if<IsCallable<F>::value>::type logPrint(Stream &stream, const F &f) {
	logPrint(stream, f());
}
template<class Stream, class Head, class ...Tail> void logPrint(Stream &stream, const Head &head, const Tail &...tail) {
	logPrint(stream, head);
	logPrint(stream, tail...);
}
template<class ...Msgs> void log(LogLevel level, const char *file, size_t line, const char *function, const Msgs &...msgs) {
	if (!logEnabled(level))
		return;
	/*
	 * Technically, this is not thread safe. It is, however, thread-safe
	 * to check if it was already initialized. And in the main library we do
	 * initialize that in the main() before starting all the threads.
	 *
	 * The trick is here to initialize logging in unit tests. They are mostly
	 * single-threaded, with a workaround at each multi-threaded test.
	 */
	static bool initialized = false;
	if (!initialized)
		loggingInitialize();
	const bool enabledSyslog = logEnabledSyslog(level);
	const bool enabledStderr = logEnabledStderr(level);
	if (enabledStderr || enabledSyslog || ringlogEnabled) {
		// Construct the message in memory and output it at once (hopufully atomic)
		std::stringstream output;
		logPrint(output, msgs...);
		output << std::endl;
		const std::string &str = output.str();
		if (enabledSyslog)
			doSyslog(level, str);
		if (enabledStderr || ringlogEnabled) {
			std::stringstream output_full;
			output_full << std::left << logName(level) << "\t" << std::setw(23) << file << std::setw(0) << "\t" << line << "\t" << std::setw(55) << function << std::setw(0) << "\t" << std::this_thread::get_id() << "\t" << str;
			const std::string full = output_full.str();
			if (enabledStderr)
				std::cerr << full;
			ringlog(full);
		}
	}
}
#define LOG(LEVEL, ...) do { log(LEVEL, __FILE__, __LINE__, __PRETTY_FUNCTION__, __VA_ARGS__); } while (0)

/*
 * A trace macro. It produces a TRACE log message with the name of
 * filename, line number and function name.
 */
//
#define TRC LOG(TRACE, "Passing here")

// Log and dump a ringbuffer if it is enabled
#define LOG_DUMP(LEVEL, ...) do { LOG(LEVEL, __VA_ARGS__); if (ringlogEnabled) dumpRingFile(); } while (0)

// Time in seconds
uint64_t timeGrab(clockid_t id = CLOCK_REALTIME);
// Time in msec
uint64_t timeMsec(clockid_t id = CLOCK_REALTIME);

bool inValgrind();

// An auxiliary function to insert (add) all items from one container into another. Kind of containerish +=
template<class C1, class C2> void insertAll(C1 &into, const C2 &what) {
	for (const auto &v: what)
		into.insert(v);
}

// Similar to python's or perl's join. The type should be something like a container of strings.
template<class C> std::string join(const C &container, const std::string &separator = std::string(), const std::string &empty = std::string()) {
	if (container.empty())
		return empty;
	auto it = container.begin();
	++ it;
	return std::accumulate(it, container.end(), container.front(), [&separator](const std::string &s1, const std::string &s2) { return s1 + separator + s2; });
}

}

#endif

