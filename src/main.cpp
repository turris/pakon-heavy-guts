/*
 * Copyright 2016, CZ.NIC z.s.p.o. (http://www.nic.cz/)
 *
 * This file is part of the pakon system.
 *
 * Updater is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 * Updater is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Updater.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "util.hpp"
#include "mainthread.hpp"

using namespace Pakon;

int main(int argc, const char *argv[]) {
	try {
		loggingInitialize();
		MainThread thread(std::vector<std::string>(argv + 1, argv + argc));
		thread.run();
		return 0;
	} catch (const std::exception &e) {
		LOG_DUMP(FATAL, "Unhandled exception: ", [&e] {return e.what();});
		return 1;
	} catch (...) {
		LOG_DUMP(FATAL, "Unhandled unknown exception");
		return 1;
	}
}
