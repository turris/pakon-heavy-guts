/*
 * Copyright 2016-2017, CZ.NIC z.s.p.o. (http://www.nic.cz/)
 *
 * This file is part of the pakon system.
 *
 * Updater is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 * Updater is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Updater.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "dissect.hpp"
#include "packet.hpp"
#include "dns.hpp"
#include "addr_info.hpp"

#include <unordered_set>
#include <unordered_map>

using std::vector;
using std::string;
using std::pair;
using std::make_pair;
using std::tie;
using std::get;
using std::unordered_map;
using std::unordered_set;

namespace std {

// Unfortunately, C++ doesn't know how to build a hash for two hashable things :-(. So provide one.
template<> struct hash<pair<Pakon::DNS::Name, Pakon::DNS::Name>> {
	size_t operator ()(const pair<Pakon::DNS::Name, Pakon::DNS::Name> &names) const {
		return hash<std::string>{}(names.first.toString() + names.second.toString());
	}
};

}

namespace Pakon {

namespace {

// Set the status of all the flows to dropped. Returns the set for convenience.
Flows markDropped(Flows &&flows) {
	for (auto f: flows)
		f->setStatus(FlowStatus::Dropped);
	return flows;
}

}

Dissector::Dissector() :
	activeFlows(maxActiveFlowCount)
{
	TRC;
}

Flows Dissector::process(vector<Packet> &packets) {
	TRC;
	Flows result, delayed;
	bool cleanupPerformed = false;
	/*
	 * While we do have the big cache of flows, it contains locking and
	 * other complex mechanisms and is accessed by other threads. In case
	 * all (or a lot of) the packets are from the same flow in both directions
	 * (therefore two different threads), they would still keep blocking
	 * each other.
	 *
	 * This optimises the access in case a lot of the packets in the batch
	 * are in the same flow (or just few flows), because this local cache
	 * is simpler and we do a bit less work when we find it here (because
	 * we did it already when we looked it up in the big cache). It is at the
	 * cost of a bit slower handling in case of very small batches (but then,
	 * we are not under heavy load) or when the packets are from many
	 * different flows (which is rather rare).
	 */
	unordered_map<string, PFlow> localCache;
	for (Packet &p: packets) {
		const string &match(p.flowMatch());
		const auto localLookup = localCache.find(match);
		PFlow flow;
		bool added = false;
		if (localLookup == localCache.end()) {
			bool cleanup;
			tie(flow, added, cleanup) = activeFlows.access(match, p);
			(added ? result : delayed).insert(flow);
			if (added)
				activeFlows.ttl(flow, flow->ttl(p.field<PKT_timestamp_monotonic>()));
			cleanupPerformed = cleanupPerformed || cleanup;
			localCache.emplace(match, flow);
		} else
			flow = localLookup->second;
		if (flow->update(p, added))
			/*
			 * The update has been somehow interesting ‒ something important has changed.
			 * Report it right away.
			 */
			result.insert(flow);
		p.store<PKT_flow>(flow);
		if (flow->maybeDns())
			maybeDns(p);
		/*
		 * TODO: Pass through NDPI, if applicable. How do dups & reorders work with that?
		 */
	}
	if (!delayed.empty()) {
		std::lock_guard<std::mutex> guard(mutex);
		insertAll(this->delayed, delayed);
	}
	// TODO: Pass the interesting things through the lua interpreter
	if (cleanupPerformed)
		insertAll(result, markDropped(activeFlows.deleted()));
	return result;
}

Flows Dissector::timeouts() {
	TRC;
	Flows result;
	// Grab the flows active in the last window
	{
		std::lock_guard<std::mutex> guard(mutex);
		result.swap(delayed);
	}
	const uint64_t now = timeMsec(CLOCK_MONOTONIC);
	// Move them to the back of the LRU and reset their TTL
	for (const auto &f: result) {
		activeFlows.touch(f);
		// TODO: Check if the flow is actually active and not closed ‒ the TTL would differ
		activeFlows.ttl(f, f->ttl(now));
	}
	activeFlows.cleanupTTL(now);
	// Drop long inactive or extra flows, but incorporate them in the result
	insertAll(result, markDropped(activeFlows.deleted()));
	return result;
}

// Do some checks to determine if we are interested in this record
bool recordInteresting(const DNS::Record &record, const DNS::Type &expected_type) {
	using namespace DNS;
	if (record.section() != Section::Answer)
		return false;
	if (record.rclass() != Class::IN)
		return false;
	return record.rtype() == expected_type;
}

// A transitive closure on the directed graph: https://en.wikipedia.org/wiki/Transitive_closure#In_graph_theory
void transitive_closure(unordered_set<std::pair<DNS::Name, DNS::Name>> &names) {
	using DNS::Name;
	bool modified;
	do {
		vector<pair<Name, Name>> new_edges;
		for (const auto &e1: names)
			for (const auto &e2: names) {
				if (e1 == e2)
					// A 1-length cycle, avoid
					continue;
				if (e1.second != e2.first)
					// These two don't connect
					continue;
				// An edge that skips the one in the middle
				const auto new_edge = make_pair(e1.first, e2.second);
				/*
				 * If the edge is not there yet, batch it for an insertion.
				 * We can't insert it right away, because that
				 * could invalidate all the iterators under our
				 * hands.
				 */
				if (names.find(new_edge) == names.end())
					new_edges.emplace_back(new_edge);
			}
		modified = !new_edges.empty();
		names.insert(new_edges.begin(), new_edges.end());
	} while (modified);
}

void Dissector::maybeDns(const Packet &packet) {
	using namespace DNS;
	try {
		LOG(DBG, "Suspecting a DNS packet");
		DNS::Message message(packet.field<PKT_payload>());
		if (message.opcode() != Opcode::Query ||
				message.qr() != QR::Response ||
				message.rcode() != RCode::NoError) {
			LOG(DBG, "Alright, a DNS packet, but not an interesting one");
			return;
		}
		if (!message.size() || message[0].section() != Section::Question || !message[0].name()) {
			LOG(DBG, "Missing the question section");
			return;
		}
		/*
		 * Note that this is *not* equivalent to unordered_multimap,
		 * as each edge (pair) may be present only once here.
		 */
		unordered_set<std::pair<Name, Name>> cnames;
		/*
		 * Construct the CNames first. They usually are before the
		 * actual answers, but it's likely not mandated and even if it
		 * was, someone would be doing it wrong anyway.
		 */
		for (const auto &record: message) {
			if (recordInteresting(record, Type::CNAME)) {
				const Name target(message.nameParseAt(record.data().begin())->toOwned());
				cnames.insert(make_pair(record.name()->toOwned(), target));
			}
		}
		/*
		 * Build a transitive closure of the graph. When we look up, we
		 * want to skip the whole possibly very long chain and get to
		 * the final destination on just one lookup.
		 */
		transitive_closure(cnames);
		// Process the IP addresses we have there
		const Name question_name(message[0].name()->toOwned());
		size_t cnt_stat = 0;
		for (const auto &record: message) {
			if (!recordInteresting(record, Type::A) && !recordInteresting(record, Type::AAAA))
				continue;
			const Name &record_name(record.name()->toOwned());
			/*
			 * We are interested in the name if the question was about it or if there's a CNAME path
			 * from the question to the record name. We are really interested in the question name
			 * (because the user asked for it). We are also interested in the real name of the computer
			 * (but not that much). We ignore the path in the middle, if there's anything more than
			 * one CNAME.
			 */
			if (record_name == question_name ||
					cnames.find(make_pair(question_name, record_name)) != cnames.end()) {
				PAddrInfo addrInfo(get<0>(addrInfoCache.access(Address(record.data()))));
				addrInfo->touchName(question_name, record.ttl());
				if (question_name != record_name)
					addrInfo->touchName(record_name, record.ttl());
			}
			cnt_stat ++;
		}
		// TODO: Other names? SRV, MX (these would work a bit like CNAME for us), PTR?
		LOG(DBG, "Harvested addresses: ", cnt_stat);
	} catch (const DNS::Message::ParseError& e) {
		LOG(DBG, "Not actually a DNS packet: ", e.what());
	}
}

}
