/*
 * Copyright 2016, CZ.NIC z.s.p.o. (http://www.nic.cz/)
 *
 * This file is part of the pakon system.
 *
 * Pakon is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 * Pakon is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pakon.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PAKOND_WORKER_H
#define PAKOND_WORKER_H

#include "util.hpp"
#include "spe.hpp"

#include <deque>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <memory>
#include <atomic>

namespace Pakon {

/*
 * A thread-safe message queue.
 */
template<class T> class MsgQueue : private NonCopyable {
public:
	typedef std::deque<T> Q;
private:
	std::mutex mutex;
	std::condition_variable condvar;
	Q queue;
public:
	void push(const T &msg) {
		{
			std::lock_guard<std::mutex> lock(mutex);
			queue.push_back(msg);
		} // Unlock before notify_one
		condvar.notify_one();
	}
	/*
	 * Extract everything that is in the queue, so it can be worked on outside of the lock.
	 * If wait is true, it would wait for at least one item.
	 */
	Q pop_all(bool wait = true) {
		std::unique_lock<std::mutex> lock(mutex);
		if (wait)
			condvar.wait(lock, [this]{ return !queue.empty(); });
		Q result;
		// Extract the data and empty the queue
		result.swap(queue);
		return result;
	}
};

/*
 * A worker thread. It'll create a SPE main loop and events may
 * be added to it.
 *
 * If an exception exits any task, the whole worker is terminated.
 * TODO: Send the error/event to the main thread.
 *
 * Note that the access to the worker is thread safe, but its
 * creation is not (it is expected only the main thread creates
 * new ones).
 */
class Worker : public SPE {
private:
	std::unique_ptr<std::thread> thread;
	MsgQueue<Procedure> commandQueue;
	std::atomic<bool> running = {true};
	const uint64_t id_;
	bool handleCommands();
	void run();
public:
	explicit Worker(uint64_t id);
	~ Worker();
	// The same id as passed to the constructor.
	uint64_t id() const { return id_; }
	/*
	 * Check if the worker is still alive.
	 * Note that it may have terminated in between you called the function and
	 * you examine the result.
	 */
	bool alive() const;
	/*
	 * Ask the thread nicely to terminate.
	 * If wait is true, wait for the thread to actually terminate.
	 * If it is false, then just ask the thread and terminate the
	 * function (you can still call stop with wait = true later to
	 * actually wait for it).
	 */
	void stop(bool wait = true);
	/*
	 * Add a task that gets called whenever the fd is readable.
	 * The task and the file descriptor is removed when it returns
	 * false. The file descriptor is closed.
	 */
	void addTask(int fd, const Task &task);
};

}

#endif
