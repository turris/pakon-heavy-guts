/*
 * Copyright 2016, CZ.NIC z.s.p.o. (http://www.nic.cz/)
 *
 * This file is part of the pakon system.
 *
 * Updater is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 * Updater is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Updater.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "worker.hpp"
#include "mainthread.hpp"

using std::bind;

namespace Pakon {

Worker::Worker(uint64_t id) :
	SPE(bind(&Worker::handleCommands, this)),
	id_(id)
{
	TRC;
	thread = std::unique_ptr<std::thread>(new std::thread(bind(&Worker::run, this)));
}

Worker::~ Worker() {
	TRC;
	// Make sure the thread terminates first
	stop();
}

void Worker::stop(bool wait) {
	TRC;
	commandQueue.push([this]{ delayedTerminate(); });
	wakeup();
	if (wait && thread->joinable())
		thread->join();
}

bool Worker::handleCommands() {
	TRC;
	// Get all the commands and run them. If there are none, don't wait for them.
	const MsgQueue<Procedure>::Q &commands(commandQueue.pop_all(false));
	for (const auto &f: commands)
		f();
	// We wait for more commands, don't close it
	return true;
}

void Worker::addTask(int fd, const Worker::Task &task) {
	TRC;
	// Insert it in the worker thread
	commandQueue.push(bind(&Worker::insertTask, this, fd, task));
	wakeup();
}

void Worker::run() {
	TRC;
	ExitGuard notRunning([this]{ running = false; });
	try {
		SPE::run();
		// TODO: Report termination
		LOG(DBG, "Terminating worker");
		return;
	} catch (const std::exception &e) {
		LOG_DUMP(ERROR, "Lost thread due to exception: ", [&e]{ return e.what(); });
	} catch (...) {
		LOG_DUMP(ERROR, "Lost thread due to unknown exception");
	}
	// We get here only if there was an exception (we return early on success)
	MainThread::global().lostThread(id());
}

bool Worker::alive() const {
	return running;
}

}
