/*
 * Copyright 2017, CZ.NIC z.s.p.o. (http://www.nic.cz/)
 *
 * This file is part of the pakon system.
 *
 * Pakon is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 * Pakon is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pakon.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PAKOND_ADDR_INFO_H
#define PAKOND_ADDR_INFO_H

#include "cache.hpp"
#include "types.hpp"
#include "dns.hpp"

#include <unordered_map>

namespace Pakon {

/*
 * Additional info for an IP address that is cached and can be added
 * to a flow/the IP address stored there. This structure is thread-safe
 * (it needs to be, as it is in the cache).
 */
class AddrInfo {
private:
	mutable std::mutex mutex;
	typedef std::lock_guard<std::mutex> Guard;
	/*
	 * The MAC address associated with this IP address.
	 *
	 * For simplicity we assume an IP address is only on one MAC address.
	 * This doesn't have to be exactly true, but for the network to work,
	 * it still needs to be the same computer. And we want to track
	 * computers by the MAC addresses, so this would still work.
	 */
	Address mac_;
	/*
	 * DNS information we know about this address.
	 *
	 * This is the first draft. In the future, we want to have more info
	 * about each name we store here, like the computer(s) that asked for
	 * the name ‒ if there are conflicts, we prefer the one the computer
	 * knows. Also, some measure of trustworthiness and/or exactness.
	 *
	 * For now, we store only the TTL (absolute, in this case, in seconds)
	 * of the name so we can delete it some day. We want some more fancy
	 * pruning strategies ‒ like limitting the number of names we are
	 * allowed to store so we accidentally don't eat all the memory,
	 * dropping the unlikely entries first, etc.
	 *
	 * Also, for now, we prune the old entries only in case we access the
	 * names.
	 */
	typedef std::unordered_map<DNS::Name, uint64_t> NameSet;
	NameSet names_;
	/*
	 * Drop all DNS names that are over their TTL.
	 */
	void pruneNames();
public:
	/*
	 * Provide the stored MAC address. Note that we have to copy it because
	 * of possible concurrent access.
	 */
	Address mac() const {
		Guard lock(mutex);
		return mac_;
	}
	// Set the MAC address.
	void setMac(const Address &address) {
		Guard lock(mutex);
		// Check it is a MAC address
		assert(address.size() == 6);
		mac_ = address;
	}
	/*
	 * Add or extend TTL of the given name on this address. Also, make sure
	 * there are no outdated names left.
	 *
	 * The TTL is relative, as found in the DNS packet. Note that we
	 * lower-cap the TTL for practical reasons (the spec allows 0, which
	 * means „use it but don't store“, but that's for the end computer. If
	 * we didn't store it, once the computer uses it, we wouldn't know it
	 * any more and there's not much hurt in keeping it for longer here
	 * (especially if it's not used).
	 *
	 * Also, the IP address entry never deletes the last (or the last
	 * batch) of names, simply because having stale data is probably better
	 * than having none at all. If the client doesn't contact the address,
	 * then it's OK and we won't use it. If it does, it does so based on
	 * the stale data as well.
	 */
	void touchName(const DNS::Name &name, uint32_t ttl);
	/*
	 * Prune old names and return what is left for this address.
	 */
	std::vector<DNS::Name> names();
};

/*
 * The cache for additional address info. It is indexed by IP addresses.
 */
typedef Cache<Address, AddrInfo> AddrInfoCache;

typedef AddrInfoCache::iterator PAddrInfo;

/*
 * Unlike the active flows, we need this one global, not per-dissector. That is
 * because we can find out info about an IP address on a different dissector
 * than it appears (eg. we can see a domain name in a DNS packet sent over IPv4
 * belonging to an IPv6 address).
 */
extern AddrInfoCache addrInfoCache;

}

#endif
