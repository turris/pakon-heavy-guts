/*
 * Copyright 2016-2017, CZ.NIC z.s.p.o. (http://www.nic.cz/)
 *
 * This file is part of the pakon system.
 *
 * Pakon is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 * Pakon is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pakon.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PAKOND_DISSECT_H
#define PAKOND_DISSECT_H

#include "flow.hpp"
#include "cache.hpp"

#include <vector>

namespace Pakon {

class Packet;

class Dissector : private NonCopyable {
private:
	std::mutex mutex;
	// Flows we consider still active
	FlowCache activeFlows;
	/*
	 * We keep flows that had some activity during the last
	 * 5-second window. On the end of each 5 seconds, we report
	 * the flows here and empty it.
	 */
	Flows delayed;
	// TODO: Make this configurable
	static constexpr size_t maxActiveFlowCount = 65536;
	/*
	 * Examine a packet that is being suspect of being a DNS packet. If any
	 * interesting addresses are found there, store them to corresponding
	 * IP addresses for future use. Do nothing if it turns out this wasn't
	 * really a DNS packet after all.
	 */
	void maybeDns(const Packet &packet);
public:
	Dissector();
	/*
	 * Process a batch of packets. Each one is assigned to a flow.
	 * Set of all „interesting“ flows is returned (flows where some
	 * event happened).
	 */
	Flows process(std::vector<Packet> &batch);
	// Run timed-based operations
	Flows timeouts();
};

/*
 * TODO: Think about when the flows are deleted and if they can access something
 * inside the dissector (eg. NDPI) when deleted (if they can get deleted after the dissector).
 */

}

#endif
