/*
 * Copyright 2016, CZ.NIC z.s.p.o. (http://www.nic.cz/)
 *
 * This file is part of the pakon system.
 *
 * Updater is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 * Updater is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Updater.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "packet.hpp"
#include "serialize.hpp"
#include "type_serialization.hpp"

#include <libnetfilter_queue/libnetfilter_queue.h>
#include <netinet/in.h>
#include <linux/netfilter.h>
#include <sstream>
#include <ios>
#include <iomanip>

using std::unordered_map;
using std::string;
using std::tie;
using std::ostringstream;

namespace Pakon {

template<> class JSORiazible<Flow> {
public:
	template<class Stream> static void jsorialize(Stream &stream, const Flow &flow) {
		stream << flow.toJSON();
	}
};

template<> class JSORiazible<ProtoLevel> {
public:
	template<class Stream> static void jsorialize(Stream &stream, const ProtoLevel &level) {
		stream << "{\"proto\":\"";
		switch (level.type) {
			case ProtoLevel::TCP:
				stream << "TCP";
				break;
			case ProtoLevel::UDP:
				stream << "UDP";
				break;
			case ProtoLevel::IPv4:
				stream << "IP";
				break;
			case ProtoLevel::IPv6:
				stream << "IPv6";
				break;
			case ProtoLevel::Ethernet:
				stream << "Ethernet";
				break;
			case ProtoLevel::Unknown:
				stream << "Unknown";
				break;
		}
		stream << "\",\"src\":";
		Pakon::jsorialize(stream, level.src.toString());
		stream << ",\"dst\":";
		Pakon::jsorialize(stream, level.dst.toString());
		stream << "}";
	}
};

template<> class JSORiazible<std::pair<uint16_t, uint16_t>> {
public:
	template<class Stream> static void jsorialize(Stream &stream, const std::pair<uint16_t, uint16_t> &v) {
		stream << "[" << v.first << "," << v.second << "]";
	}
};

template<> class JSORiazible<Blob> {
public:
	template<class Stream> static void jsorialize(Stream &stream, const Blob &blob) {
		Pakon::jsorialize(stream, blob.toString());
	}
};

// Some interesting protocols inside IP packet (we simply ignore the rest)
enum IpApp {
	IPv4 = 4,
	TCP = 6,
	UDP = 17,
	IPv6 = 41,
	Ethernet = 97,
	// TODO: Other protos, like SCTP, ICMP, etc.
};

void Packet::parse() {
	Blob pkt(this->data, data_len);
	LOG(TRACE, "Data: ", [&pkt] { return pkt.toString(); });
	int proto = -1;
	do {
		uint8_t vbyte = pkt.get<uint8_t>();
		uint8_t version = bitsExtract(vbyte, 0, 4);
		if (version == 4) {
			uint8_t ihl = bitsExtract(vbyte, 4, 4);
			// Skip several stupid fields we are not interested in
			pkt.skip(8);
			// The protocol inside
			proto = pkt.get<uint8_t>();
			LOG(TRACE, "Proto", (unsigned) proto);
			// Skip the header checksum
			pkt.skip(2);
			// Get the addresses
			Blob src(pkt.slice(4));
			Blob dst(pkt.slice(4));
			store<PKT_level>(ProtoLevel{ProtoLevel::IPv4, Address(src), Address(dst)});
			pkt.skip((ihl - 5) * 4);
		} else if (version == 6) {
			// Some stupid fields
			pkt.skip(5);
			proto = pkt.get<uint8_t>();
			// Skip the hop limit
			pkt.skip(1);
			// The addresses
			Blob src(pkt.slice(16));
			Blob dst(pkt.slice(16));
			store<PKT_level>(ProtoLevel{ProtoLevel::IPv6, Address(src), Address(dst)});
		} else
			LOG(DBG, "Unknown IPv6 version: ", version);
		// Handle possible ethernet encapsulation
	} while (proto == IpApp::IPv4 || proto == IpApp::IPv6);
	switch (proto) {
		case IpApp::TCP: {
			// The ports (store to two places)
			Blob src(pkt.slice(2));
			Blob dst(pkt.slice(2));
			store<PKT_level>(ProtoLevel{ ProtoLevel::TCP, Address(src), Address(dst) });
			store<PKT_ports>(std::make_pair(src.get<uint16_t>(true), dst.get<uint16_t>(true)));
			// Skip the sequence number and ack number
			pkt.skip(8);
			uint16_t flags = pkt.get<uint16_t>(true);
			// Data offset
			uint8_t doff = bitsExtract(flags, 0, 4);
			uint16_t tcp_flags = bitsExtract(flags, 4, 12);
			store<PKT_tcp_flags>(tcp_flags);
			// We already ate 14 bytes of TCP, skip the rest of the header
			pkt.skip(doff * 4 - 14);
			break;
		}
		case IpApp::UDP: {
			Blob src(pkt.slice(2));
			Blob dst(pkt.slice(2));
			store<PKT_level>(ProtoLevel{ ProtoLevel::UDP, Address(src), Address(dst) });
			store<PKT_ports>(std::make_pair(src.get<uint16_t>(true), dst.get<uint16_t>(true)));
			pkt.skip(4);
			break;
		}
		default: {
			uint8_t protoBase = proto;
			// At least split the flows by the sub-protocol
			Blob b(&protoBase, sizeof protoBase);
			store<PKT_level>(ProtoLevel{ ProtoLevel::Unknown, Address(b), Address(b)});
			break;
		}
	}
	// Whatever is left is the payload
	store<PKT_payload>(pkt.rest());
	store<PKT_ip_proto>(proto);
	LOG(TRACE, "Packet: ", [this]{ return toJSON(*this); });
	flowMatchCompute();
}

Packet::Packet(struct nfq_data *data, const unordered_map<string, string> &flags, Direction direction, uint64_t timestamp, uint64_t timestamp_monotonic) {
	TRC;
	struct nfqnl_msg_packet_hdr *header = nfq_get_msg_packet_hdr(data);
	if (!header)
		throw Unparsable("Can't parse the packet id");
	// passing just header->packet_id to ntohl causes compile error: "cannot bind packed field 'header->nfqnl_msg_packet_hdr::packet_id' to 'unsigned int&'"
	unsigned long packet_id_n = header->packet_id;
	store<PKT_id>(ntohl(packet_id_n));
	uint8_t *data_ptr;
	int len = nfq_get_payload(data, &data_ptr);
	this->data = data_ptr;
	if (len < 0)
		throw Unparsable("Missing the packet content");
	data_len = len;
	store<PKT_mark>(nfq_get_nfmark(data));
	store<PKT_qflags>(&flags);
	store<PKT_direction>(direction);
	// Store a timestamp of the packet. The nfq offers one, but it is there only sometimes and different packets have obviously different epochs, so it's not usable at all. So we use our own.
	store<PKT_timestamp>(timestamp);
	store<PKT_timestamp_monotonic>(timestamp_monotonic);
	ProtoLevel ethLevel;
	ethLevel.type = ProtoLevel::Ethernet;
	// It is sad, but we don't get all the MAC addresses. Only the source one and only when the packet is incoming and only sometimes.
	struct nfqnl_msg_packet_hw *hwaddr = nfq_get_packet_hw(data);
	if (hwaddr) {
		const Blob addr(hwaddr->hw_addr, ntohs(hwaddr->hw_addrlen));
		ethLevel.src = Address(addr);
	}
	store<PKT_level>(ethLevel);
	parse();
}

Packet::Packet(const uint8_t *data, size_t size) :
	data_len(size),
	data(data)
{
	TRC;
	store<PKT_id>(0);
	store<PKT_direction>(Direction::OUT);
	store<PKT_mark>(0);
	store<PKT_timestamp>(0);
	store<PKT_timestamp_monotonic>(0);
	static const unordered_map<string, string> fake_flags;
	store<PKT_qflags>(&fake_flags);
	parse();
}

const string &Packet::flowMatch() const {
	return flowMatchValue;
}

void Packet::flowMatchCompute() {
	const auto addrs = field<PKT_level>();
	// Skip the ethernets at the beginning ‒ one direction wouldn't have the address and changing MAC mid-flow is allowed.
	bool active = false;
	// TODO: Maybe compute the size first and reserve()?
	for (const auto &l: addrs)
		if (active || l.type != ProtoLevel::Ethernet) {
			active = true;
			// We want both directions to be in the same flow, so we must swap the addresses according to the direction
			if (field<PKT_direction>() == Direction::IN)
				flowMatchValue += static_cast<char>(l.type) + l.src.binString() + l.dst.binString();
			else
				flowMatchValue += static_cast<char>(l.type) + l.dst.binString() + l.src.binString();
		}

}

}
