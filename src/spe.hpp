/*
 * Copyright 2016, CZ.NIC z.s.p.o. (http://www.nic.cz/)
 *
 * This file is part of the pakon system.
 *
 * Pakon is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 * Pakon is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pakon.  If not, see <http://www.gnu.org/licenses/>.
 */

// SPE - self-pipe epoll

#ifndef PAKOND_SPE_H
#define PAKOND_SPE_H

#include "util.hpp"

#include <functional>
#include <list>

namespace Pakon {

/*
 * An epoll event loop that can be woken up by a self-pipe.
 */
class SPE : private NonCopyable {
public:
	/*
	 * A task that happens when the file descriptor is readable.
	 *
	 * It can terminate with either:
	 * * true: it is called again next time
	 * * false: the file descriptor is removed and closed
	 * * throwing TerminateBatchException the loop is executed again (may be needed if some other tasks got removed, or something)
	 * * throwing TerminateLoopException the loop terminates
	 * * other exception: the loop propagates the exception
	 */
	typedef std::function<bool()> Task;
	class TerminateBatchException {};
	class TerminateLoopException {};
	struct TaskHolder;
private:
	static constexpr size_t periodicInterval = 5000;
	Procedure periodic;
	uint64_t lastPeriodic = 0;
	int wake_read_fd = -1, wake_write_fd = -1, epoll_fd = -1;
	std::list<std::unique_ptr<TaskHolder> > taskHolders;
	bool terminate = false;
	int timeoutCompute() const;
protected:
	// If you want to terminate after the current batch.
	void delayedTerminate() { terminate = true; }
	/*
	 * The task run when we wake up by the wakeup FD.
	 * The periodic is called every 5 seconds, if it is set
	 */
	explicit SPE(const Task &wakeup, const Procedure &periodic = Procedure());
	~ SPE();
	/*
	 * Run the wakeup task, but delayed through the loop inside.
	 * Possibly in another thread.
	 */
	void wakeup();
	// Run the event loop
	void run();
public:
	// Add a new task with FD
	const TaskHolder *insertTask(int fd, const Task &task);
	// Drop a task (be careful not to call when it no longer exists!)
	void dropTask(const TaskHolder *holder);
};

}

#endif
