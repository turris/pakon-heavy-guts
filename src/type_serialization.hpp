/*
 * Copyright 2016, CZ.NIC z.s.p.o. (http://www.nic.cz/)
 *
 * This file is part of the pakon system.
 *
 * Updater is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 * Updater is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Updater.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PAKON_TYPE_SERIALIZATION_H
#define PAKON_TYPE_SERIALIZATION_H

#include "types.hpp"
#include "serialize.hpp"

namespace Pakon {

template<> class JSORiazible<Address> {
public:
	template<class Stream> static void jsorialize(Stream &stream, const Address &address) {
		Pakon::jsorialize(stream, address.toString());
	}
};

template<> class JSORiazible<Direction> {
public:
	template<class Stream> static void jsorialize(Stream &stream, Direction direction) {
		stream << (direction == Direction::IN ? "\"IN\"" : "\"OUT\"");
	}
};

template<> class JSORiazible<Family> {
public:
	template<class Stream> static void jsorialize(Stream &stream, Family family) {
		stream << (family == Family::IPv4 ? "\"IPv4\"" : "\"IPv6\"");
	}
};

template<> class JSORiazible<IpProto> {
public:
	template<class Stream> static void jsorialize(Stream &stream, IpProto proto) {
		switch (proto) {
			case IpProto::TCP:
				stream << "\"TCP\"";
				break;
			case IpProto::UDP:
				stream << "\"UDP\"";
				break;
			case IpProto::Other:
				stream << "\"?\"";
				break;
		}
	}
};

}

#endif
