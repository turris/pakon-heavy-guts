/*
 * Copyright 2017, CZ.NIC z.s.p.o. (http://www.nic.cz/)
 *
 * This file is part of the pakon system.
 *
 * Pakon is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 * Pakon is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pakon.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "ringlog.hpp"
#include "util.hpp"

#include <sys/types.h>
#include <unistd.h>
#include <cstring>
#include <atomic>
#include <vector>
#include <algorithm>
#include <fstream>

using std::atomic;
using std::vector;
using std::string;
using std::sort;
using std::min;
using std::ofstream;

/*
 * Internal workings
 *
 * There's a global (shared between thread) ring buffer. The ring buffer is
 * implemented as an array of atomic pointers to messages and an atomic
 * countern (called generation). When logging a message, the thread allocates a
 * message and fills it. Then it increments the counter, reserving a slot in
 * the buffer. It atomically swaps the pointer in the buffer with the message
 * it created and frees any possible message it received in exchange.
 *
 * As the range of the counter is dividible by the size of the ring buffer, we
 * can simply let the counter wrap around naturally, saving us the work to
 * modulo it atomically.
 *
 * Each thread has its own cache for allocated messages, so we save on
 * allocations.
 *
 * When dumping, all the messages are stolen from the buffer (exchanged with
 * NULLs). However, since new messages may be logged at the same time, the
 * stealing and logging may go in tandem, so we can get old messages
 * interspersed with new ones. To get reasonable output, we store the value of
 * the counter in each message and sort according to its value (with
 * wrap-around comparing semantics, so if we are near the wrap, it is still
 * sorted correctly).
 *
 * For this to work well, we assume some things:
 * - We steal the messages faster than we log them.
 * - The other threads won't turn the whole buffer around in the meantime
 *   between reserving the slot and placing the message there.
 *
 * However, these are soft assumptions. No disruption (eg. data races,
 * undefined behaviour or similar) can happen even if it doesn't hold. The bad
 * effects may include things like wrong ordering of the messages or some
 * messages being lost. This should still be rather rare in practice.
 */

namespace {

constexpr size_t msgSize = 1024;
/*
 * How many items there are in the buffer. This should be a power of two, so
 * wrapping the generation around doesn't cause „jumps“ in the buffer.
 */
constexpr size_t ringbufSize = 4096;
constexpr size_t cacheMaxSize = 128;

struct Msg {
	uint32_t generation;
	char msg[msgSize];
};

atomic<Msg *> buffer[ringbufSize];
atomic<uint32_t> generation;

// A cache for allocated but currently unused messages. Per thread.
// cppcheck-suppress noConstructor It doesn't have a constructor, but the only
// value that matters has an initializer
class MsgCache {
private:
	Msg *cache[cacheMaxSize] = {};
	size_t cacheSize = 0;
public:
	// When a thread terminates, we don't want to leak the messages
	~ MsgCache() {
		for (size_t i = 0; i < cacheSize; i ++)
			delete cache[i];
	}
	Msg *get() {
		if (cacheSize)
			return cache[-- cacheSize];
		else
			return new Msg;
	}
	void release(Msg *msg) {
		if (cacheSize == cacheMaxSize)
			delete msg;
		else
			cache[cacheSize ++] = msg;
	}
};

thread_local MsgCache msgCache;

// A less implementation, but with wrap-around semantics (eg. 0, 1, 2, 3, …, max, 0, 1, …)
constexpr bool wrap_less(const Msg *a, const Msg *b) {
	return (int)(a->generation - b->generation) < 0;
}

}

namespace Pakon {


void ringlog(const string &msg) {
	Msg *storage = msgCache.get();
	/*
	 * We calculate the size ourselves. We could use strncpy, but it
	 * needlessly zeroes the rest. And we would need to handle the end zero
	 * anyway, as it doesn't provide one on truncation.
	 */
	size_t msgLen = min(msg.length(), sizeof storage->msg - 1);
	memcpy(storage->msg, msg.c_str(), msgLen);
	storage->msg[msgLen] = '\0';
	/*
	 * Relaxed order: we want to only reserve the slot here. We don't
	 * synchronize any memory here yet.
	 */
	uint32_t gen = generation.fetch_add(1, std::memory_order_relaxed);
	storage->generation = gen;
	/*
	 * Release order: we make sure the message content is written and
	 * visible to other threads before we provide the pointer to it.
	 *
	 * We have no kind of acquire here, because we don't read the memory we
	 * get from the other message.
	 */
	Msg *previous = buffer[gen % ringbufSize].exchange(storage, std::memory_order_release);
	if (previous)
		msgCache.release(previous);
}

string dumpRingMem() {
	vector<Msg *> messages;
	messages.reserve(ringbufSize);
	/*
	 * Steal all the messages that are in the buffer. Do the stealing as
	 * fast as possible (without other work in between and with
	 * pre-allocated vector), so the interference around the write head of
	 * the ring buffer is as small as possible.
	 */
	for (auto &m: buffer) {
		Msg *extracted = m.exchange(nullptr, std::memory_order_acquire);
		if (extracted)
			messages.push_back(extracted);
	}
	/*
	 * The messages are usually out of order (because of how the ring
	 * buffer works, which could be worked around by finding the right
	 * place and swapping the „halves“) and we can have a round-old and new
	 * messages interspersed around the write head if we go a while with
	 * the writer in tandem, or if some reserved slots haven't been
	 * replaced yet. So we sort it according to the generation, which has a
	 * much larger range than the size of the buffer ‒ so the chance of the
	 * generation wrapping around within the messages we get is really
	 * astronomically small.
	 */
	sort(messages.begin(), messages.end(), wrap_less);
	/*
	 * Now just some boring formatting of what we got out of the buffer and
	 * releasing the messages.
	 */
	string result;
	for (const auto &msg: messages) {
		result += static_cast<const char *>(msg->msg);
		result += "\n";
		msgCache.release(msg);
	}
	return result;
}

void dumpRingFile() {
	static unsigned cnt = 0;
	string fname = "/tmp/pakond-dump." + to_string(getpid()) + "." + to_string(cnt ++);
	ofstream ofs(fname, ofstream::out | ofstream::trunc);
	ofs << dumpRingMem();
	ofs.close();
	LOG(ERROR, "Detailed logging prior to the problem is dumped into ", fname);
}

}
