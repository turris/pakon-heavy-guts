/*
 * Copyright 2016, CZ.NIC z.s.p.o. (http://www.nic.cz/)
 *
 * This file is part of the pakon system.
 *
 * Updater is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 * Updater is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Updater.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "util.hpp"

#include <syslog.h>
#include <cstdlib>
#include <cstring>

namespace Pakon {

namespace {

const int prios[] = {
	[TRACE] = LOG_MAKEPRI(LOG_DAEMON, LOG_DEBUG),
	[DBG] = LOG_MAKEPRI(LOG_DAEMON, LOG_DEBUG),
	[INFO] = LOG_MAKEPRI(LOG_DAEMON, LOG_INFO),
	[WARN] = LOG_MAKEPRI(LOG_DAEMON, LOG_WARNING),
	[ERROR] = LOG_MAKEPRI(LOG_DAEMON, LOG_ERR),
	[FATAL] = LOG_MAKEPRI(LOG_DAEMON, LOG_CRIT),
};

LogLevel name2Level(const char *name) {
	if (!name)
		name = "";
	for (size_t i = 0; i < sizeof logNames / sizeof logNames[0]; i ++)
		if (strcasecmp(name, logNames[i]) == 0)
			return static_cast<LogLevel>(i);
	// If we don't recognize it, do it at a very verbose level
	return TRACE;
}

}

// This is also limited by what is compiled in with MAX_LOG_LEVEL
LogLevel maxLogLevelStderr = TRACE;
LogLevel maxLogLevelSyslog = TRACE;
bool ringlogEnabled = false;

void doSyslog(LogLevel level, const std::string &msg) {
	syslog(prios[level], "%s", msg.c_str());
}

void loggingInitialize() {
	maxLogLevelStderr = name2Level(getenv("PAKOND_STDERR_LEVEL"));
	maxLogLevelSyslog = name2Level(getenv("PAKOND_SYSLOG_LEVEL"));
	const char *ringlogEnabledEnv = getenv("PAKOND_RINGLOG");
	ringlogEnabled = ringlogEnabledEnv && (strcasecmp(ringlogEnabledEnv, "yes") == 0);
	openlog("pakond", LOG_PID, LOG_DAEMON);
}

uint64_t timeMsec(clockid_t id) {
	struct timespec ts;
	CHECK(clock_gettime(id, &ts) != -1);
	return (uint64_t)ts.tv_sec * 1000 + (uint64_t)ts.tv_nsec / 1000000;
}

uint64_t timeGrab(clockid_t id) {
	return timeMsec(id) / 1000;
}

bool inValgrind() {
	const char *inValgrindEnv = getenv("IN_VALGRIND");
	return inValgrindEnv && (strcasecmp(inValgrindEnv, "yes") == 0);
}

}
