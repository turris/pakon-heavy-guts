/*
 * Copyright 2016, CZ.NIC z.s.p.o. (http://www.nic.cz/)
 *
 * This file is part of the pakon system.
 *
 * Pakon is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 * Pakon is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pakon.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PAKOND_REPORT_H
#define PAKOND_REPORT_H

#include "util.hpp"
#include "flow.hpp"

#include <memory>
#include <unordered_set>

namespace Pakon {

class Report : public Singleton<Report> {
private:
	friend class Singleton<Report>;
	class UnixSocket;
	class ListenSocket;
	class ReportSocket;
	class SocketHash;
	class SocketEq;
	class Private;
	typedef std::unordered_set<ReportSocket, SocketHash, SocketEq> ReportSockets;
	std::unique_ptr<Private> priv;
	Report();
	bool accept(ListenSocket *socket);
	bool read(ReportSocket *socket);
	void processFlows();
public:
	~ Report();
	/*
	 * Report there are some interesting flows that
	 * shall be sent to the user. It is possible to call
	 * safely from any thread.
	 */
	void reportFlows(const Flows &flows);
	/*
	 * Add a listening unix-domain socket.
	 * May be called only when reconfiguring (eg. without any
	 * other threads).
	 *
	 * Does not check if such a socket already exists, therefore
	 * if it does, strange things will happen.
	 */
	void addSocket(const std::string &path);
	/*
	 * Get rid of a listening unix-domain socket.
	 * May be called only when reconfiguring (eg. without any
	 * other threads).
	 */
	void removeSocket(const std::string &path);
	// If the pathname is too long
	EXCEPTION(std::runtime_error, PathTooLong);
};

}

#endif
