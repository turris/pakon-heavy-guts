/*
 * Copyright 2016, CZ.NIC z.s.p.o. (http://www.nic.cz/)
 *
 * This file is part of the pakon system.
 *
 * Pakon is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 * Pakon is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pakon.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "lua_convert.hpp"

#include <string>
#include <cassert>

using std::string;

namespace Pakon {
namespace Lua {

void push(lua_State *state, bool value) {
	lua_pushboolean(state, value);
}
void push(lua_State *state, const std::string &value) {
	lua_pushlstring(state, value.c_str(), value.size());
}
void push(lua_State *state, const char *value) {
	lua_pushstring(state, value);
}
void push(lua_State *state, const Value &value) {
	assert(state == value.state);
	value.extractToStack();
}
void push(lua_State *state, const AnyFunction &value) {
	push(state, static_cast<const Value &>(value));
}

void extract(lua_State *state, int index, bool &value) {
	value = lua_toboolean(state, index);
}
void extract(lua_State *state, int index, std::string &value) {
	if (lua_isstring(state, index)) {
		size_t len;
		const char *str = lua_tolstring(state, index, &len);
		value = std::string(str, len);
	} else
		throw Mismatch("String expected on index " + to_string(index));
}
void extract(lua_State *state, int index, Value &value) {
	value.store(index, state);
}
void extract(lua_State *state, int index, AnyFunction &value) {
	extract(state, index, static_cast<Value &>(value));
}

void Value::extractToStack() const {
	pushId();
	lua_rawget(state, LUA_REGISTRYINDEX);
}

string Value::id() const {
	/*
	 * Yes, we embed the value of the this pointer
	 * in the ID. We copy it (because we can't take address
	 * of this as it is not really a variable) and then type-cast
	 * pointer to that pointer so it can be fed into string
	 * constructor.
	 *
	 * This is actually allowed even by the strict aliasing
	 * rules.
	 */
	const Value *v = this;
	const char *data = reinterpret_cast<const char *>(&v);
	return string("value_held_") + string(data, sizeof v);
}

void Value::pushId() const {
	const string &id(this->id());
	lua_pushlstring(state, id.c_str(), id.size());
}

void Value::store(int index, lua_State *newState) {
	if (newState) {
		assert(newState == state || !state);
		state = newState;
	}
	StackGuard stackGuard(state);
	// Prepare the key and the value on top. We first copy the value once, to make sure we know its index after we add the key
	lua_pushvalue(state, index);
	// Get the key
	pushId();
	// Get another copy of the value, now at the right place on the stack
	lua_pushvalue(state, -2);
	// Insert it into the registry
	lua_rawset(state, LUA_REGISTRYINDEX);
	// The stack gets cleaned up automatically now
}

Value::Value(lua_State *state) :
	state(state)
{} // We don't store anything in there, which acts as nil

Value::Value(lua_State *state, int index) :
	state(state)
{
	store(index);
}

Value::Value(const Value &other) :
	state(other.state)
{
	if (!other.state)
		return;
	StackGuard guard(state);
	other.extractToStack();
	// Extract the other's value onto the stack and then delegate to the other constructor
	store(-1);
}

Value &Value::operator =(const Value &other) {
	if (&other != this) {
		assert(state == other.state || !state);
		state = other.state;
		if (state) {
			StackGuard guard(state);
			other.extractToStack();
			store(-1);
		}
	}
	return *this;
}

Value::~ Value() {
	if (!state)
		return;
	StackGuard guard(state);
	lua_pushnil(state);
	store(-1);
}

}
}
