/*
 * Copyright 2016, CZ.NIC z.s.p.o. (http://www.nic.cz/)
 *
 * This file is part of the pakon system.
 *
 * Updater is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 * Updater is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Updater.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "spe.hpp"

#include <sys/epoll.h>
#include <unistd.h>
#include <cstring>
#include <cerrno>
#include <algorithm>
#include <sys/types.h>
#include <sys/socket.h>
#include <vector>

namespace Pakon {

struct SPE::TaskHolder {
	const int fd;
	const Task task;
	std::list<std::unique_ptr<TaskHolder> >::iterator iterator;
	struct epoll_event event;
	TaskHolder(int fd, const Task &task) :
		fd(fd),
		task(task)
	{
		TRC;
		memset(&event, 0, sizeof event);
		event.events = EPOLLIN;
		event.data.ptr = this;
	}
	~TaskHolder() {
		TRC;
		if (fd != -1)
			// This also removes the file descriptor from epoll
			close(fd);
	}
};

SPE::SPE(const SPE::Task &wakeup, const Procedure &periodic) :
	periodic(periodic),
	lastPeriodic(timeMsec(CLOCK_MONOTONIC)),
	epoll_fd(epoll_create(42)) /* 42 is arbitrary number; man says to pass any positive number that is ignored. */
{
	TRC;
	CHECK(epoll_fd != -1);
	int pipeFds[2];
	// Due to send/recv complaining about socket operation on non-socket, we use socketpair instead of pipe. We use it as a pipe otherwise.
	CHECK(socketpair(PF_LOCAL, SOCK_STREAM, 0, pipeFds) != -1);
	ExitGuard pipeGuard([pipeFds]{
		// This gets called only when we throw exception: don't check and throw another.
		close(pipeFds[0]);
		close(pipeFds[1]);
	});
	wake_write_fd = pipeFds[1];
	wake_read_fd = pipeFds[0];
	// Make sure we install a handler inside the thread that allows us to issue commands
	insertTask(pipeFds[0], [this, wakeup]{
		// First, read the commands from the file descriptor, to reset the wakeup
		const size_t bufsize = 1024;
		uint8_t buffer[bufsize];
		const ssize_t result = recv(wake_read_fd, buffer, bufsize, MSG_DONTWAIT);
		// These errors are non-fatal. Worst case we would get woken up again.
		CHECK(result != -1 || (errno == EAGAIN || errno == EWOULDBLOCK || errno == EINTR));
		return wakeup();
	});
	// We didn't throw, so keep the pipes open (the destructor can handle it now).
	pipeGuard.abort();
}

SPE::~ SPE() {
	TRC;
	// Close bunch of file descriptors (wake_read_fd is a task, so it is deleted automatically)
	if (wake_write_fd != -1)
		close(wake_write_fd);
	if (epoll_fd != -1)
		close(epoll_fd);
	// The tasks get destroyed here and therefore they close their FDs
}

const SPE::TaskHolder *SPE::insertTask(int fd, const SPE::Task &task) {
	TRC;
	LOG(DBG, "Inserting task with FD ", fd);
	// Use emplace instead of emplace_back, as this gives us the new iterator
	auto it = taskHolders.emplace(taskHolders.end(), new TaskHolder(fd, task));
	// Store the iterator in the newly created element
	taskHolders.back()->iterator = it;
	// Now place the thing into epoll
	CHECK(epoll_ctl(epoll_fd, EPOLL_CTL_ADD, fd, &taskHolders.back()->event) != -1);
	return taskHolders.back().get();
}

void SPE::dropTask(const SPE::TaskHolder *holder) {
	TRC;
	// When it is erased, the unique_ptr deletes the holder and that one closes the FD, removing it from epoll.
	taskHolders.erase(holder->iterator);
}

int SPE::timeoutCompute() const {
	if (!periodic) // Nothing to run, don't wake
		return -1;
	uint64_t now = timeMsec(CLOCK_MONOTONIC);
	if (now >= lastPeriodic + periodicInterval) // The time is up, wake up imediatelly
		return 0;
	return lastPeriodic + periodicInterval - now;
}

void SPE::run() {
	try {
		while (!terminate && !taskHolders.empty()) {
			const size_t event_cnt = std::min<size_t>(50, taskHolders.size());
			std::vector<struct epoll_event> events(event_cnt);
			int epoll_result = epoll_wait(epoll_fd, &events.front(), event_cnt, timeoutCompute());
			LOG(TRACE, "Epoll tick with ", epoll_result, " events");
			if (epoll_result == -1)
				CHECK(errno == EINTR); // Others are forbidden
			else {
				try {
					for (int i = 0; i < epoll_result; i ++) {
					const struct epoll_event &event = events[i];
					TaskHolder *holder = static_cast<TaskHolder *>(event.data.ptr);
					if (event.events & (EPOLLIN | EPOLLERR | EPOLLHUP)) {
						LOG(TRACE, "Event on task FD ", holder->fd);
						if (!holder->task()) {
							LOG(DBG, "Task with FD ", holder->fd, " terminated");
							dropTask(holder);
						}
					} else
						LOG(WARN, "Unknown event on task FD ", holder->fd, ": ", event.events);
					}
				} catch (const TerminateBatchException &) {
					LOG(TRACE, "Terminating batch prematurely");
				}
			}
			// Run the periodic task, if the time is up and there's one
			if (periodic) {
				uint64_t now = timeMsec(CLOCK_MONOTONIC);
				if (now >= lastPeriodic + periodicInterval) {
					lastPeriodic = now;
					periodic();
				}
			}
		}
		LOG(TRACE, "Terminating loop");
	} catch (const TerminateLoopException &) {
		LOG(TRACE, "Terminating loop prematurely");
	}
}

void SPE::wakeup() {
	const ssize_t result = send(wake_write_fd, "w", 1, MSG_DONTWAIT);
	if (result == -1) {
		if (errno == EINTR)
			// In case of interruption, we have to try again
			return wakeup();
		// The pipe is full. That's OK, it shall wake up anyway.
		CHECK(errno == EAGAIN || errno == EWOULDBLOCK);
	}
}

}
