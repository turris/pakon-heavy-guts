/*
 * Copyright 2016, CZ.NIC z.s.p.o. (http://www.nic.cz/)
 *
 * This file is part of the pakon system.
 *
 * Updater is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 * Updater is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Updater.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "mainthread.hpp"
#include "configurator.hpp"
#include "report.hpp"

#include <functional>
#include <csignal>
#include <cstring>
#include <cassert>
#include <atomic>

using std::bind;
using std::vector;
using std::string;
using std::unordered_map;

namespace Pakon {

namespace {

// Signals leading to „nice“ termination
const int termSignals[] = {
	SIGINT,
	SIGQUIT,
	SIGTERM
};

}

/*
 * Installs a signal handler. When it is called, it calls the
 * wakeup function passed in the constructor. Then it can
 * be examined (and reset) by calling getPending().
 */
class MainThread::Signal : private Pakon::NonCopyable {
private:
	typedef unordered_map<int, Signal *> Signals;
	static Signals signals;
	Procedure wakeup;
	struct sigaction acold;
	bool reset = false;
	int number;
	std::atomic<bool> pending = {false};
	void called() {
		pending = true;
		if (wakeup)
			wakeup();
	}
	static void called(int signal) {
		signals.at(signal)->called();
	}
public:
	Signal(int number, const Procedure &wakeup = Procedure()) :
		wakeup(wakeup),
		number(number)
	{
		const auto &inserted = signals.insert(Signals::value_type(number, this));
		CHECK(inserted.second);
		struct sigaction acnew;
		memset(&acnew, 0, sizeof acnew);
		acnew.sa_handler = called;
		CHECK(sigaction(number, &acnew, &acold) == 0);
		reset = true;
	}
	~Signal() {
		signals.erase(number);
		if (reset)
			// Don't check - if we could set the handler, we can set it back
			sigaction(number, &acold, 0);
	}
	// Ask if it is pending and reset it.
	bool getPending() {
		return pending.exchange(false);
	}
};

unordered_map<int, MainThread::Signal *> MainThread::Signal::signals;
MainThread *MainThread::instance;

MainThread::MainThread(const vector<string> &args) :
	SPE(bind(&MainThread::wokenUp, this), bind(&MainThread::periodicTasks, this)),
	args(args)
{
	assert(!instance);
	instance = this;
}

MainThread:: ~MainThread() {
	instance = nullptr;
}

bool MainThread::wokenUp() {
	for (int sig: termSignals)
		if (signals.at(sig)->getPending())
			throw TerminateLoopException();
	if (signals.at(SIGHUP)->getPending()) {
		reconfigure();
		// Make sure any sockets closed during reconfigure are not in the batch ‒ get a fresh one
		throw TerminateBatchException();
	}
	/*
	 * We don't do the swap-iterate trick here, as we expect p() to throw Terminate* exceptions sometimes.
	 * This is a limit to prevent starvation ‒ we want to handle the other events as well.
	 */
	for (size_t i = 0; i < 100; i ++) {
		Procedure p;
		{
			std::lock_guard<std::recursive_mutex> lock(delayMutex);
			// This is effectively the loop condition, but inside the mutex
			if (delayed.empty())
				goto RUN_OUT;
			p.swap(delayed.front());
			delayed.pop_front(); // Remove it from there before running
		}
		p();
	}
	// If we haven't run out, but hit the limit, wake it up again
	wakeup();
RUN_OUT:
	return true;
}

void MainThread::run() {
	const auto &wakeProc = bind(&MainThread::wakeup, this);
	/*
	 * Initialize the signal handlers.
	 * (We could actually keep them even after termination of the function,
	 * but we simply clean up the mess).
	 */
	ExitGuard signalGuard([this]{ signals.clear(); });
	signals[SIGHUP] = SignalPtr(new Signal(SIGHUP, wakeProc));
	for (int sig: termSignals)
		signals[sig] = SignalPtr(new Signal(sig, wakeProc));
	reconfigure();
	SPE::run();
	// Go to an empty configuration, which terminates all threads.
	Configurator::Transaction t;
}

void MainThread::reconfigure() {
	// We hold a transaction through the reconfiguration and auto-terminate it
	Configurator::Transaction t;
	for (const auto &arg: args)
		interpreter.doFile(arg);
}

void MainThread::delay(const Procedure &procedure) {
	std::lock_guard<std::recursive_mutex> lock(delayMutex);
	delayed.emplace_back(procedure);
	wakeup();
}

void MainThread::periodicTasks() {
	Flows flows;
	for (auto d: Configurator::instance().allDissectors()) {
		const Flows &nf(d->timeouts());
		flows.insert(nf.begin(), nf.end());
	}
	Report::instance().reportFlows(flows);
}

void MainThread::lostThread(uint64_t thread) {
	// Make sure the restart of thread is called in the main thread
	delay(bind(&Configurator::restartThread, &Configurator::instance(), thread));
}

}
