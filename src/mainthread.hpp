/*
 * Copyright 2016, CZ.NIC z.s.p.o. (http://www.nic.cz/)
 *
 * This file is part of the pakon system.
 *
 * Pakon is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 * Pakon is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pakon.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PAKOND_MAINTHREAD_H
#define PAKOND_MAINTHREAD_H

#include "spe.hpp"
#include "interpreter.hpp"

#include <vector>
#include <string>
#include <unordered_map>
#include <memory>
#include <mutex>
#include <deque>

namespace Pakon {

/*
 * This class runs in the main thread, handling reports of
 * other threads, sending out info to the aggregator and
 * managing signals.
 *
 * It is something like a singleton, however it is enforced
 * by a condition in the constructor and it is possible
 * to destroy (and then create a new instance).
 */
class MainThread : public SPE {
private:
	class Signal;
	typedef std::unique_ptr<Signal> SignalPtr;
	std::unordered_map<int, SignalPtr> signals;
	const std::vector<std::string> args;
	Interpreter interpreter;
	// Mutex for the queue of delayed tasks
	std::recursive_mutex delayMutex;
	std::deque<Procedure> delayed;
	bool wokenUp();
	void reconfigure();
	static MainThread *instance;
	void periodicTasks();
public:
	explicit MainThread(const std::vector<std::string> &args);
	~ MainThread();
	void run();
	/*
	 * Call this procedure some time later, in the main thread.
	 *
	 * This ensures unwinding of the current stack before it
	 * is called as well as running the task in the main thread.
	 */
	void delay(const Procedure &procedure);
	/*
	 * Notify the main thread that a worker thread is lost due to
	 * an exception. It shall be restarted.
	 */
	void lostThread(uint64_t id);
	// Return the global instance.
	static MainThread &global() { return *instance; }
};

}

#endif
