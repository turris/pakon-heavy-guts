/*
 * Copyright 2016, CZ.NIC z.s.p.o. (http://www.nic.cz/)
 *
 * This file is part of the pakon system.
 *
 * Pakon is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 * Pakon is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pakon.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PAKOND_FLOW_H
#define PAKOND_FLOW_H

#include "util.hpp"
#include "introsp.hpp"
#include "types.hpp"
#include "cache.hpp"
#include "dns.hpp"

#include <mutex>
#include <unordered_set>
#include <memory>
#include <tuple>
#include <list>

namespace Pakon {

class Dissector;
class Packet;

// The verdict, mark mask and mark value
struct Verdict {
	uint32_t action;
	uint32_t mark_mask;
	uint32_t mark_value;
	Verdict(uint32_t action, uint32_t mark_mask, uint32_t mark_value) :
		action(action),
		mark_mask(mark_mask),
		mark_value(mark_value)
	{}
	Verdict() = default;
};
// The flow id. It is the current time, computer runtime time (both in seconds), thread-id and sequence number.
struct FID {
	uint64_t ctime;
	uint64_t rtime;
	std::thread::id tid;
	uint64_t seq;
	explicit FID(uint64_t curtime);
	FID() = default;
};

// Some more details about a domain name
// This is empty for now, just a placeholder. And it is tentatively here, but may move somewhere else.
struct NameDetails {
};

// Named Field
#define NF(NAME) struct FL_##NAME { static constexpr const char *name() { return #NAME; } }

// The verdict of the whole flow
NF(verdict);
NF(id);
NF(status);
NF(starttime);
NF(starttime_monotonic);
NF(lasttime); // Time of last activity
NF(lasttime_monotonic);
NF(flags);
NF(direction);
// The IP addresses and their family
NF(family);
NF(local);
NF(remote);
// TCP, UDP, …
NF(ip_proto);
NF(ip_proto_raw);
// The address types
NF(port);
NF(ip);
NF(mac);
NF(name);
// The transferred counter in each direction
NF(in);
NF(out);

// These are inside the flow half
NF(packets);
NF(payload);
NF(total);
NF(closed);

#undef NF

using Introspectable::Field;
using Introspectable::Simple;
using Introspectable::Optional;
using Introspectable::Explicit;

class FlowHalf : public Introspectable::Struct<
	Field<uint64_t, Simple, FL_packets>,
	Field<uint64_t, Simple, FL_payload>,
	Field<uint64_t, Simple, FL_total>,
	Field<bool, Simple, FL_closed>
> {
public:
	FlowHalf();
	void update(const Packet &pkt);
};

class Addresses : public Introspectable::Struct<
	Field<Address, Simple, FL_ip>,
	Field<std::unordered_map<DNS::Name, NameDetails>, Simple, FL_name>,
	Field<Address, Optional<Explicit>, FL_mac>,
	Field<uint16_t, Optional<Explicit>, FL_port>
> {};

enum class FlowStatus {
	// It is a new flow without much info about it, just born
	New,
	/*
	 * This is an update of the information about the flow, and
	 * it is simply continuing.
	 */
	Ongoing,
	/*
	 * Both ends terminated the flow. Not all flows can be terminated.
	 * Also, we still keep the flow around for a bit of a time.
	 */
	Closed,
	/*
	 * We drop the flow, likely because it is inactive for some time, or
	 * because we have a full cache.
	 */
	Dropped
};

/*
 * One netflow. It holds some info about that flow, which
 * may be exported to lua or json (TBD). It also keeps
 * a verdict for the packets assigned to it and has a buffer
 * for list of last events which happened on it.
 *
 * It is synchronized by a mutex to make it usable from
 * multiple threads.
 *
 * Only the Dissector creates flows.
 */
class Flow : private NonCopyable {
private:
	// We are allowed to use mutex in const methods
	mutable std::mutex mutex;
	// Things that can be printed or exported to lua
	Introspectable::Struct<
		Field<Verdict, Simple, FL_verdict>,
		Field<FID, Simple, FL_id>,
		Field<FlowStatus, Simple, FL_status>,
		Field<uint64_t, Simple, FL_starttime>,
		Field<uint64_t, Simple, FL_starttime_monotonic>,
		Field<uint64_t, Simple, FL_lasttime>,
		Field<uint64_t, Simple, FL_lasttime_monotonic>,
		Field<Direction, Simple, FL_direction>,
		Field<Family, Simple, FL_family>,
		Field<Addresses, Simple, FL_local>,
		Field<Addresses, Simple, FL_remote>,
		Field<IpProto, Simple, FL_ip_proto>, // TCP/UDP/…
		Field<unsigned, Simple, FL_ip_proto_raw>, // Raw value of the above
		Field<FlowHalf, Simple, FL_in>,
		Field<FlowHalf, Simple, FL_out>,
		// Arbitrary string flags, combined from various sources
		Field<std::unordered_map<std::string, std::string>, Simple, FL_flags>
		> fields;
	friend class Dissector;
	// If no data go there for this long, drop it as dead
	static constexpr uint32_t inactivityTime = 900 * 1000;
	// If no data go there after the flow has been closed, drop it as dead
	static constexpr uint32_t closedTime = 15 * 1000;
public:
	const std::string match;
	// Get the verdict currently assigned to the flow
	const Verdict &verdict() const {
		std::lock_guard<std::mutex> lock(mutex);
		return fields.field<FL_verdict>();
	}
	std::string toJSON() const;
	/*
	 * Add another packet to the flow. It updates the counters, last
	 * activity time and possibly extracts some more missing fields.
	 *
	 * Returns if something interesting happened (eg. changed state,
	 * added some fields).
	 *
	 * The initial specifies if it is the first update with the very
	 * first packet of the flow.
	 */
	bool update(const Packet &packet, bool initial);
	// Create the flow from a first packet
	explicit Flow(const Packet &packet);
	// Change the status of the flow
	void setStatus(FlowStatus status);
	// Compute the TTL based on the flow status and now
	uint64_t ttl(uint64_t now) const;
	/*
	 * Do we have a reason to suspect this flow to be DNS?
	 */
	bool maybeDns() const;
};

typedef Cache<std::string, Flow, 13, true, true, false> FlowCache;
typedef FlowCache::iterator PFlow;
// Bunch of flows, in no particular order.
typedef FlowCache::Iterators Flows;

}

#endif
