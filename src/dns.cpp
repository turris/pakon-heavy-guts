/*
 * Copyright 2017, CZ.NIC z.s.p.o. (http://www.nic.cz/)
 *
 * This file is part of the pakon system.
 *
 * Pakon is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 * Pakon is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pakon.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "dns.hpp"

#include <algorithm>
#include <cassert>

using std::string;
using std::vector;
using std::transform;

namespace {

char charTolower(char c) {
	// We explicitly want to work on ascii
	if (c >= 'A' && c <= 'Z')
		c -= 'A' - 'a';
	return c;
}

/*
 * Convert a string to lower case. This explicitly is *not* locale aware, as
 * DNS's lower-case is ASCII only. Furthermore, we must not break on
 * characters/bytes outside of ASCII, but keep them as they are.
 */
string strTolower(const string &str) {
	string result;
	result.resize(str.size(), 0);
	transform(str.begin(), str.end(), result.begin(), charTolower);
	return result;
}

}

namespace Pakon {
namespace DNS {

Name ParsedName::toOwned() const {
	Name result;
	const ParsedName *label = this;
	while (label) {
		result.labels_.emplace_back(label->label_.begin(), label->label_.end());
		/*
		 * Convert the from the shared_ptr to a raw pointer (more
		 * convenient for local manipulation and without performance
		 * penalties for all the atomic reference counters).
		 */
		label = &*label->next_;
	}
	return result;
}

string Name::toString() const {
	/*
	 * TODO: This is probably a bit wrong (for example we are likely
	 * supposed to escape embedded dots in the labels). But for now, we are
	 * content with this and will get something better once we have full
	 * handling of the names.
	 */
	return strTolower(join(labels_, "."));
}

bool Name::operator ==(const Name &other) const {
	if (labels_.size() != other.labels_.size())
		return false;
	for (size_t i = 0; i < labels_.size(); i ++)
		if (strTolower(labels_[i]) != strTolower(other.labels_[i]))
			return false;
	return true;
}

bool Name::operator ==(const ParsedName &other) const {
	return (*this) == other.toOwned();
}

Message::Message(Blob packet) :
	data(packet)
{
	try {
		assert(packet.pos() == packet.begin());
		// There's no way to have a longer DNS packet
		assert(packet.size() <= 0xffff);
		// We are not interested in the ID
		packet.skip(2);
		// Decode the flags
		const uint16_t flags(packet.get<uint16_t>(true));
		qr_ = static_cast<QR>(bitsExtract(flags, 0, 1));
		const uint16_t opcode(bitsExtract(flags, 1, 4));
		opcode_ = opcode ? Opcode::Other : Opcode::Query;
		authoritative_ = bitsExtract(flags, 5, 1);
		truncated_ = bitsExtract(flags, 6, 1);
		const uint16_t rcode(bitsExtract(flags, 12, 4));
		rcode_ = rcode >= static_cast<uint16_t>(RCode::Other) ? RCode::Other : static_cast<RCode>(rcode);
		const uint16_t qcount(packet.get<uint16_t>(true));
		const uint16_t ancount(packet.get<uint16_t>(true));
		const uint16_t nscount(packet.get<uint16_t>(true));
		const uint16_t adcount(packet.get<uint16_t>(true));
		records_.reserve(qcount + ancount + nscount + adcount);
		sectionParse(Section::Question, packet, qcount);
		sectionParse(Section::Answer, packet, ancount);
		sectionParse(Section::Authority, packet, nscount);
		sectionParse(Section::Additional, packet, adcount);
		if (packet.pos() != packet.end())
			throw ParseError("Leftover data");
	} catch (const Blob::OutOfRange &) {
		throw ParseError("The buffer doesn't contain the whole message");
	}
}

// Parse a name, recursively, using a cache to parse the parts that are compressed only once.
PParsedName Message::nameParse(Blob &packet) {
	TRC;
	const uint8_t len = packet.get<uint8_t>(true);
	constexpr uint8_t ptrMark = (1 << 7) | (1 << 6); // First two bits set
	if ((len & ptrMark) == ptrMark) {
		// This is a pointer somewhere else. Get the rest of the pointer.
		/*
		 * The offset is relative to the start of the packet and we
		 * find the rest of the name at that address.
		 */
		const uint16_t offset = (static_cast<uint16_t>(len & ~ptrMark) << 8) | packet.get<uint8_t>(true);
		const auto cached = nameCache_.find(offset);
		if (cached == nameCache_.end()) {
			TRC;
			if (offset == packet.pos() - packet.begin() - 2)
				// Reference directly to self
				throw Message::ParseError("Cyclic name compression");
			// Not there yet. Unlikely, but possible (jump ahead or something that wasn't start of a label before)
			// We create a new blob and set it to the right place and parse there (we don't want to modify this one)
			Blob another(packet.begin(), packet.size());
			another.skip(offset);
			return nameParse(another);
		} else {
			TRC;
			// We already parsed that, good
			if (cached->second)
				return cached->second;
			else
				throw Message::ParseError("Cyclic name compression");
		}
	} else if (len) {
		// Not at the end yet ‒ non-zero length
		TRC;
		const uint16_t offset = packet.pos() - packet.begin() - 1;
		// Store NULL, in case there's a cycle
		nameCache_[offset] = PParsedName();
		ParsedName label;
		label.label_ = packet.slice(len);
		// Recurse for the rest
		label.next_ = nameParse(packet);
		// It's now complete, so store it in the cache
		PParsedName result(new ParsedName(label));
		nameCache_[offset] = result;
		return result;
	} else {
		TRC;
		return PParsedName();
	}
}

void Message::sectionParse(Section section, Blob &packet, size_t cnt) {
	for (size_t i = 0; i < cnt; i ++) {
		Record r;
		r.section_ = section;
		r.name_ = nameParse(packet);
		const uint16_t rtype(packet.get<uint16_t>(true));
		// TODO: Something nicer/more generic than this would be great
		switch (rtype) {
			case 1:
				r.type_ = Type::A;
				break;
			case 5:
				r.type_ = Type::CNAME;
				break;
			case 6:
				r.type_ = Type::SOA;
				break;
			case 12:
				r.type_ = Type::PTR;
				break;
			case 15:
				r.type_ = Type::MX;
				break;
			case 28:
				r.type_ = Type::AAAA;
				break;
			case 33:
				r.type_ = Type::SRV;
				break;
			default:
				r.type_ = Type::Other;
				break;
		}
		const uint16_t rclass(packet.get<uint16_t>(true));
		r.class_ = rclass < static_cast<uint16_t>(Class::Other) ? static_cast<Class>(rclass) : Class::Other;
		if (section != Section::Question) {
			r.ttl_ = packet.get<uint32_t>(true);
			const uint16_t rdataLen(packet.get<uint16_t>(true));
			r.data_ = packet.slice(rdataLen);
		} else
			r.ttl_ = 0;
		records_.push_back(r);
	}
}

PParsedName Message::nameParseAt(const uint8_t *position) {
	Blob copy(data.begin(), data.size());
	assert(position >= copy.begin());
	copy.skip(position - copy.begin());
	return nameParse(copy);
}

}
}
