/*
 * Copyright 2017, CZ.NIC z.s.p.o. (http://www.nic.cz/)
 *
 * This file is part of the pakon system.
 *
 * Pakon is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 * Pakon is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pakon.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../src/packet.hpp"
#define BOOST_TEST_MODULE packet
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>

using namespace Pakon;

namespace {

BOOST_AUTO_TEST_CASE(parseTCP) {
	// The packet to parse. We ignore the checksums here.
	const uint8_t packet_data[] = {
		// The IP deader:
		0x45, 0x00, 0x00, 0x2C /* IP 20 + TCP 20 + data 4 */,
		0x00, 0x00, 0x40, 0x00,
		0x40, 0x06, 0x00, 0x00,
		0xC0, 0x00, 0x02, 0x01,
		0xC0, 0x00, 0x02, 0x02,
		// The TCP header:
		0xDD, 0xD5, 0x00, 0x16,
		0xAA, 0xBB, 0xCC, 0xDD,
		0xDD, 0xCC, 0xBB, 0xAA,
		0x50, 0x17 /* ACK + RST + SYN + FIN */, 0xFF, 0xFF,
		0x00, 0x00, 0x00, 0x00,
		// The data:
		0x11, 0x22, 0x33, 0x44
	};
	// Parse the packet
	const Packet packet(packet_data, sizeof packet_data);
	/*
	 * Check the protocols.
	 * Note that during the testing parsing, we don't create the ethernet
	 * layer.
	 */
	const auto &protocols(packet.field<PKT_level>());
	BOOST_CHECK_EQUAL(2, protocols.size());
	BOOST_CHECK_EQUAL(ProtoLevel::IPv4, protocols[0].type);
	BOOST_CHECK_EQUAL("192.0.2.1", protocols[0].src.toString());
	BOOST_CHECK_EQUAL("192.0.2.2", protocols[0].dst.toString());
	BOOST_CHECK_EQUAL(ProtoLevel::TCP, protocols[1].type);
	// TCP properties
	// Source port
	BOOST_CHECK_EQUAL(56789, packet.field<PKT_ports>().first);
	// Destination port
	BOOST_CHECK_EQUAL(22, packet.field<PKT_ports>().second);
	// The flags
	const uint16_t flags(packet.field<PKT_tcp_flags>());
	BOOST_CHECK_EQUAL(TcpFlags::ACK | TcpFlags::RST | TcpFlags::SYN | TcpFlags::FIN, flags);
	// The data in the packet
	Blob payload(packet.field<PKT_payload>());
	BOOST_CHECK_EQUAL(4, payload.size());
	BOOST_CHECK_EQUAL(0x11223344, payload.get<uint32_t>(true));
}

// TODO: We want more tests, with IPv6, UDP, etc… there might be some subtle bugs there.

}
