/*
 * Copyright 2017, CZ.NIC z.s.p.o. (http://www.nic.cz/)
 *
 * This file is part of the pakon system.
 *
 * Pakon is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 * Pakon is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pakon.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../src/worker.hpp"
#include "../src/util.hpp"
#define BOOST_TEST_MODULE worker
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>
#include <vector>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

namespace {

using namespace Pakon;
using namespace std;

// Tests for the message queue
BOOST_AUTO_TEST_CASE(queue) {
	TRC; // To initialize logging before we run threads
	// Prepare the queue
	MsgQueue<int> q;
	// Prepare some data
	vector<int> data;
	const int iterations = inValgrind() ? 100 : 10000;
	for (int i = 0; i < iterations; i ++)
		data.push_back(i);
	/*
	 * As this is a thread synchronisation, it might as well contain some
	 * kind of race condition. So we run it multiple times.
	 */
	for (size_t i = 0; i < 100; i ++) {
		// Push bunch of data into it in one thread
		thread feeder([&q, &data] {
			for (auto i: data)
				q.push(i);
		});
		// Extract them on the other end and check we get the same as expected.
		MsgQueue<int>::Q received;
		for (auto i: data) {
			if (received.empty())
				received = q.pop_all();
			BOOST_REQUIRE(!received.empty());
			BOOST_CHECK_EQUAL(i, received.front());
			received.pop_front();
		}
		// Make sure that thread terminates.
		feeder.join();
	}
}

class TestSPE : public SPE {
public:
	int fd;
	size_t wakeup_count = 0;
	size_t periodic_count = 0;
	size_t zero_count = 0;
private:
	bool zero_task() {
		zero_count ++;
		uint8_t byte;
		ssize_t result = read(fd, &byte, 1);
		if (result == 1)
			BOOST_CHECK_EQUAL('!', byte);
		else if (result == 0) {
			wakeup();
			return false;
		} else {
			BOOST_FAIL("Read failed");
		}
		return true;
	}
	bool wakeup_task() {
		if (wakeup_count == 0) {
			int pipes[2];
			BOOST_REQUIRE_EQUAL(0, pipe2(pipes, O_NONBLOCK));
			fd = pipes[0];
			BOOST_CHECK_EQUAL(1, write(pipes[1], "!", 1));
			BOOST_CHECK_EQUAL(0, close(pipes[1]));
			BOOST_CHECK(insertTask(fd, bind(&TestSPE::zero_task, this)) != nullptr);
		} else {
			uint8_t byte;
			// It's already closed, so it should error.
			BOOST_CHECK_EQUAL(-1, read(fd, &byte, 1));
		}
		wakeup_count ++;
		return true;
	}
	void periodic_task() {
		periodic_count ++;
		delayedTerminate();
	}
public:
	TestSPE() :
		SPE(bind(&TestSPE::wakeup_task, this), bind(&TestSPE::periodic_task, this))
	{}
	void do_test() {
		// Get a file descriptor that is readable all the time
		wakeup();
		// Not called directly
		BOOST_CHECK_EQUAL(0, wakeup_count);
		run();
	}
};

/*
 * Some self-pipe event loop torture
 *
 * This test is a bit convoluted. We inherit the SPE in our own class (since
 * SPE is not usable directly, it's only a building block). We count the events
 * as they are fired.  We first wake up the loop. From that first event we add
 * a file descriptor. That one gets fired twice (once to read the one byte
 * available there, the second time to inform it is terminated). After that we
 * do another wakeup.  In that we check the file descriptor is already closed.
 * Finally, after 5 seconds, the periodic task is called and that one
 * terminates the loop.
 */
BOOST_AUTO_TEST_CASE(spe) {
	TestSPE spe;
	spe.do_test();
	BOOST_CHECK_EQUAL(2, spe.wakeup_count);
	BOOST_CHECK_EQUAL(1, spe.periodic_count);
	BOOST_CHECK_EQUAL(2, spe.zero_count);
}

// A simple test for the worker
BOOST_AUTO_TEST_CASE(worker) {
	Worker worker(42);
	BOOST_CHECK_EQUAL(42, worker.id());
	BOOST_CHECK(worker.alive());
	// The int there has no meaning, just a value
	MsgQueue<int> terminated;
	int pipes[2];
	BOOST_REQUIRE_EQUAL(0, pipe2(pipes, O_NONBLOCK));
	BOOST_CHECK_EQUAL(0, close(pipes[1]));
	worker.addTask(pipes[0], [&terminated, &worker] {
		// Stop, but don't wait for it.
		worker.stop(false);
		// Signal we terminated.
		terminated.push(0);
		// Drop the FD.
		return false;
	});
	// Wait for the worker to terminate
	terminated.pop_all(true);
	// Make sure it is completely stopped
	worker.stop(true);
	BOOST_CHECK(!worker.alive());
	// The FD got closed by now
	uint8_t byte;
	BOOST_CHECK_EQUAL(-1, read(pipes[0], &byte, 1));
}

}
