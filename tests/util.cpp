/*
 * Copyright 2017, CZ.NIC z.s.p.o. (http://www.nic.cz/)
 *
 * This file is part of the pakon system.
 *
 * Pakon is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 * Pakon is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pakon.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../src/util.hpp"
#define BOOST_TEST_MODULE util
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>
#include <set>
#include <vector>
#include <string>

namespace {

using namespace Pakon;
using namespace std;

size_t singletons_created = 0;

class S: public Singleton<S> {
private:
	friend class Singleton<S>;
	S() {
		singletons_created ++;
	}
};

// Make sure a singleton is really a singleton, that it is created on the first
// access and never again.
BOOST_AUTO_TEST_CASE(singleton) {
	BOOST_CHECK_EQUAL(0, singletons_created);
	const S *singleton = &S::instance();
	BOOST_CHECK_EQUAL(1, singletons_created);
	BOOST_CHECK_EQUAL(singleton, &S::instance());
	BOOST_CHECK_EQUAL(1, singletons_created);
}

// Check that exit guard is called and even when there's an exception.
BOOST_AUTO_TEST_CASE(exitGuard) {
	size_t call_count = 0;
	{
		ExitGuard guard([&call_count]{ call_count ++; });
		// Not yet
		BOOST_CHECK_EQUAL(0, call_count);
	}
	// But here the exit guard is no more, so it incremented the count
	BOOST_CHECK_EQUAL(1, call_count);
	try {
		ExitGuard guard([&call_count]{ call_count ++; });
		BOOST_CHECK_EQUAL(1, call_count);
		// We don't care what we throw…
		throw 0;
	} catch (...) {
		BOOST_CHECK_EQUAL(2, call_count);
	}
	// It doesn't get incremented if we abort the guard
	{
		ExitGuard guard([&call_count]{ call_count ++; });
		BOOST_CHECK_EQUAL(2, call_count);
		guard.abort();
	}
	BOOST_CHECK_EQUAL(2, call_count);
}

// The insertAll utility function
BOOST_AUTO_TEST_CASE(inserts) {
	vector<int> inputs;
	inputs.push_back(42);
	inputs.push_back(43);
	inputs.push_back(13);
	set<unsigned> output;
	insertAll(output, inputs);
	BOOST_CHECK_EQUAL(3, output.size());
	auto it = output.begin();
	BOOST_CHECK_EQUAL(13, *it ++);
	BOOST_CHECK_EQUAL(42, *it ++);
	BOOST_CHECK_EQUAL(43, *it ++);
}

// Test the joining of strings
BOOST_AUTO_TEST_CASE(joins) {
	// Start with an empty one
	vector<string> strings;
	BOOST_CHECK_EQUAL("", join(strings));
	BOOST_CHECK_EQUAL("", join(strings, ", "));
	BOOST_CHECK_EQUAL("@@", join(strings, "XX", "@@"));
	// Put some data inside it
	strings.push_back("hello");
	strings.push_back("world");
	BOOST_CHECK_EQUAL("helloworld", join(strings));
	BOOST_CHECK_EQUAL("hello world", join(strings, " "));
	BOOST_CHECK_EQUAL("hello world", join(strings, " ", "@@"));
}

}
