/*
 * Copyright 2016, CZ.NIC z.s.p.o. (http://www.nic.cz/)
 *
 * This file is part of the pakon system.
 *
 * Pakon is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 * Pakon is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pakon.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../src/types.hpp"
#include "../src/type_serialization.hpp"
#define BOOST_TEST_MODULE Types
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>

namespace {

using namespace Pakon;
using namespace std;

// Test creation of an empty blob
BOOST_AUTO_TEST_CASE(blobEmpty) {
	Blob b;
	BOOST_CHECK_EQUAL(0, b.size());
	BOOST_CHECK_EQUAL(b.begin(), b.end());
	BOOST_CHECK_EQUAL(string(), b.toString());
	BOOST_CHECK_THROW(b.skip(10), Blob::OutOfRange);
	BOOST_CHECK_THROW(b.get<int>(), Blob::OutOfRange);
	BOOST_CHECK_THROW(b.slice(10), Blob::OutOfRange);
}

string b2s(const Blob b) {
	return string(b.begin(), b.end());
}

// Test accessing data in the blob
BOOST_AUTO_TEST_CASE(blobOps) {
	const char *hw = "hello world";
	Blob b(hw, 11);
	// Really do check pointer equality
	BOOST_CHECK_EQUAL(hw, reinterpret_cast<const char *>(b.begin()));
	BOOST_CHECK_EQUAL(hw + 11, reinterpret_cast<const char *>(b.end()));
	BOOST_CHECK_EQUAL(11, b.size());
	BOOST_CHECK_EQUAL("hello world", b2s(b));
	BOOST_CHECK_EQUAL('h', b.get<char>());
	BOOST_CHECK_EQUAL('e', b.get<char>());
	b.skip(2);
	Blob b2(b.slice(2));
	BOOST_CHECK_EQUAL("o ", b2s(b2));
	BOOST_CHECK_EQUAL("6f 20 ", b2.toString());
	BOOST_CHECK_EQUAL("world", b2s(b.rest()));
	BOOST_CHECK_THROW(b.get<char>(), Blob::OutOfRange);
	b.reset();
	BOOST_CHECK_EQUAL('h', b.get<char>());
}

// Test accessing integers in network byte order
BOOST_AUTO_TEST_CASE(blobEndian) {
	uint8_t data[] = {0x00, 0x00, 0xff, 0x00, 0x00, 0x0a};
	Blob b(data, sizeof data);
	BOOST_CHECK_EQUAL(6, b.size());
	BOOST_CHECK_EQUAL(0xff00U, b.get<uint32_t>(true));
	BOOST_CHECK_EQUAL(10, b.get<uint16_t>(true));
}

// Test extracting fields of bits
BOOST_AUTO_TEST_CASE(bits) {
	uint32_t value = 0xff00ff00;
	BOOST_CHECK_EQUAL(0x0f, bitsExtract(value, 0, 4));
	BOOST_CHECK_EQUAL(0xf00, bitsExtract(value, 4, 12));
	BOOST_CHECK_EQUAL(0xff00, bitsExtract(value, 16, 16));
}

// Check the address works as it should.
void addrCheck(const string &input, const string &expected) {
	Blob binput(input.c_str(), input.size());
	Address addr(binput);
	BOOST_CHECK_EQUAL(input, addr.binString());
	BOOST_CHECK_EQUAL(expected, addr.toString());
	BOOST_CHECK_EQUAL(input.size(), addr.size());
	// This may also belong to the serialization test, but it is more convenient to put it here.
	BOOST_CHECK_EQUAL("\"" + expected + "\"", toJSON(addr));
}

BOOST_AUTO_TEST_CASE(address) {
	Address empty;
	BOOST_CHECK_EQUAL("", empty.toString());
	BOOST_CHECK_EQUAL("", empty.binString());
	BOOST_CHECK_EQUAL(0, empty.size());
	addrCheck(string("\x7f\0\0\x01", 4), "127.0.0.1");
	addrCheck(string("\0\x11 \xff\xaa\xbb", 6), "00:11:20:ff:aa:bb");
	addrCheck(string("\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\x01", 16), "0000:0000:0000:0000:0000:0000:0000:0001");
	addrCheck(string("\x20\x01\0\0\0\0\0\0\0\0\0\0\0\0\0\x01", 16), "2001:0000:0000:0000:0000:0000:0000:0001");
}

}
