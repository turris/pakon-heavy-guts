/*
 * Copyright 2016, CZ.NIC z.s.p.o. (http://www.nic.cz/)
 *
 * This file is part of the pakon system.
 *
 * Pakon is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 * Pakon is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pakon.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../src/serialize.hpp"
#include "../src/type_serialization.hpp"
#include "../src/introsp.hpp"
#define BOOST_TEST_MODULE Serializable
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>
#include <boost/test/output_test_stream.hpp>
#include <boost/test/test_case_template.hpp>
#include <boost/mpl/list.hpp>
#include <set>
#include <map>

/*
 * Tests for JSON serialization. This tests several files from src/:
 * • serialize
 * • type_serialization
 */

namespace {

using namespace Pakon;
using namespace Pakon::Introspectable;
using namespace std;
using namespace boost::test_tools;

// Check expected output of the given type
template<class T> void jst(const string &expected, const T &value) {
	BOOST_CHECK_EQUAL(expected, toJSON(value));
	output_test_stream output;
	jsorialize(output, value);
	BOOST_CHECK(output.is_equal(expected));
}
// The same as above, but add quotes first, for convenience
template<class T> void jstq(const string &expected, const T &value) {
	jst("\"" + expected + "\"", value);
}

// Some basic data types.
BOOST_AUTO_TEST_CASE(basic) {
	jst("42", 42);
	jst("42", 42U);
	jst("42.5", 42.5f);
	jstq("hello", "hello");
	jstq<string>("hello", "hello");
	jstq("hello\\u005cworld", "hello\\world");
	jst("true", true);
	jst("false", false);
	jst<int *>("null", nullptr);
	int x = 42;
	jst<int *>("42", &x);
}

// Some types we provide conversion for ourselves.
class Dummy {};

}

// We need to do the convertor in the correct namespace
namespace Pakon {

template<> class JSORiazible<Dummy> {
public:
	template<class Stream> static void jsorialize(Stream &stream, const Dummy &) {
		stream << "{\"dummy\": null}";
	}
};

}

namespace {

BOOST_AUTO_TEST_CASE(providedConversion) {
	jst("{\"dummy\": null}", Dummy());
}

// Some tests for types that are external to serialize.hpp, but provided by src/ libraries
BOOST_AUTO_TEST_CASE(providedLib) {
	jstq("IN", Direction::IN);
	jstq("OUT", Direction::OUT);
	jstq("IPv4", Family::IPv4);
	jstq("IPv6", Family::IPv6);
	jstq("TCP", IpProto::TCP);
	jstq("UDP", IpProto::UDP);
	jstq("?", IpProto::Other);
}

// Struct serialization (even recursive one)
#define N(Name) struct Name { static const char *name() { return #Name; } };
N(A)
N(B)
N(C)
#undef N
typedef Struct<Field<int, Multiple<Dribble>, A>, Field<int, Multiple<BatchNonempty>, B>, Field<bool, Simple, C>> Inner;
typedef Struct<Field<Inner, Simple, A>, Field<string, InlineMap, B>> Outer;

BOOST_AUTO_TEST_CASE(introspStruct) {
	Outer o;
	o.add<B>("x1", "x1");
	o.field<A>().store<A>(42);
	o.field<A>().store<A>(43);
	o.field<A>().store<B>(44);
	o.field<A>().store<B>(45);
	o.field<A>().store<C>(true);
	jst("{\"A\":{\"A\":42,\"A\":43,\"B\":[44,45],\"C\":true},\"x1\":\"x1\"}", o);
}

// Some compound types (avoid unordered ones, as we wouldn't guess the result correctly)
typedef boost::mpl::list<set<int>, multiset<int>, list<int>, vector<int>> Containers;
// As the set-style and sequence-style containers insert in a different way, we provide adaptor for that
template<class C> typename enable_if<IsSequence<C>::value>::type insert(C &c, int value) {
	c.push_back(value);
}
template<class C> typename enable_if<IsSet<C>::value>::type insert(C &c, int value) {
	c.insert(value);
}
BOOST_AUTO_TEST_CASE_TEMPLATE(containers, Container, Containers) {
	Container c;
	jst("[]", c);
	insert(c, 42);
	insert(c, 43);
	insert(c, 44);
	jst("[42,43,44]", c);
}

typedef boost::mpl::list<map<string, int>, multimap<string, int>> Maps;
BOOST_AUTO_TEST_CASE_TEMPLATE(maps, Map, Maps) {
	Map m;
	jst("{}", m);
	m.insert(make_pair(string("hello"), 42));
	m.insert(make_pair(string("world"), 43));
	jst("{\"hello\":42,\"world\":43}", m);
}

}
