/*
 * Copyright 2016, CZ.NIC z.s.p.o. (http://www.nic.cz/)
 *
 * This file is part of the pakon system.
 *
 * Pakon is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 * Pakon is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pakon.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../src/interpreter.hpp"
#include "../src/lua_convert.hpp"
#define BOOST_TEST_MODULE IntrospectableStruct
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>
#include <set>
#include <map>
#include <unordered_set>

namespace {

using namespace Pakon::Lua;
using namespace Pakon;
using namespace std;

// A dummy test type
struct Wrap {
	string content;
	bool operator ==(const Wrap &other) const { return content == other.content; }
};

}

namespace Pakon {
namespace Lua {

// Manipulators for our own test type
template<> class Pushable<Wrap> {
public:
	static void push(lua_State *state, const Wrap &wrap) {
		Pakon::Lua::push(state, wrap.content);
	}
};
template<> class Extractable<Wrap> {
public:
	static void extract(lua_State *state, int index, Wrap &wrap) {
		Pakon::Lua::extract(state, index, wrap.content);
	}
};

}
}

namespace {
/*
 * Do a push-extract pair and compare the same thing got out
 *
 * In addition, this also checks the StackGuard and the Value
 * manipulation.
 */
template<class Tin, class Tout = Tin> void pe(const Interpreter &interpreter, const Tin &value) {
	lua_State *state = interpreter.getState();
	Value v;
	int stackSize = lua_gettop(state);
	{
		// First try to store and extract
		StackGuard sg(state);
		push(state, value);
		Tout extracted;
		extract(state, -1, extracted);
		BOOST_CHECK(value == extracted); // Not BOOST_CHECK_EQUAL, as some of the types aren't printable
		BOOST_CHECK_EQUAL(stackSize + 1, lua_gettop(state));
		// Extract once again into the Value holder
		extract(state, -1, v);
		BOOST_CHECK_EQUAL(stackSize + 1, lua_gettop(state));
	} // Get rid of the value on the stack
	// Check the holder holds the right thing
	BOOST_CHECK_EQUAL(stackSize, lua_gettop(state));
	BOOST_CHECK(value == v.extract<Tout>());
	BOOST_CHECK_EQUAL(stackSize, lua_gettop(state));
	// Try one more push-extract with the holder as the source
	StackGuard sg(state);
	push(state, v);
	Tout extracted;
	extract(state, -1, extracted);
	BOOST_CHECK(value == extracted);
}
BOOST_AUTO_TEST_CASE(pushExtract) {
	Interpreter interpreter;
	// Some basic types
	pe(interpreter, true);
	pe(interpreter, false);
	pe<const char *, string>(interpreter, "hello");
	pe(interpreter, string("world"));
	pe(interpreter, 42U);
	pe(interpreter, 42);
	pe(interpreter, 42.5);
	// Some containers
	vector<string> strings;
	strings.push_back("hello");
	strings.push_back("world");
	pe(interpreter, strings);
	pe(interpreter, list<string>(strings.begin(), strings.end()));
	pe(interpreter, set<string>(strings.begin(), strings.end()));
	pe(interpreter, unordered_set<string>(strings.begin(), strings.end()));
	map<string, int> m;
	m["hello"] = 42;
	m["world"] = 43;
	pe(interpreter, m);
	map<int, vector<set<string>>> complexType;
	complexType[42].push_back(set<string>(strings.begin(), strings.end()));
	pe(interpreter, complexType);
	// Our own dummy data type
	Wrap w;
	w.content = "hello world";
	pe(interpreter, w);
}

// Test the behavior of extract under different type mismatch conditions
BOOST_AUTO_TEST_CASE(typeMismatch) {
	Interpreter interpreter;
	lua_State *state = interpreter.getState();
	push(state, true);
	string s;
	int i;
	BOOST_CHECK_THROW(extract(state, -1, s), Mismatch);
	BOOST_CHECK_THROW(extract(state, -1, i), Mismatch);
	// If we push string, it can be converted to boolean and to int if it number
	push(state, "42");
	extract(state, -1, i);
	BOOST_CHECK_EQUAL(42, i);
	bool b;
	extract(state, -1, b);
	BOOST_CHECK(b);
	// But if we put something that is not number, it can't be converted (but it still can be a nice boolean)
	push(state, "hello");
	BOOST_CHECK_THROW(extract(state, -1, i), Mismatch);
	extract(state, -1, b);
	BOOST_CHECK(b);
	// Get an invalid index ‒ it should contain nil
	int idx = lua_gettop(state) + 2;
	BOOST_CHECK_THROW(extract(state, idx, s), Mismatch);
	BOOST_CHECK_THROW(extract(state, idx, i), Mismatch);
	// Even nil is nice boolean
	extract(state, idx, b);
	BOOST_CHECK(!b);
	// Push a table there and check again
	push(state, vector<int>());
	BOOST_CHECK_THROW(extract(state, idx, s), Mismatch);
	BOOST_CHECK_THROW(extract(state, idx, i), Mismatch);
	extract(state, -1, b);
	BOOST_CHECK(b);
}

// Call a C++ function/functor from lua (well, almost, we don't do the actual calling, just the decoding of parameteres)
BOOST_AUTO_TEST_CASE(callC) {
	Interpreter interpreter;
	lua_State *state = interpreter.getState();
	list<string> in;
	in.push_back("hello");
	in.push_back("world");
	// Store the parameters there
	push(state, true, "42", 12, "hello", in);
	bool called = false;
	// Call the lambda function. Note that we have different types at places and do auto-conversion.
	BOOST_CHECK_EQUAL(cFromLua(state, [&called](bool b, int i, const string &s, const string &s2, const vector<string> &v) -> tuple<int, string, bool> {
		called = true;
		BOOST_CHECK(b);
		BOOST_CHECK_EQUAL(i, 42);
		BOOST_CHECK_EQUAL(s, "12");
		BOOST_CHECK_EQUAL(s2, "hello");
		BOOST_CHECK_EQUAL(v.size(), 2);
		BOOST_CHECK_EQUAL(v[0], "hello");
		BOOST_CHECK_EQUAL(v[1], "world");
		return make_tuple(43, "world", false);
	}), 3);
	BOOST_CHECK(called);
	// Extract the result
	int i;
	string s;
	bool b;
	extract(state, -3, i, s, b);
	BOOST_CHECK_EQUAL(i, 43);
	BOOST_CHECK_EQUAL(s, "world");
	BOOST_CHECK(!b);
}

// A helper function for a test ‒ push parameters back onto stack in reverse order
int reverse(lua_State *state) {
	int max = lua_gettop(state);
	for (int i = 0; i < max; i ++)
		lua_pushvalue(state, max - i);
	return max;
}
// Call a „lua“ function from C++. We fake the lua function by a C++ one plugged in, but this is about the parameter conversion.
BOOST_AUTO_TEST_CASE(callLua) {
	Interpreter interpreter;
	lua_State *state = interpreter.getState();
	// Prepare the function value
	lua_pushcfunction(state, reverse);
	Value v(state, -1);
	// Get rid of the function from the stack, to make sure it is kept inside the Value object
	lua_pop(state, 1);
	// Try calling it as AnyFunction few times
	AnyFunction af(v);
	const auto &result1 = af.call<int, int>(42);
	BOOST_CHECK_EQUAL(result1, 42);
	const auto &result2 = af.call<tuple<string, string, int>, int, int, string>(42, 43, "world");
	BOOST_CHECK(result2 == make_tuple("world", "43", 42));
	// Now as a specialized type
	Function<tuple<bool, string>, int, int> f1(af);
	BOOST_CHECK(f1(42, 43) == make_tuple(true, "42"));
	Function<Value, string> f2(af);
	const auto &result3 = f2("42");
	BOOST_CHECK_EQUAL(result3.extract<string>(), "42");
	BOOST_CHECK_EQUAL(result3.extract<unsigned>(), 42);
	BOOST_CHECK(result3.extract<bool>());
	// Some more complex type
	Function<vector<int>, const list<string> &> f3(af);
	list<string> in;
	in.push_back("42");
	in.push_back("43");
	const auto &out = f3(in);
	BOOST_CHECK_EQUAL(out.size(), 2);
	// The order is not reversed here, as this is one parameter, not multiple
	BOOST_CHECK_EQUAL(out[0], 42);
	BOOST_CHECK_EQUAL(out[1], 43);
	// Provoke a wrong type
	Function<set<int>, string> f4(af);
	BOOST_CHECK_THROW(f4("hello"), Mismatch);
}

}
