/*
 * Copyright 2016, CZ.NIC z.s.p.o. (http://www.nic.cz/)
 *
 * This file is part of the pakon system.
 *
 * Pakon is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 * Pakon is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pakon.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../src/cache.hpp"
#define BOOST_TEST_MODULE cache
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>
#include <set>

namespace {

using namespace Pakon;
using namespace std;

struct CacheValue {
	size_t i;
	atomic<size_t> accessCount = {0};
	string key;
	thread::id creator;
	bool operator ==(const CacheValue &other) const {
		return i == other.i &&
			accessCount == other.accessCount &&
			key == other.key &&
			creator == other.creator;
	}
	CacheValue(size_t i, const string &key) :
		i(i),
		key(key),
		creator(this_thread::get_id())
	{}
	CacheValue(const CacheValue &other) :
		i(other.i),
		key(other.key),
		creator(other.creator)
	{
		accessCount.store(other.accessCount);
	}
};

/*
 * As BOOST_CHECK_* things aren't threat safe (tried, crashed), we accumulate the
 * errors here and provide them at the end of the test, in a sigle thread.
 */
class Checker {
private:
	mutex m;
	vector<string> errors;
public:
	void check(size_t line, const char *text, bool check) {
		if (!check) {
			lock_guard<mutex> g(m);
			errors.push_back(to_string(line) + ":" + text);
		}
	}
	template<class T1, class T2> void checkEq(size_t line, const T1 &v1, const T2 &v2) {
		if (v1 != v2) {
			ostringstream os;
			os << line << ":" << line << v1 << "!=" << v2;
			lock_guard<mutex> g(m);
			errors.push_back(os.str());
		}
	}
	~ Checker() {
		for (const auto &e: errors)
			BOOST_ERROR(e);
	}
};
#define C(CHECK) do { c.check(__LINE__, #CHECK, (CHECK)); } while (0)
#define CE(V1, V2) do { c.checkEq(__LINE__, (V1), (V2)); } while (0)

template<class Cache> string create(Cache &cache, Checker &c, size_t i, size_t j, bool mayDelete = false) {
	const string key = to_string(i) + "|" + to_string(j);
	const CacheValue v(j, key);
	C(!cache.find(key));
	const auto result = cache.access(key, v);
	C(get<0>(result));
	C(*get<0>(result) == v);
	C(get<1>(result));
	if (!mayDelete)
		C(!get<2>(result));
	C(get<0>(result) == cache.find(key));
	return key;
}

template<class Cache> void deletedCheck(Cache &cache, size_t threadCount, size_t itemCount, bool checkSize = true) {
	const auto &deleted = cache.deleted();
	if (checkSize)
		BOOST_CHECK_EQUAL(deleted.size(), threadCount * itemCount);
	set<string> keys;
	for (const auto v: deleted)
		keys.insert(v->key);
	for (size_t i = 0; i < threadCount; i ++)
		for (size_t j = 0; j < itemCount; j ++)
			BOOST_CHECK(keys.find(to_string(i) + "|" + to_string(j)) != keys.end());
}

void wait(list<thread> &threads) {
	for (auto &t: threads)
		t.join();
	threads.clear();
}

/*
 * Access the cache from multiple threads at once and see it doesn't break.
 * In this test we have enough space for all items to fit (but only just).
 */
template<bool keepDeleted, bool sizeLimit> void concurrentTest() {
	TRC; // To initialize logging before we run threads
	const bool inVal = inValgrind();
	const size_t threadCount = inVal ? 2 : 10;
	const size_t itemCount = inVal ? 1000 : 100000;
	Cache<string, CacheValue, 5, keepDeleted, sizeLimit, true, uint64_t> cache(threadCount * itemCount);
	// Fill the cache in parallel
	list<thread> threads;
	Checker c;
	for (size_t i = 0; i < threadCount; i ++)
		threads.emplace_back([itemCount, &cache, i, &c]() {
			for (size_t j = 0; j < itemCount; j ++) {
				const string &key = create(cache, c, i, j);
				cache.ttl(key, j);
			}
		});
	wait(threads);
	BOOST_CHECK_EQUAL(cache.size(), threadCount * itemCount);
	for (size_t i = 0; i < threadCount * 2; i ++)
		threads.emplace_back([itemCount, &cache, i, &c]() {
			for (size_t j = 0; j < itemCount; j ++) {
				const string key = to_string(i / 2) + "|" + to_string(j);
				auto result = cache.find(key);
				C(result);
				CE(result->i, j);
				CE(result->key, key);
				result->accessCount ++;
			}
		});
	wait(threads);
	// Check access counts at all the items
	for (size_t i = 0; i < threadCount; i ++)
		for (size_t j = 0; j < itemCount; j ++)
			BOOST_CHECK_EQUAL(cache.find(to_string(i) + "|" + to_string(j))->accessCount, 2);
	BOOST_CHECK(cache.deleted().empty());
	// Launch some more threads accessing the data while we delete old TTLs
	for (size_t i = 0; i < threadCount * 2; i ++)
		threads.emplace_back([itemCount, &cache, i, &c]() {
			for (size_t j = 0; j < itemCount; j ++) {
				const string key = to_string(i / 2) + "|" + to_string(j);
				const auto &result = cache.find(key);
				if (j >= itemCount / 2) {
					C(result);
					CE(result->i, j);
					CE(result->key, key);
				}
			}
		});
	// Do the deleting (half of them) while the accesses run
	cache.cleanupTTL(itemCount / 2);
	// Wait for the access threads
	wait(threads);
	// Now we should have exactly half of the items still alive
	BOOST_CHECK_EQUAL(cache.size(), threadCount * itemCount / 2);
	// Check we have the right items in the cache
	for (size_t i = 0; i < threadCount; i ++)
		for (size_t j = itemCount / 2; j < itemCount; j ++)
			BOOST_CHECK(cache.find(to_string(i) + "|" + to_string(j)));
	if (keepDeleted)
		// The other half is deleted, check what is there
		deletedCheck(cache, threadCount, itemCount / 2);
}

BOOST_AUTO_TEST_CASE(concurrent) {
	concurrentTest<true, true>();
}

BOOST_AUTO_TEST_CASE(concurrentNoKeep) {
	concurrentTest<false, true>();
}

BOOST_AUTO_TEST_CASE(concurrentNoLimit) {
	concurrentTest<true, false>();
}

// Not all things fit. Try stuffing them there and see that the correct ones fall out.
BOOST_AUTO_TEST_CASE(lru) {
	TRC; // To initialize logging before we run threads
	const bool inVal = inValgrind();
	const size_t threadCount = inVal ? 2 : 10;
	const size_t itemCount = inVal ? 1000 : 100000;
	const size_t deleteBatch = 50;
	Cache<string, CacheValue, 5, true, true, true> cache(threadCount * itemCount, deleteBatch);
	Checker c;
	list<thread> threads;
	// First create bunch of items that fit
	for (size_t i = 0; i < threadCount; i ++)
		threads.emplace_back([itemCount, i, &cache, &c]() {
			for (size_t j = 0; j < itemCount; j ++)
				create(cache, c, i, j);
		});
	wait(threads);
	BOOST_CHECK_EQUAL(cache.size(), threadCount * itemCount);
	BOOST_CHECK(cache.deleted().empty());
	// Then create another bunch which will push the first bunch out
	for (size_t i = 0; i < threadCount; i ++)
		threads.emplace_back([itemCount, i, &cache, &c]() {
			for (size_t j = 0; j < itemCount; j ++)
				create(cache, c, i, j + itemCount, true);
		});
	wait(threads);
	BOOST_CHECK(cache.size() <= threadCount * itemCount && cache.size() > threadCount * itemCount - deleteBatch);
	deletedCheck(cache, threadCount, itemCount, false);
}

}
