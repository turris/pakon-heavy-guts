/*
 * Copyright 2016, CZ.NIC z.s.p.o. (http://www.nic.cz/)
 *
 * This file is part of the pakon system.
 *
 * Pakon is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 * Pakon is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pakon.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../src/introsp.hpp"
#include "../src/util.hpp"
#define BOOST_TEST_MODULE IntrospectableStruct
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>
#include <boost/test/test_case_template.hpp>
#include <boost/mpl/list.hpp>
#include <numeric>

namespace {

class IntField;
class StringField;
class StringField2;

using namespace Pakon::Introspectable;
using namespace Pakon;
using namespace std;

// Test basic manipulation of the simple fields
BOOST_AUTO_TEST_CASE(simple) {
	Struct<Field<int, Simple, IntField>, Field<string, Simple, StringField>, Field<string, Simple, StringField2>> s;
	// Default values for the types that make sense (int has no default)
	BOOST_CHECK_EQUAL(s.field<StringField>(), "");
	BOOST_CHECK_EQUAL(s.field<StringField2>(), "");
	BOOST_CHECK(s.present<IntField>());
	BOOST_CHECK(s.present<StringField>());
	BOOST_CHECK(s.present<StringField2>());
	// We can set value, both by assignment and by store
	s.store<StringField>("hello");
	s.field<StringField2>() = "world";
	s.store<IntField>(42);
	BOOST_CHECK_EQUAL(s.field<StringField>(), "hello");
	BOOST_CHECK_EQUAL(s.field<StringField2>(), "world");
	BOOST_CHECK_EQUAL(s.field<IntField>(), 42);
}

// Test basic manipulation of the optional fields
BOOST_AUTO_TEST_CASE(optional) {
	Struct<Field<int, Optional<Implicit>, IntField>, Field<string, Optional<Implicit>, StringField>, Field<string, Optional<Explicit>, StringField2>> s;
	// Nothing is present by default
	BOOST_CHECK(!s.present<IntField>());
	BOOST_CHECK(!s.present<StringField>());
	BOOST_CHECK(!s.present<StringField2>());
	// Access something by fields, which should create the implicit ones
	s.field<IntField>() = 42;
	BOOST_CHECK_EQUAL(s.field<IntField>(), 42);
	BOOST_CHECK_EQUAL(s.field<StringField>(), "");
	// They appear in the structure
	BOOST_CHECK(s.present<IntField>());
	BOOST_CHECK(s.present<StringField>());
	BOOST_CHECK(!s.present<StringField2>());
	// But accessing the explicit one this way throws
	BOOST_CHECK_THROW(s.field<StringField2>(), MissingField);
	// Which still doesn't create it
	BOOST_CHECK(!s.present<StringField2>());
	// We can however store it
	s.store<StringField2>("hello");
	BOOST_CHECK(s.present<StringField2>());
	BOOST_CHECK_EQUAL(s.field<StringField2>(), "hello");
	// And when we clear it, it disappears again
	s.clear<StringField2>();
	BOOST_CHECK(!s.present<StringField2>());
	// If we store and clear an implicit one, we get the default again
	s.store<StringField>("world");
	s.clear<StringField>();
	BOOST_CHECK_EQUAL(s.field<StringField>(), "");
}

/*
 * Because boost test wants to access the type and isn't happy with just
 * a forward declaration, we need to wrap them into real types here.
 */
template<class V> struct Wrap {
	typedef V Type;
};
typedef boost::mpl::list<Wrap<BatchNonempty>, Wrap<BatchEmpty>, Wrap<Dribble>> Iterations;
// Basic access methods for the multiple field. As the iteration doesn't matter here, the tests for all of them are the same.
BOOST_AUTO_TEST_CASE_TEMPLATE(multiple, Iteration, Iterations) {
	Struct<Field<string, Multiple<typename Iteration::Type>, StringField>> s;
	BOOST_CHECK(!s.template present<StringField>());
	BOOST_CHECK_EQUAL(s.template field<StringField>().size(), 0);
	s.template store<StringField>("Hello");
	s.template store<StringField>("World", 4); // Use just the first 4 letters (test multi-param constructor)
	s.template field<StringField>().push_back("XYZ");
	BOOST_CHECK(s.template present<StringField>());
	const auto &v = s.template field<StringField>();
	BOOST_CHECK_EQUAL(v.size(), 3);
	BOOST_CHECK_EQUAL(v[0], "Hello");
	BOOST_CHECK_EQUAL(v[1], "Worl");
	BOOST_CHECK_EQUAL(v[2], "XYZ");
	s.template clear<StringField>();
	BOOST_CHECK(!s.template present<StringField>());
	BOOST_CHECK_EQUAL(s.template field<StringField>().size(), 0);
}

// Manipulation with the inline map
BOOST_AUTO_TEST_CASE(inlineMap) {
	Struct<Field<string, InlineMap, StringField>> s;
	// Empty at first
	BOOST_CHECK(!s.present<StringField>());
	BOOST_CHECK(!s.present<StringField>("name"));
	BOOST_CHECK_THROW(s.field<StringField>("name"), MissingField);
	BOOST_CHECK_EQUAL(s.field<StringField>().size(), 0);
	// Store one item
	s.add<StringField>("name", "hello");
	BOOST_CHECK(s.present<StringField>());
	BOOST_CHECK(s.present<StringField>("name"));
	BOOST_CHECK(!s.present<StringField>("name2"));
	BOOST_CHECK_EQUAL(s.field<StringField>("name"), "hello");
	// Overwrite the field
	s.add<StringField>("name", "world");
	BOOST_CHECK_EQUAL(s.field<StringField>("name"), "world");
	// And back, in other way
	s.field<StringField>("name") = "hello";
	BOOST_CHECK_EQUAL(s.field<StringField>("name"), "hello");
	// Store a copy of the whole map
	auto map = s.field<StringField>();
	BOOST_CHECK_EQUAL(map.size(), 1);
	// Make it empty
	s.clear<StringField>();
	BOOST_CHECK(!s.present<StringField>());
	// And return the data back
	s.store<StringField>(map);
	BOOST_CHECK(s.present<StringField>());
}

// The call that converts the data into an output string, for easier check
class Call {
private:
	string &output;
	const string &stringify(const string &str) const {
		return str;
	}
	string stringify(const vector<string> &strs) const {
		return "{" + join(strs, ",") + "}";
	}
public:
	Call(string &output) :
		output(output)
	{}
	template<class Type, class Name, class Field> void step(const string &name, const Type &value) const {
		output += name + ":" + stringify(value) + "\n";
	}
	template<class Type, class Name, class Field> void step(const Type &value) const {
		step<Type, Name, Field>(string(Name::name()), value);
	}
};
// Test the iterate method that iterates trough the whole structure
BOOST_AUTO_TEST_CASE(iterate) {
	string output;
	// Bunch of names
#define N(n) struct n { static const char *name() { return #n; } };
	N(s1);
	N(s2); // An optional field that will be there
	N(missing); // An optional field that won't be there
	N(vempt); // Empty multi-value that should be present
	N(vnonempt); // Empty multi-value that should not be present
	N(vdata); // Non-empty multi-value
	N(vdrib); // Multi-value that shall be called on per-item basis
	N(m); // An inline map
#undef N
	Struct<
		Field<string, Simple, s1>,
		Field<string, Optional<Implicit>, s2>,
		Field<string, Optional<Implicit>, missing>,
		Field<string, Multiple<BatchEmpty>, vempt>,
		Field<string, Multiple<BatchNonempty>, vnonempt>,
		Field<string, Multiple<BatchNonempty>, vdata>,
		Field<string, Multiple<Dribble>, vdrib>,
		Field<string, InlineMap, m>
	> s;
	// Provide some data (and leave some empty)
#define S(value) s.store<value>(#value);
	S(s1);
	S(s2);
	S(vdata);
	S(vdata);
	S(vdrib);
	S(vdrib);
#undef S
	s.add<m>("a", "b");
	// Just make a list of what is inside by using the iteration
	Call call(output);
	s.iterate(call);
	BOOST_CHECK_EQUAL(output,
		"s1:s1\n"
		"s2:s2\n"
		"vempt:{}\n"
		"vdata:{vdata,vdata}\n"
		"vdrib:vdrib\n"
		"vdrib:vdrib\n"
		"a:b\n");
}

}
