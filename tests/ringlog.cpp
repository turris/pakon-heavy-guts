/*
 * Copyright 2017, CZ.NIC z.s.p.o. (http://www.nic.cz/)
 *
 * This file is part of the pakon system.
 *
 * Pakon is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 * Pakon is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pakon.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../src/ringlog.hpp"
#include "../src/util.hpp"
#define BOOST_TEST_MODULE ringlog
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>
#include <string>
#include <list>
#include <thread>

namespace {

using namespace Pakon;
using namespace std;

// Log two messages and check the same thing gets out
BOOST_AUTO_TEST_CASE(simple) {
	ringlog("msg1");
	ringlog("msg2");
	BOOST_CHECK_EQUAL("msg1\nmsg2\n", dumpRingMem());
	// It is empty the second time
	BOOST_CHECK_EQUAL("", dumpRingMem());
}

/*
 * Log many messages, so the buffer fills up.
 *
 * We would like to wrap the generation around as well. But that takes too long
 * to happen and is not practical as a unit test.
 */
BOOST_AUTO_TEST_CASE(wrap) {
	// This is not publicly visible, we just use the same size here
	const size_t ringbufLen = 4096;
	const size_t batch = 0xffff;
	string expected;
	for (size_t i = batch - ringbufLen; i < batch; i ++)
		expected += "msg" + to_string(i) + "\n";
	for (size_t i = 0; i < batch; i ++)
		ringlog("msg" + to_string(i));
	BOOST_CHECK_EQUAL(expected, dumpRingMem());
}

// Log concurrently and check it still acts sane (doesn't crash, doesn't lose
// messages, and doesn't mix them up in any way).
BOOST_AUTO_TEST_CASE(concurrent) {
	TRC; // To initialize logging before we run threads
	const size_t thread_count = 16;
	const size_t msg_count = 128;
	list<thread> threads;
	for (size_t i = 0; i < thread_count; i ++)
		threads.emplace_back([] {
			for (size_t j = 0; j < msg_count; j ++)
				ringlog("msg");
		});
	string expected;
	for (size_t i = 0; i < thread_count * msg_count; i ++)
		expected += "msg\n";
	for (auto &t: threads)
		t.join();
	threads.clear();
	BOOST_CHECK_EQUAL(expected, dumpRingMem());
}

}
