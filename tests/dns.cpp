/*
 * Copyright 2017, CZ.NIC z.s.p.o. (http://www.nic.cz/)
 *
 * This file is part of the pakon system.
 *
 * Pakon is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 * Pakon is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pakon.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../src/dns.hpp"
#define BOOST_TEST_MODULE DNS
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>
#include <unordered_set>

namespace {

using namespace Pakon;
using namespace Pakon::DNS;
using namespace std;

// A captured DNS packet: query for turris.cz
const uint8_t turris_cz_query[] = {
	0x6b, 0xab, 0x01, 0x00,
	0x00, 0x01, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00,
	0x06, 0x74, 0x75, 0x72,
	0x72, 0x69, 0x73, 0x02,
	0x63, 0x7a, 0x00, 0x00,
	0x01, 0x00, 0x01
};

// A captured DNS packet: reply for turris.cz
const uint8_t turris_cz_reply[] = {
	0x6b, 0xab, 0x81, 0x80,
	0x00, 0x01, 0x00, 0x01,
	0x00, 0x03, 0x00, 0x00,
	0x06, 0x74, 0x75, 0x72,
	0x72, 0x69, 0x73, 0x02,
	0x63, 0x7a, 0x00, 0x00,
	0x01, 0x00, 0x01, 0xc0,
	0x0c, 0x00, 0x01, 0x00,
	0x01, 0x00, 0x00, 0x07,
	0x07, 0x00, 0x04, 0xd9,
	0x1f, 0xc0, 0x69, 0xc0,
	0x0c, 0x00, 0x02, 0x00,
	0x01, 0x00, 0x00, 0x07,
	0x07, 0x00, 0x0b, 0x01,
	0x61, 0x02, 0x6e, 0x73,
	0x03, 0x6e, 0x69, 0x63,
	0xc0, 0x13, 0xc0, 0x0c,
	0x00, 0x02, 0x00, 0x01,
	0x00, 0x00, 0x07, 0x07,
	0x00, 0x04, 0x01, 0x62,
	0xc0, 0x39, 0xc0, 0x0c,
	0x00, 0x02, 0x00, 0x01,
	0x00, 0x00, 0x07, 0x07,
	0x00, 0x04, 0x01, 0x64,
	0xc0, 0x39
};

// A (broken) packet with a name pointer somewhere far outside of it
const uint8_t out_of_bounds_query[] = {
	0x6b, 0xab, 0x01, 0x00,
	0x00, 0x01, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00,
	0xff, 0xff
};

// A (broken) packet with a label pointing to itself
const uint8_t cyclic_query[] = {
	0x6b, 0xab, 0x01, 0x00,
	0x00, 0x01, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00,
	0xc0, 0x0c, 0x00, 0x01,
	0x00, 0x01
};

// A captured DNS packet: query for turris.cz
const uint8_t longer_cyclic_query[] = {
	0x6b, 0xab, 0x01, 0x00,
	0x00, 0x01, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00,
	0x06, 0x74, 0x75, 0x72,
	0x72, 0x69, 0x73, 0xc0,
	0x0c, 0x00, 0x01, 0x00,
	0x01
};

void queryCheck(const Record &r) {
	BOOST_CHECK_EQUAL("turris.cz", r.name()->toString());
	BOOST_CHECK(Section::Question == r.section());
	BOOST_CHECK(Class::IN == r.rclass());
	BOOST_CHECK(Type::A == r.rtype());
	// Other items of r are not relevant in the Question section
}

void answerCheck(const Record &r) {
	BOOST_CHECK_EQUAL("turris.cz", r.name()->toString());
	BOOST_CHECK(Section::Answer == r.section());
	BOOST_CHECK(Class::IN == r.rclass());
	BOOST_CHECK(Type::A == r.rtype());
	BOOST_CHECK_EQUAL(1799, r.ttl());
	BOOST_CHECK_EQUAL("217.31.192.105", Address(r.data()).toString());
}

void nsCheck(const Record &r, Message &msg, const string &prefix) {
	BOOST_CHECK_EQUAL("turris.cz", r.name()->toString());
	BOOST_CHECK(Section::Authority == r.section());
	BOOST_CHECK(Class::IN == r.rclass());
	// It's NS, but we are not interested in that in the program, so we don't have a separate type.
	BOOST_CHECK(Type::Other == r.rtype());
	BOOST_CHECK_EQUAL(1799, r.ttl());
	BOOST_CHECK_EQUAL(prefix + ".ns.nic.cz", msg.nameParseAt(r.data().begin())->toString());
}

// Parse the query
BOOST_AUTO_TEST_CASE(parseQuery) {
	const Blob packet(turris_cz_query, sizeof turris_cz_query);
	const Message msg(packet);
	BOOST_CHECK(QR::Query == msg.qr());
	BOOST_CHECK(Opcode::Query == msg.opcode());
	BOOST_CHECK(!msg.truncated());
	BOOST_CHECK(!msg.authoritative());
	BOOST_CHECK(RCode::NoError == msg.rcode());
	BOOST_REQUIRE_EQUAL(1, msg.size());
	queryCheck(msg[0]);
}

// Parse the response
BOOST_AUTO_TEST_CASE(parseReply) {
	const Blob packet(turris_cz_reply, sizeof turris_cz_reply);
	Message msg(packet);
	BOOST_CHECK(QR::Response == msg.qr());
	BOOST_CHECK(Opcode::Query == msg.opcode());
	BOOST_CHECK(!msg.truncated());
	BOOST_CHECK(!msg.authoritative());
	BOOST_CHECK(RCode::NoError == msg.rcode());
	BOOST_REQUIRE_EQUAL(5, msg.size());
	queryCheck(msg[0]);
	answerCheck(msg[1]);
	nsCheck(msg[2], msg, "a");
	nsCheck(msg[3], msg, "b");
	nsCheck(msg[4], msg, "d");
	// Also, check some relational operators of the Name and ParsedName
	BOOST_CHECK(*msg[0].name() == *msg[1].name());
	BOOST_CHECK(msg[0].name()->toOwned() == *msg[1].name());
	BOOST_CHECK(msg[0].name()->toOwned() == msg[1].name()->toOwned());
	const PParsedName otherName = msg.nameParseAt(msg[2].data().begin());
	BOOST_CHECK(*msg[0].name() != *otherName);
	BOOST_CHECK(msg[0].name()->toOwned() != *otherName);
	BOOST_CHECK(msg[0].name()->toOwned() != otherName->toOwned());
	// And check it acts sane with regards to hashing and hash containers
	const std::hash<Name> hasher;
	BOOST_CHECK_EQUAL(hasher(msg[0].name()->toOwned()), hasher(msg[1].name()->toOwned()));
	// This could in theory fail if there's a hash collision, but the chance is pretty small that we risk it in the test.
	BOOST_CHECK_NE(hasher(msg[0].name()->toOwned()), hasher(otherName->toOwned()));
	unordered_set<Name> names;
	names.insert(msg[0].name()->toOwned());
	BOOST_CHECK(names.find(msg[1].name()->toOwned()) != names.end());
	BOOST_CHECK(names.find(otherName->toOwned()) == names.end());
}

// If the message is too short to contain even the flags
BOOST_AUTO_TEST_CASE(parseFlagsShort) {
	const Blob packet(turris_cz_query, 3);
	BOOST_CHECK_THROW(Message msg(packet), Message::ParseError);
}

// Some parts of the records are missing
BOOST_AUTO_TEST_CASE(parseRecordsShort) {
	const Blob packet(turris_cz_reply, sizeof turris_cz_reply - 1);
	BOOST_CHECK_THROW(Message msg(packet), Message::ParseError);
}

// This one is too long, extra garbage at the end → not a valid DNS message
BOOST_AUTO_TEST_CASE(parseLong) {
	vector<uint8_t> data(turris_cz_reply, turris_cz_reply + sizeof turris_cz_reply);
	data.push_back(0);
	const Blob packet(&data[0], data.size());
	BOOST_CHECK_THROW(Message msg(packet), Message::ParseError);
}

// See that we handle a packet with broken label pointer properly
BOOST_AUTO_TEST_CASE(parseNameOutOfBounds) {
	const Blob packet(out_of_bounds_query, sizeof out_of_bounds_query);
	BOOST_CHECK_THROW(Message msg(packet), Message::ParseError);
}

// See that we handle a packet with cyclic label
BOOST_AUTO_TEST_CASE(cyclicLabel) {
	const Blob packet(cyclic_query, sizeof cyclic_query);
	BOOST_CHECK_THROW(Message msg(packet), Message::ParseError);
}

// See that we handle a packet with cyclic label
BOOST_AUTO_TEST_CASE(longerCyclicLabel) {
	const Blob packet(longer_cyclic_query, sizeof longer_cyclic_query);
	BOOST_CHECK_THROW(Message msg(packet), Message::ParseError);
}

}
