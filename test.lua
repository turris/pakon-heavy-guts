
-- queue(dict_of_flags, dissector_name, queue_len, queue_num, family, direction
for i, dir in pairs({"in", "out"}) do
	local tags = {src = "eth0", qdir = dir}
	queue(tags, "v4", 500, i - 1, 4, dir);
	queue(tags, "v6", 500, i + 1, 6, dir);
end
listen("socket")
